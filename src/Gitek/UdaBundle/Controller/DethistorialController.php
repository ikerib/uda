<?php

namespace Gitek\UdaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Gitek\UdaBundle\Entity\Dethistorial;

/**
 * Dethistorial controller.
 *
 * @Route("/dethistorial")
 */
class DethistorialController extends Controller
{
    /**
     * Lists all Dethistorial entities.
     *
     * @Route("/", name="dethistorial")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $dethistoriales = $em->getRepository('GitekUdaBundle:Dethistorial')->findAll();

        return $this->render('GitekUdaBundle:Dethistorial:index.html.twig', array(
            'dethistoriales' => $dethistoriales
        ));
    }

    /**
     * Finds and displays a Dethistorial entity.
     *
     * @Route("/{id}/show", name="dethistorial_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Dethistorial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Dethistorial entity.');
        }

        return $this->render('GitekUdaBundle:Dethistorial:show.html.twig', array(
            'entity'      => $entity,
        ));
    }

}
