<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Gitek\UdaBundle\Entity\Detformacion
 *
 * @ORM\Table()
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="Gitek\UdaBundle\Entity\DetformacionRepository")
 */
class Detformacion
{
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var text $nombre
     *
     * @ORM\Column(name="nombre", type="text")
     */
    private $nombre;

    /**
     * @var integer $orden
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;

    /**
     * @var text $texto
     *
     * @ORM\Column(name="texto", type="text", nullable=true)
     */
    private $texto;

    /**
     * @var integer $formacion_id
     *
     * @ORM\Column(name="formacion_id", type="integer")
     */
    private $formacion_id;

    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imagen", nullable=true)
    *
    * @var File $file
    */
    protected $file;

    /**
     * @ORM\Column(type="string", length=255, name="imagen", nullable=true)
     *
     * @var string $imagen
     */
    protected $imagen;

    public function setFile($file) {
        $this->file = $file;
        if ($file instanceof UploadedFile) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    public function getFile() {
        return $this-> file;
    }


    /**
    * @Assert\File(
    *     maxSize="10M",
    *     mimeTypes={"audio/mp3","audio/mpeg", "audio/wav", "audio/x-wav", "audio/mp4", "audio/ogg" }
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="audio", nullable=true)
    *
    * @var File $file2
    */
    protected $file2;

    /**
     * @ORM\Column(type="string", length=255, name="audio", nullable=true)
     *
     * @var string $audio
     */
    protected $audio;

    public function setFile2($file2) {
        $this->file2 = $file2;
        if ($file2 instanceof UploadedFile) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    public function getFile2() {
        return $this-> file2;
    }

    /**
    * @Assert\File(
    *     maxSize="10M",
    *     mimeTypes={"video/mp4", "video/webm", "video/ogg", "video/x-msvideo", "video/quicktime"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="video", nullable=true)
    *
    * @var File $file3
    */
    protected $file3;

    /**
     * @ORM\Column(type="string", length=255, name="video", nullable=true)
     *
     * @var string $video
     */
    protected $video;
    public function setFile3($file3) {
        $this->file3 = $file3;
        if ($file3 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }

    public function getFile3() {
        return $this-> file3;
    }

    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true))
     */
    private $url;

	/**
	* @ORM\Column(type="datetime", name="created_at")
    *
    * @var DateTime $createdAt
    */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     *
     * @var DateTime $updatedAt
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Formacion",inversedBy="detformaciones")
     * @ORM\JoinColumn(name="formacion_id", referencedColumnName="id")
     */
    private $formacion;

    /**
     * @ORM\OneToMany(targetEntity="Detentrenamiento", mappedBy="detformacion", cascade={"remove"})
     */
    private $detformacion;

    /**
     * @ORM\OneToMany(targetEntity="Dethistorial", mappedBy="detformacion", cascade={"remove"})
     */
    private $dethistoriales;


    public function __construct()
    {
		$this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function __toString()
    {
        return $this->getNombre();
    }




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Detformacion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Detformacion
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return Detformacion
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set formacion_id
     *
     * @param integer $formacionId
     * @return Detformacion
     */
    public function setFormacionId($formacionId)
    {
        $this->formacion_id = $formacionId;

        return $this;
    }

    /**
     * Get formacion_id
     *
     * @return integer
     */
    public function getFormacionId()
    {
        return $this->formacion_id;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Detformacion
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set audio
     *
     * @param string $audio
     * @return Detformacion
     */
    public function setAudio($audio)
    {
        $this->audio = $audio;

        return $this;
    }

    /**
     * Get audio
     *
     * @return string
     */
    public function getAudio()
    {
        return $this->audio;
    }

    /**
     * Set video
     *
     * @param string $video
     * @return Detformacion
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Detformacion
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Detformacion
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Detformacion
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set formacion
     *
     * @param \Gitek\UdaBundle\Entity\Formacion $formacion
     * @return Detformacion
     */
    public function setFormacion(\Gitek\UdaBundle\Entity\Formacion $formacion = null)
    {
        $this->formacion = $formacion;

        return $this;
    }

    /**
     * Get formacion
     *
     * @return \Gitek\UdaBundle\Entity\Formacion
     */
    public function getFormacion()
    {
        return $this->formacion;
    }

    /**
     * Add detformacion
     *
     * @param \Gitek\UdaBundle\Entity\Detentrenamiento $detformacion
     * @return Detformacion
     */
    public function addDetformacion(\Gitek\UdaBundle\Entity\Detentrenamiento $detformacion)
    {
        $this->detformacion[] = $detformacion;

        return $this;
    }

    /**
     * Remove detformacion
     *
     * @param \Gitek\UdaBundle\Entity\Detentrenamiento $detformacion
     */
    public function removeDetformacion(\Gitek\UdaBundle\Entity\Detentrenamiento $detformacion)
    {
        $this->detformacion->removeElement($detformacion);
    }

    /**
     * Get detformacion
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDetformacion()
    {
        return $this->detformacion;
    }

    /**
     * Add dethistoriales
     *
     * @param \Gitek\UdaBundle\Entity\Dethistorial $dethistoriales
     * @return Detformacion
     */
    public function addDethistoriale(\Gitek\UdaBundle\Entity\Dethistorial $dethistoriales)
    {
        $this->dethistoriales[] = $dethistoriales;

        return $this;
    }

    /**
     * Remove dethistoriales
     *
     * @param \Gitek\UdaBundle\Entity\Dethistorial $dethistoriales
     */
    public function removeDethistoriale(\Gitek\UdaBundle\Entity\Dethistorial $dethistoriales)
    {
        $this->dethistoriales->removeElement($dethistoriales);
    }

    /**
     * Get dethistoriales
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDethistoriales()
    {
        return $this->dethistoriales;
    }
}