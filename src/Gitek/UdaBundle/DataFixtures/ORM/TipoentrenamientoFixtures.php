<?php

namespace Gitek\UdaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\UdaBundle\Entity\Tipoentrenamiento;

class TipoentrenamientoFixtures extends AbstractFixture implements OrderedFixtureInterface
{
	public function load($manager)
	    {
	    	
	        $tipoentrenamiento1 = new Tipoentrenamiento();
	        $tipoentrenamiento1->setTipo('X7');
	        $manager->persist($tipoentrenamiento1);

	        $tipoentrenamiento2 = new Tipoentrenamiento();
	        $tipoentrenamiento2->setTipo('1-Imagen');
	        $manager->persist($tipoentrenamiento2);

	        $tipoentrenamiento3 = new Tipoentrenamiento();
	        $tipoentrenamiento3->setTipo('4-Imagen');
	        $manager->persist($tipoentrenamiento3);

	        $tipoentrenamiento4 = new Tipoentrenamiento();
	        $tipoentrenamiento4->setTipo('Colocar componente');
	        $manager->persist($tipoentrenamiento4);


	        $manager->flush();

	        $this->addReference('tipoentrenamiento1', $tipoentrenamiento1);
	        $this->addReference('tipoentrenamiento2', $tipoentrenamiento2);
			$this->addReference('tipoentrenamiento3', $tipoentrenamiento3);
			$this->addReference('tipoentrenamiento4', $tipoentrenamiento4);
	    }

	    public function getOrder()
	    {
	        return 3; // el orden en el que se deben cargar los accesorios
	    }
}