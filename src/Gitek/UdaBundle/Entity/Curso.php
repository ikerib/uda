<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Gitek\UdaBundle\Entity\Curso
 *
 * @ORM\Table(name="Curso")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Gitek\UdaBundle\Entity\CursoRepository")
 */
class Curso
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool $protegido
     *
     * @ORM\Column(name="protegido", type="boolean", nullable=true)
     */
    private $protegido;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string $version
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @var integer $orden
     *
     * @ORM\Column(name="orden", type="integer", nullable=true, nullable=true)
     */
    private $orden;

    /**
     * @var integer $tiempo
     *
     * @ORM\Column(name="tiempo", type="integer", nullable=true, nullable=true)
     */
    private $tiempo;

    /**
     * @ORM\OneToMany(targetEntity="Detcurso", mappedBy="curso", cascade={"remove"})
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    private $detcursos;

    /**
     * @ORM\OneToMany(targetEntity="Historial", mappedBy="curso", cascade={"remove"})
     */
    private $historiales;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;


    public function __construct()
    {
        $this->detcursos = new \Doctrine\Common\Collections\ArrayCollection();
		$this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
    }

    public function __toString()
    {
        return $this->getNombre();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set version
     *
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add detcursos
     *
     * @param Gitek\UdaBundle\Entity\Detcurso $detcursos
     */
    public function addDetcurso(\Gitek\UdaBundle\Entity\Detcurso $detcursos)
    {
        $this->detcursos[] = $detcursos;
    }

    /**
     * Get detcursos
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getDetcursos()
    {
        return $this->detcursos;
    }

    /**
     * Add historiales
     *
     * @param Gitek\UdaBundle\Entity\Historial $historiales
     */
    public function addHistorial(\Gitek\UdaBundle\Entity\Historial $historiales)
    {
        $this->historiales[] = $historiales;
    }

    /**
     * Get historiales
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getHistoriales()
    {
        return $this->historiales;
    }

    /**
     * Get historial
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getHistorial()
    {
        return $this->historial;
    }

    /**
     * Set tiempo
     *
     * @param integer $tiempo
     */
    public function setTiempo($tiempo)
    {
        $this->tiempo = $tiempo;
    }

    /**
     * Get tiempo
     *
     * @return integer
     */
    public function getTiempo()
    {
        return $this->tiempo;
    }

    /**
     * Remove detcursos
     *
     * @param \Gitek\UdaBundle\Entity\Detcurso $detcursos
     */
    public function removeDetcurso(\Gitek\UdaBundle\Entity\Detcurso $detcursos)
    {
        $this->detcursos->removeElement($detcursos);
    }

    /**
     * Add historiales
     *
     * @param \Gitek\UdaBundle\Entity\Historial $historiales
     * @return Curso
     */
    public function addHistoriale(\Gitek\UdaBundle\Entity\Historial $historiales)
    {
        $this->historiales[] = $historiales;

        return $this;
    }

    /**
     * Remove historiales
     *
     * @param \Gitek\UdaBundle\Entity\Historial $historiales
     */
    public function removeHistoriale(\Gitek\UdaBundle\Entity\Historial $historiales)
    {
        $this->historiales->removeElement($historiales);
    }

    /**
     * Set protegido
     *
     * @param boolean $protegido
     * @return Curso
     */
    public function setProtegido($protegido)
    {
        $this->protegido = $protegido;
    
        return $this;
    }

    /**
     * Get protegido
     *
     * @return boolean 
     */
    public function getProtegido()
    {
        return $this->protegido;
    }
}