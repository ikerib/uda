<?php

namespace Gitek\UdaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
// use Gitek\UdaBundle\Entity\Formacion;
use Gitek\UdaBundle\Entity\Detformacion;

class DetformacionFixtures extends AbstractFixture implements OrderedFixtureInterface
{
	public function load($manager)
	{
		
	    $detformacion1 = new Detformacion();
	    $detformacion1->setNombre('1-Colocar Subconjunto Potting');
	    $detformacion1->setUrl('http://www.youtube.com/watch?v=pp2aNmvM9Bw&feature=g-all-u&context=G2e7e175FAAAAAAAABAA');
		$detformacion1->setVideo('noborrar/operacion1.mp4');
		$detformacion1->setTexto('Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 				labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum');
		$detformacion1->setFormacion($manager->merge($this->getReference('formacion1')));
	    $manager->persist($detformacion1);

	    
	    	        	     
	    
	    $manager->flush();

        $this->addReference('detformacion1', $detformacion1);
        
    }

	    public function getOrder()
	    {
	        return 2; // el orden en el que se deben cargar los accesorios
	    }
}