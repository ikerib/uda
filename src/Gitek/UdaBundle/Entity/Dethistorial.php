<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gitek\UdaBundle\Entity\Dethistorial
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\UdaBundle\Entity\DethistorialRepository")
 */
class Dethistorial
{

	/**
	 * @ORM\PreUpdate()
	 */
	public function preUpdate()
	{
		$this->updatedAt = new \DateTime();
	}

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $orden
     *
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden;

    /**
     * @var integer $historial_id
     *
     * @ORM\Column(name="historial_id", type="integer")
     */
    private $historial_id;

    /**
     * @var datetime $fecha_ini
     *
     * @ORM\Column(name="fecha_ini", type="datetime", nullable=true)
     */
    private $fecha_ini;

    /**
     * @var datetime $fecha_fin
     *
     * @ORM\Column(name="fecha_fin", type="datetime", nullable=true, nullable=true)
     */
    private $fecha_fin;

    /**
     * @var integer $entrenamiento_id
     *
     * @ORM\Column(name="entrenamiento_id", type="integer", nullable=true)
     */
    private $entrenamiento_id;

    /**
     * @var integer $detformacion_id
     *
     * @ORM\Column(name="detformacion_id", type="integer", nullable=true)
     */
    private $detformacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Detformacion",inversedBy="dethistoriales")
     * @ORM\JoinColumn(name="detformacion_id", referencedColumnName="id")
     */
    private $detformacion;

    /**
     * @var integer $detcurso_id
     *
     * @ORM\Column(name="detcurso_id", type="integer")
     */
    private $detcurso_id;

    /**
     * @ORM\ManyToOne(targetEntity="Detcurso",inversedBy="dethistoriales2")
     * @ORM\JoinColumn(name="detcurso_id", referencedColumnName="id")
     */
    private $detcurso;

    /**
     * @var integer $pulsaOK
     *
     * @ORM\Column(name="pulsaOK", type="integer", nullable=true)
     */
    private $pulsaOK;

    /**
     * @var integer $pulsaKO
     *
     * @ORM\Column(name="pulsaKO", type="integer", nullable=true)
     */
    private $pulsaKO;

    /**
     * @var integer $pulsasolucion
     *
     * @ORM\Column(name="pulsasolucion", type="integer", nullable=true)
     */
    private $pulsasolucion;

    /**
     * @var integer $pulsareintentar
     *
     * @ORM\Column(name="pulsareintentar", type="integer", nullable=true)
     */
    private $pulsareintentar;

    /**
     * @var integer $pulsaD16
     *
     * @ORM\Column(name="pulsaD16", type="integer", nullable=true)
     */
    private $pulsaD16;


    /**
     * @ORM\ManyToOne(targetEntity="Historial",inversedBy="dethistoriales")
     * @ORM\JoinColumn(name="historial_id", referencedColumnName="id")
     */
    private $historial;


   /**
		*
    * DATOS DE LOS ENTRENAMIENTOS
		*
		**/


		/**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

	/**
    * @var string $pregunta
    *
    * @ORM\Column(name="pregunta", type="string", length=255, nullable=true)
    */
    private $pregunta;

    // /**
    //  * @ORM\ManyToOne(targetEntity="Formacion",inversedBy="formacion")
    //  * @ORM\JoinColumn(name="formacion_id", referencedColumnName="id")
    //  */
    // private $formacion;

		/*
		 *
		 *  COMPONENTE CORRECTO
		 *
		 */


    /**
     * @var string $img1
     *
     * @ORM\Column(name="img1", type="string", length=255, nullable=true)
     */
    private $img1;

    /**
     * @var string $img2
     *
     * @ORM\Column(name="img2", type="string", length=255, nullable=true)
     */
    private $img2;


    /**
     * @var string $img3
     *
     * @ORM\Column(name="img3", type="string", length=255, nullable=true)
     */
    private $img3;

    /**
     * @var string $imgok
     *
     * @ORM\Column(name="imgok", type="string", length=255, nullable=true)
     */
    private $imgok;



    /*
     *
		 * EPI CORRECTO
     *
     */

		/**
     * @var string $imgepi1
     *
     * @ORM\Column(name="imgepi1", type="string", length=255, nullable=true)
     */
    private $imgepi1;

		/**
     * @var string $imgepi2
     *
     * @ORM\Column(name="imgepi2", type="string", length=255, nullable=true)
     */
    private $imgepi2;

		/**
     * @var string $imgepi3
     *
     * @ORM\Column(name="imgepi3", type="string", length=255, nullable=true)
     */
    private $imgepi3;

		/**
     * @var string $imgepiok
     *
     * @ORM\Column(name="imgepiok", type="string", length=255, nullable=true)
     */
    private $imgepiok;


		/*
		 *
		 *  COMPONENTE NO DEFECTUOSO
		 *
		 */

		/**
     * @var string $imgcomponente1
     *
     * @ORM\Column(name="imgcomponente1", type="string", length=255, nullable=true)
     */
    private $imgcomponente1;

		/**
     * @var string $imgcomponente2
     *
     * @ORM\Column(name="imgcomponente2", type="string", length=255, nullable=true)
     */
    private $imgcomponente2;

		/**
     * @var string $imgcomponente3
     *
     * @ORM\Column(name="imgcomponente3", type="string", length=255, nullable=true)
     */
    private $imgcomponente3;

		/**
     * @var string $imgcomponenteok
     *
     * @ORM\Column(name="imgcomponenteok", type="string", length=255, nullable=true)
     */
    private $imgcomponenteok;


		/*
		 *
		 *  COLOCAR COMPONENTE
		 *
		 */

		/**
     * @var string $imgrotar1
     *
     * @ORM\Column(name="imgrotar1", type="string", length=255, nullable=true)
     */
    private $imgrotar1;

		/**
     * @var string $imgrotar2
     *
     * @ORM\Column(name="imgrotar2", type="string", length=255, nullable=true)
     */
    private $imgrotar2;

		/**
     * @var string $imgrotar3
     *
     * @ORM\Column(name="imgrotar3", type="string", length=255, nullable=true)
     */
    private $imgrotar3;

		/**
     * @var string $imgrotarok
     *
     * @ORM\Column(name="imgrotarok", type="string", length=255, nullable=true)
     */
    private $imgrotarok;

    /**
     * @var string $imgbase
     *
     * @ORM\Column(name="imgbase", type="string", length=255, nullable=true)
     */
    private $imgbase;


    /**
     * @var string $coordenadas
     *
     * @ORM\Column(name="coordenadas", type="string", length=255, nullable=true)
     */
    private $coordenadas;

    /**
     * @var string $coordenadas2
     *
     * @ORM\Column(name="coordenadas2", type="string", length=255, nullable=true)
     */
    private $coordenadas2;

    /**
     * @var string $coordenadas3
     *
     * @ORM\Column(name="coordenadas3", type="string", length=255, nullable=true)
     */
    private $coordenadas3;

    /**
     * @var string $coordenadasok
     *
     * @ORM\Column(name="coordenadasok", type="string", length=255, nullable=true)
     */
    private $coordenadasok;


		/**
		* @ORM\Column(type="datetime", name="created_at")
    *
    * @var DateTime $createdAt
    */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     *
     * @var DateTime $updatedAt
     */
    protected $updatedAt;


    /**
     * @var integer $tipoentrenamiento_id
     *
     * @ORM\Column(name="tipoentrenamiento_id", type="integer", nullable=true)
     */
    private $tipoentrenamiento_id;

    /**
      * @ORM\ManyToOne(targetEntity="Tipoentrenamiento")
      * @ORM\JoinColumn(name="tipoentrenamiento_id", referencedColumnName="id")
      */
     private $tipoentrenamiento;



   public function __construct()
   {
			$this->createdAt = new \DateTime();
      $this->updatedAt = new \DateTime();
   }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set historial_id
     *
     * @param integer $historialId
     */
    public function setHistorialId($historialId)
    {
        $this->historial_id = $historialId;
    }

    /**
     * Get historial_id
     *
     * @return integer
     */
    public function getHistorialId()
    {
        return $this->historial_id;
    }

    /**
     * Set fecha_ini
     *
     * @param datetime $fechaIni
     */
    public function setFechaIni($fechaIni)
    {
        $this->fecha_ini = $fechaIni;
    }

    /**
     * Get fecha_ini
     *
     * @return datetime
     */
    public function getFechaIni()
    {
        return $this->fecha_ini;
    }

    /**
     * Set fecha_fin
     *
     * @param datetime $fechaFin
     */
    public function setFechaFin($fechaFin)
    {
        $this->fecha_fin = $fechaFin;
    }

    /**
     * Get fecha_fin
     *
     * @return datetime
     */
    public function getFechaFin()
    {
        return $this->fecha_fin;
    }

    /**
     * Set entrenamiento_id
     *
     * @param integer $entrenamientoId
     */
    public function setEntrenamientoId($entrenamientoId)
    {
        $this->entrenamiento_id = $entrenamientoId;
    }

    /**
     * Get entrenamiento_id
     *
     * @return integer
     */
    public function getEntrenamientoId()
    {
        return $this->entrenamiento_id;
    }

    /**
     * Set formacion_id
     *
     * @param integer $formacionId
     */
    public function setFormacionId($formacionId)
    {
        $this->formacion_id = $formacionId;
    }

    /**
     * Get formacion_id
     *
     * @return integer
     */
    public function getFormacionId()
    {
        return $this->formacion_id;
    }

    /**
     * Set hora_ini
     *
     * @param time $horaIni
     */
    public function setHoraIni($horaIni)
    {
        $this->hora_ini = $horaIni;
    }

    /**
     * Get hora_ini
     *
     * @return time
     */
    public function getHoraIni()
    {
        return $this->hora_ini;
    }

    /**
     * Set hora_fin
     *
     * @param time $horaFin
     */
    public function setHoraFin($horaFin)
    {
        $this->hora_fin = $horaFin;
    }

    /**
     * Get hora_fin
     *
     * @return time
     */
    public function getHoraFin()
    {
        return $this->hora_fin;
    }

    /**
     * Set pulsaOK
     *
     * @param integer $pulsaOK
     */
    public function setPulsaOK($pulsaOK)
    {
        $this->pulsaOK = $pulsaOK;
    }

    /**
     * Get pulsaOK
     *
     * @return integer
     */
    public function getPulsaOK()
    {
        return $this->pulsaOK;
    }

    /**
     * Set pulsaKO
     *
     * @param integer $pulsaKO
     */
    public function setPulsaKO($pulsaKO)
    {
        $this->pulsaKO = $pulsaKO;
    }

    /**
     * Get pulsaKO
     *
     * @return integer
     */
    public function getPulsaKO()
    {
        return $this->pulsaKO;
    }

    /**
     * Set pulsasolucion
     *
     * @param integer $pulsasolucion
     */
    public function setPulsasolucion($pulsasolucion)
    {
        $this->pulsasolucion = $pulsasolucion;
    }

    /**
     * Get pulsasolucion
     *
     * @return integer
     */
    public function getPulsasolucion()
    {
        return $this->pulsasolucion;
    }

    /**
     * Set pulsareintentar
     *
     * @param integer $pulsareintentar
     */
    public function setPulsareintentar($pulsareintentar)
    {
        $this->pulsareintentar = $pulsareintentar;
    }

    /**
     * Get pulsareintentar
     *
     * @return integer
     */
    public function getPulsareintentar()
    {
        return $this->pulsareintentar;
    }

    /**
     * Set pulsaD16
     *
     * @param integer $pulsaD16
     */
    public function setPulsaD16($pulsaD16)
    {
        $this->pulsaD16 = $pulsaD16;
    }

    /**
     * Get pulsaD16
     *
     * @return integer
     */
    public function getPulsaD16()
    {
        return $this->pulsaD16;
    }

    /**
     * Set ent_nombre
     *
     * @param string $entNombre
     */
    public function setEntNombre($entNombre)
    {
        $this->ent_nombre = $entNombre;
    }

    /**
     * Get ent_nombre
     *
     * @return string
     */
    public function getEntNombre()
    {
        return $this->ent_nombre;
    }

    /**
     * Set img1
     *
     * @param string $img1
     */
    public function setImg1($img1)
    {
        $this->img1 = $img1;
    }

    /**
     * Get img1
     *
     * @return string
     */
    public function getImg1()
    {
        return $this->img1;
    }

    /**
     * Set img2
     *
     * @param string $img2
     */
    public function setImg2($img2)
    {
        $this->img2 = $img2;
    }

    /**
     * Get img2
     *
     * @return string
     */
    public function getImg2()
    {
        return $this->img2;
    }

    /**
     * Set img3
     *
     * @param string $img3
     */
    public function setImg3($img3)
    {
        $this->img3 = $img3;
    }

    /**
     * Get img3
     *
     * @return string
     */
    public function getImg3()
    {
        return $this->img3;
    }

    /**
     * Set imgok
     *
     * @param string $imgok
     */
    public function setImgok($imgok)
    {
        $this->imgok = $imgok;
    }

    /**
     * Get imgok
     *
     * @return string
     */
    public function getImgok()
    {
        return $this->imgok;
    }

    /**
     * Set imgepi1
     *
     * @param string $imgepi1
     */
    public function setImgepi1($imgepi1)
    {
        $this->imgepi1 = $imgepi1;
    }

    /**
     * Get imgepi1
     *
     * @return string
     */
    public function getImgepi1()
    {
        return $this->imgepi1;
    }

    /**
     * Set imgepi2
     *
     * @param string $imgepi2
     */
    public function setImgepi2($imgepi2)
    {
        $this->imgepi2 = $imgepi2;
    }

    /**
     * Get imgepi2
     *
     * @return string
     */
    public function getImgepi2()
    {
        return $this->imgepi2;
    }

    /**
     * Set imgepi3
     *
     * @param string $imgepi3
     */
    public function setImgepi3($imgepi3)
    {
        $this->imgepi3 = $imgepi3;
    }

    /**
     * Get imgepi3
     *
     * @return string
     */
    public function getImgepi3()
    {
        return $this->imgepi3;
    }

    /**
     * Set imgepiok
     *
     * @param string $imgepiok
     */
    public function setImgepiok($imgepiok)
    {
        $this->imgepiok = $imgepiok;
    }

    /**
     * Get imgepiok
     *
     * @return string
     */
    public function getImgepiok()
    {
        return $this->imgepiok;
    }

    /**
     * Set imgcomponente1
     *
     * @param string $imgcomponente1
     */
    public function setImgcomponente1($imgcomponente1)
    {
        $this->imgcomponente1 = $imgcomponente1;
    }

    /**
     * Get imgcomponente1
     *
     * @return string
     */
    public function getImgcomponente1()
    {
        return $this->imgcomponente1;
    }

    /**
     * Set imgcomponente2
     *
     * @param string $imgcomponente2
     */
    public function setImgcomponente2($imgcomponente2)
    {
        $this->imgcomponente2 = $imgcomponente2;
    }

    /**
     * Get imgcomponente2
     *
     * @return string
     */
    public function getImgcomponente2()
    {
        return $this->imgcomponente2;
    }

    /**
     * Set imgcomponente3
     *
     * @param string $imgcomponente3
     */
    public function setImgcomponente3($imgcomponente3)
    {
        $this->imgcomponente3 = $imgcomponente3;
    }

    /**
     * Get imgcomponente3
     *
     * @return string
     */
    public function getImgcomponente3()
    {
        return $this->imgcomponente3;
    }

    /**
     * Set imgcomponenteok
     *
     * @param string $imgcomponenteok
     */
    public function setImgcomponenteok($imgcomponenteok)
    {
        $this->imgcomponenteok = $imgcomponenteok;
    }

    /**
     * Get imgcomponenteok
     *
     * @return string
     */
    public function getImgcomponenteok()
    {
        return $this->imgcomponenteok;
    }

    /**
     * Set imgrotar1
     *
     * @param string $imgrotar1
     */
    public function setImgrotar1($imgrotar1)
    {
        $this->imgrotar1 = $imgrotar1;
    }

    /**
     * Get imgrotar1
     *
     * @return string
     */
    public function getImgrotar1()
    {
        return $this->imgrotar1;
    }

    /**
     * Set imgrotar2
     *
     * @param string $imgrotar2
     */
    public function setImgrotar2($imgrotar2)
    {
        $this->imgrotar2 = $imgrotar2;
    }

    /**
     * Get imgrotar2
     *
     * @return string
     */
    public function getImgrotar2()
    {
        return $this->imgrotar2;
    }

    /**
     * Set imgrotar3
     *
     * @param string $imgrotar3
     */
    public function setImgrotar3($imgrotar3)
    {
        $this->imgrotar3 = $imgrotar3;
    }

    /**
     * Get imgrotar3
     *
     * @return string
     */
    public function getImgrotar3()
    {
        return $this->imgrotar3;
    }

    /**
     * Set imgrotarok
     *
     * @param string $imgrotarok
     */
    public function setImgrotarok($imgrotarok)
    {
        $this->imgrotarok = $imgrotarok;
    }

    /**
     * Get imgrotarok
     *
     * @return string
     */
    public function getImgrotarok()
    {
        return $this->imgrotarok;
    }

    /**
     * Set imgbase
     *
     * @param string $imgbase
     */
    public function setImgbase($imgbase)
    {
        $this->imgbase = $imgbase;
    }

    /**
     * Get imgbase
     *
     * @return string
     */
    public function getImgbase()
    {
        return $this->imgbase;
    }

    /**
     * Set coordenadas
     *
     * @param string $coordenadas
     */
    public function setCoordenadas($coordenadas)
    {
        $this->coordenadas = $coordenadas;
    }

    /**
     * Get coordenadas
     *
     * @return string
     */
    public function getCoordenadas()
    {
        return $this->coordenadas;
    }

    /**
     * Set coordenadas2
     *
     * @param string $coordenadas2
     */
    public function setCoordenadas2($coordenadas2)
    {
        $this->coordenadas2 = $coordenadas2;
    }

    /**
     * Get coordenadas2
     *
     * @return string
     */
    public function getCoordenadas2()
    {
        return $this->coordenadas2;
    }

    /**
     * Set coordenadas3
     *
     * @param string $coordenadas3
     */
    public function setCoordenadas3($coordenadas3)
    {
        $this->coordenadas3 = $coordenadas3;
    }

    /**
     * Get coordenadas3
     *
     * @return string
     */
    public function getCoordenadas3()
    {
        return $this->coordenadas3;
    }

    /**
     * Set coordenadasok
     *
     * @param string $coordenadasok
     */
    public function setCoordenadasok($coordenadasok)
    {
        $this->coordenadasok = $coordenadasok;
    }

    /**
     * Get coordenadasok
     *
     * @return string
     */
    public function getCoordenadasok()
    {
        return $this->coordenadasok;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set tipoentrenamiendo_id
     *
     * @param integer $tipoentrenamiendoId
     */
    public function setTipoentrenamiendoId($tipoentrenamiendoId)
    {
        $this->tipoentrenamiendo_id = $tipoentrenamiendoId;
    }

    /**
     * Get tipoentrenamiendo_id
     *
     * @return integer
     */
    public function getTipoentrenamiendoId()
    {
        return $this->tipoentrenamiendo_id;
    }

    /**
     * Set historial
     *
     * @param Gitek\UdaBundle\Entity\Historial $historial
     */
    public function setHistorial(\Gitek\UdaBundle\Entity\Historial $historial)
    {
        $this->historial = $historial;
    }

    /**
     * Get historial
     *
     * @return Gitek\UdaBundle\Entity\Historial
     */
    public function getHistorial()
    {
        return $this->historial;
    }

    /**
     * Set entrenamiento
     *
     * @param Gitek\UdaBundle\Entity\Entrenamiento $entrenamiento
     */
    public function setEntrenamiento(\Gitek\UdaBundle\Entity\Entrenamiento $entrenamiento)
    {
        $this->entrenamiento = $entrenamiento;
    }

    /**
     * Get entrenamiento
     *
     * @return Gitek\UdaBundle\Entity\Entrenamiento
     */
    public function getEntrenamiento()
    {
        return $this->entrenamiento;
    }

    /**
     * Set tipoentrenamiento
     *
     * @param Gitek\UdaBundle\Entity\Tipoentrenamiento $tipoentrenamiento
     */
    public function setTipoentrenamiento(\Gitek\UdaBundle\Entity\Tipoentrenamiento $tipoentrenamiento)
    {
        $this->tipoentrenamiento = $tipoentrenamiento;
    }

    /**
     * Get tipoentrenamiento
     *
     * @return Gitek\UdaBundle\Entity\Tipoentrenamiento
     */
    public function getTipoentrenamiento()
    {
        return $this->tipoentrenamiento;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set tipoentrenamiento_id
     *
     * @param integer $tipoentrenamientoId
     */
    public function setTipoentrenamientoId($tipoentrenamientoId)
    {
        $this->tipoentrenamiento_id = $tipoentrenamientoId;
    }

    /**
     * Get tipoentrenamiento_id
     *
     * @return integer
     */
    public function getTipoentrenamientoId()
    {
        return $this->tipoentrenamiento_id;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set pregunta
     *
     * @param string $pregunta
     */
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;
    }

    /**
     * Get pregunta
     *
     * @return string
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set formacion
     *
     * @param Gitek\UdaBundle\Entity\Detformacion $formacion
     */
    public function setFormacion(\Gitek\UdaBundle\Entity\Detformacion $formacion)
    {
        $this->formacion = $formacion;
    }

    /**
     * Get formacion
     *
     * @return Gitek\UdaBundle\Entity\Detformacion
     */
    public function getFormacion()
    {
        return $this->formacion;
    }

    /**
     * Set detformacion
     *
     * @param Gitek\UdaBundle\Entity\Detformacion $detformacion
     */
    public function setDetformacion(\Gitek\UdaBundle\Entity\Detformacion $detformacion)
    {
        $this->detformacion = $detformacion;
    }

    /**
     * Get detformacion
     *
     * @return Gitek\UdaBundle\Entity\Detformacion
     */
    public function getDetformacion()
    {
        return $this->detformacion;
    }

    /**
     * Set detformacion_id
     *
     * @param integer $detformacionId
     */
    public function setDetformacionId($detformacionId)
    {
        $this->detformacion_id = $detformacionId;
    }

    /**
     * Get detformacion_id
     *
     * @return integer
     */
    public function getDetformacionId()
    {
        return $this->detformacion_id;
    }

    /**
     * Set detcurso_id
     *
     * @param integer $detcursoId
     */
    public function setDetcursoId($detcursoId)
    {
        $this->detcurso_id = $detcursoId;
    }

    /**
     * Get detcurso_id
     *
     * @return integer
     */
    public function getDetcursoId()
    {
        return $this->detcurso_id;
    }

    /**
     * Set detcurso
     *
     * @param Gitek\UdaBundle\Entity\Detcurso $detcurso
     */
    public function setDetcurso(\Gitek\UdaBundle\Entity\Detcurso $detcurso)
    {
        $this->detcurso = $detcurso;
    }

    /**
     * Get detcurso
     *
     * @return Gitek\UdaBundle\Entity\Detcurso
     */
    public function getDetcurso()
    {
        return $this->detcurso;
    }

    /**
     * Set pregunta2
     *
     * @param string $pregunta2
     */
    public function setPregunta2($pregunta2)
    {
        $this->pregunta2 = $pregunta2;
    }

    /**
     * Get pregunta2
     *
     * @return string
     */
    public function getPregunta2()
    {
        return $this->pregunta2;
    }

    /**
     * Set pregunta3
     *
     * @param string $pregunta3
     */
    public function setPregunta3($pregunta3)
    {
        $this->pregunta3 = $pregunta3;
    }

    /**
     * Get pregunta3
     *
     * @return string
     */
    public function getPregunta3()
    {
        return $this->pregunta3;
    }

    /**
     * Set pregunta4
     *
     * @param string $pregunta4
     */
    public function setPregunta4($pregunta4)
    {
        $this->pregunta4 = $pregunta4;
    }

    /**
     * Get pregunta4
     *
     * @return string
     */
    public function getPregunta4()
    {
        return $this->pregunta4;
    }
}