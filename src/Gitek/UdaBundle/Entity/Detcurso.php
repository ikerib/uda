<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Gitek\UdaBundle\Entity\Detcurso
 *
 * @ORM\Table(name="Detcurso")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Gitek\UdaBundle\Entity\DetcursoRepository")
 */
class Detcurso
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

		/**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var integer $curso_id
     *
     * @ORM\Column(name="curso_id", type="integer")
     */
    private $curso_id;

    /**
     * @var array $cursos
     *
     * @ORM\Column(name="formacion_id", type="integer", nullable=true)
     */
    private $formacion_id;

    /**
     * @var integer $entrenamiento_id
     *
     * @ORM\Column(name="entrenamiento_id", type="integer", nullable=true)
     */
    private $entrenamiento_id;

    /**
     * @var string $orden
     *
     * @ORM\Column(name="orden", type="integer", length=255, nullable=true)
     */
    private $orden;

    /**
     * @ORM\ManyToOne(targetEntity="Curso",inversedBy="detcursos")
     * @ORM\JoinColumn(name="curso_id", referencedColumnName="id")
     */
    private $curso;

    /**
     * @ORM\ManyToOne(targetEntity="Formacion", inversedBy="detcursos")
     * @ORM\JoinColumn(name="formacion_id", referencedColumnName="id")
     */
    private $formacion;

    /**
     * @ORM\ManyToOne(targetEntity="Entrenamiento", inversedBy="detcursos")
     * @ORM\JoinColumn(name="entrenamiento_id", referencedColumnName="id")
     */
    private $entrenamiento;


    /**
     * @ORM\OneToMany(targetEntity="Dethistorial", mappedBy="detcurso", cascade={"remove"})
     */
    private $dethistoriales2;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set curso_id
     *
     * @param integer $cursoId
     */
    public function setCursoId($cursoId)
    {
        $this->curso_id = $cursoId;
    }

    /**
     * Get curso_id
     *
     * @return integer
     */
    public function getCursoId()
    {
        return $this->curso_id;
    }

    /**
     * Set formacion_id
     *
     * @param integer $formacionId
     */
    public function setFormacionId($formacionId)
    {
        $this->formacion_id = $formacionId;
    }

    /**
     * Get formacion_id
     *
     * @return integer
     */
    public function getFormacionId()
    {
        return $this->formacion_id;
    }

    /**
     * Set entrenamiento_id
     *
     * @param integer $entrenamientoId
     */
    public function setEntrenamientoId($entrenamientoId)
    {
        $this->entrenamiento_id = $entrenamientoId;
    }

    /**
     * Get entrenamiento_id
     *
     * @return integer
     */
    public function getEntrenamientoId()
    {
        return $this->entrenamiento_id;
    }

    /**
     * Set orden
     *
     * @param string $orden
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    }

    /**
     * Get orden
     *
     * @return string
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set curso
     *
     * @param Gitek\UdaBundle\Entity\Curso $curso
     */
    public function setCurso(\Gitek\UdaBundle\Entity\Curso $curso)
    {
        $this->curso = $curso;
    }

    /**
     * Get curso
     *
     * @return Gitek\UdaBundle\Entity\Curso
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * Set formacion
     *
     * @param Gitek\UdaBundle\Entity\Formacion $formacion
     */
    public function setFormacion(\Gitek\UdaBundle\Entity\Formacion $formacion)
    {
        $this->formacion = $formacion;
    }

    /**
     * Get formacion
     *
     * @return Gitek\UdaBundle\Entity\Formacion
     */
    public function getFormacion()
    {
        return $this->formacion;
    }

    /**
     * Set entrenamiento
     *
     * @param Gitek\UdaBundle\Entity\Entrenamiento $entrenamiento
     */
    public function setEntrenamiento(\Gitek\UdaBundle\Entity\Entrenamiento $entrenamiento)
    {
        $this->entrenamiento = $entrenamiento;
    }

    /**
     * Get entrenamiento
     *
     * @return Gitek\UdaBundle\Entity\Entrenamiento
     */
    public function getEntrenamiento()
    {
        return $this->entrenamiento;
    }
    public function __construct()
    {
        $this->dethistoriales = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add dethistoriales
     *
     * @param Gitek\UdaBundle\Entity\Dethistorial $dethistoriales
     */
    public function addDethistorial(\Gitek\UdaBundle\Entity\Dethistorial $dethistoriales)
    {
        $this->dethistoriales[] = $dethistoriales;
    }

    /**
     * Get dethistoriales
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getDethistoriales()
    {
        return $this->dethistoriales;
    }

    /**
     * Get dethistoriales2
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getDethistoriales2()
    {
        return $this->dethistoriales2;
    }

    /**
     * Add dethistoriales2
     *
     * @param \Gitek\UdaBundle\Entity\Dethistorial $dethistoriales2
     * @return Detcurso
     */
    public function addDethistoriales2(\Gitek\UdaBundle\Entity\Dethistorial $dethistoriales2)
    {
        $this->dethistoriales2[] = $dethistoriales2;

        return $this;
    }

    /**
     * Remove dethistoriales2
     *
     * @param \Gitek\UdaBundle\Entity\Dethistorial $dethistoriales2
     */
    public function removeDethistoriales2(\Gitek\UdaBundle\Entity\Dethistorial $dethistoriales2)
    {
        $this->dethistoriales2->removeElement($dethistoriales2);
    }
}