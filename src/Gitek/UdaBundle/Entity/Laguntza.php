<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Laguntza
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\UdaBundle\Entity\LaguntzaRepository")
 */
class Laguntza
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube", type="string", length=255)
     */
    private $youtube;

    /**
     * @var integer
     *
     * @ORM\Column(name="mostrar", type="smallint")
     */
    private $mostrar;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Laguntza
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set youtube
     *
     * @param string $youtube
     * @return Laguntza
     */
    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;
    
        return $this;
    }

    /**
     * Get youtube
     *
     * @return string 
     */
    public function getYoutube()
    {
        return $this->youtube;
    }

    /**
     * Set mostrar
     *
     * @param integer $mostrar
     * @return Laguntza
     */
    public function setMostrar($mostrar)
    {
        $this->mostrar = $mostrar;
    
        return $this;
    }

    /**
     * Get mostrar
     *
     * @return integer 
     */
    public function getMostrar()
    {
        return $this->mostrar;
    }
}