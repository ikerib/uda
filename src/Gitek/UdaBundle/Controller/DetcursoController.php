<?php

namespace Gitek\UdaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Gitek\UdaBundle\Entity\Curso;
use Gitek\UdaBundle\Entity\Detcurso;
use Gitek\UdaBundle\Entity\Formacion;
use Gitek\UdaBundle\Entity\Detformacion;
use Gitek\UdaBundle\Entity\Entrenamiento;
use Gitek\UdaBundle\Entity\Detentrenamiento;
use Gitek\UdaBundle\Entity\Tipoentrenamiento;
use Gitek\UdaBundle\Form\DetcursoType;

/**
 * Detcurso controller.
 *
 * @Route("/detcurso")
 */
class DetcursoController extends Controller
{
    /**
     * Lists all Detcurso entities.
     *
     * @Route("/", name="detcurso")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $detcurso = $em->getRepository('GitekUdaBundle:Detcurso')->findAll();

		return $this->render('GitekUdaBundle:Detcurso:index.html.twig', array(
            'detcurso' => $detcurso
        ));

    }

    /**
     * Finds and displays a Detcurso entity.
     *
     * @Route("/{id}/show", name="detcurso_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $detcurso = $em->getRepository('GitekUdaBundle:Detcurso')->find($id);

        if (!$detcurso) {
            throw $this->createNotFoundException('Unable to find Detcurso entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GitekUdaBundle:Detcurso:show.html.twig', array(
            'detcurso'      => $detcurso,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to create a new Detcurso entity.
     *
     * @Route("/new", name="detcurso_new")
     * @Template()
     */
    public function newAction($curso_id) {

	        $curso = $this->getCurso($curso_id);

	        $detcurso = new Detcurso();
	        $detcurso->setCurso($curso);
	        $form   = $this->createForm(new DetcursoType(), $detcurso);

	        return $this->render('GitekUdaBundle:Detcurso:form.html.twig', array(
	            'detcurso' => $detcurso,
	            'form'   => $form->createView()
	        ));
	  }


    /**
     * Creates a new Detcurso entity.
     *
     * @Route("/create", name="detcurso_create")
     * @Method("post")
     * @Template("GitekUdaBundle:Detcurso:new.html.twig")
     */
    public function createAction(Request $request, $curso_id) {
		$curso = $this->getCurso($curso_id);
        $detcurso  = new Detcurso();

        $detcurso->setCurso($curso);

        $comodines = $this->getSiguienteorden($curso_id);
        $comodines = $this->getSiguienteorden($curso_id);
        if ( empty($comodines) ) {
            $detcurso->setOrden(0);
        } else {
            $comodin = $comodines[0];
            $detcurso->setOrden($comodin->getOrden()+1);
        }

		$request = $this->getRequest();
		$form    = $this->createForm(new DetcursoType(), $detcurso);
		$form->bind($request);

		if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

			//creamos Formacion asociada
			$formacion = new Formacion();
			$formacion->setNombre($detcurso->getNombre());
            $em->persist($formacion);

			//creamos Entrenamiento asociado
			$entrenamiento = new Entrenamiento();
			$entrenamiento->setNombre($detcurso->getNombre());
            $em->persist($entrenamiento);

			$detcurso->setFormacion($formacion);
			$detcurso->setEntrenamiento($entrenamiento);

            $em->persist($detcurso);
            $em->flush();

			// $request->getSession()->setFlash('notice', 'OK');

            return $this->redirect($this->generateUrl('curso_show', array(
                            'id' => $detcurso->getCurso()->getId())) .
                            '#detcurso-' . $detcurso->getId()
                        );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }


    /**
     * Displays a form to edit an existing Detcurso entity.
     *
     * @Route("/{id}/edit", name="detcurso_edit")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Detcurso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Detcurso entity.');
        }

        $editForm = $this->createForm(new DetcursoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GitekUdaBundle:Detcurso:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Detcurso entity.
     *
     * @Route("/{id}/update", name="detcurso_update")
     * @Method("post")
     * @Template("GitekUdaBundle:Detcurso:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Detcurso')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Detcurso entity.');
        }

        $editForm   = $this->createForm(new DetcursoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bind($request);

        if ($editForm->isValid()) {

            $detcurso = $em->getRepository('GitekUdaBundle:Detcurso')->find($id);
            $curso_id = $detcurso->getCursoid();

            $em->persist($entity);
            $em->flush();

            // return $this->redirect($this->generateUrl('detcurso_edit', array('id' => $id)));
            return $this->redirect($this->generateUrl('curso_show', array('id'=>$curso_id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }


    /**
     *
     * @Route("/", name="copy")
     */
    public function copyAction()
    {
        $em = $this->getDoctrine()->getManager();

				//cogemos el curso_id
				$curso_id = $this->get('request')->query->get('curso_id');
				$detcurso_id = $this->get('request')->query->get('detcurso_id');
				// print_r("Copy1 curso_id: " . $curso_id);
				// print_r("<br>");
				// print_r("Copy1 formacion_id: " . $formacion_id);
				// print_r("<br>");
				// print_r("Copy1 detcurso_id: " . $detcurso_id);
				// print_r("<br>");

				$cursos  = $em->getRepository('GitekUdaBundle:Curso')->findAll();
				$operaciones = $em->getRepository('GitekUdaBundle:Detcurso')->findAll();

        return $this->render('GitekUdaBundle:Detcurso:copy.html.twig', array(
          'cursos' => $cursos,
          'operaciones' => $operaciones,
          'curso_id'=>$curso_id,
        ));

        // return new Response(200,"OK",array(
        //   'cursos' => $cursos,
        //   'operaciones' => $operaciones,
        //   'curso_id'=>$curso_id,

        // ));
    //     return array(
				// 	'cursos' => $cursos,
				// 	'operaciones' => $operaciones,
				// 	'curso_id'=>$curso_id,

				// );
    }

	     /**
	     *
	     * @Route("/", name="copy2")
	     * @Template()
	     */
	public function copy2Action()
	{
				$request = $this->getRequest();

				//Obtenemos los seleccionados (Tenemos su ID)
				$miarray = $request->request->get('seleccionados');

				$em = $this->getDoctrine()->getManager();

				//Aqui tenemos el ID del curso
				$mi_curso = $request->request->get('curso_id');

				print_r("Curso_id:".$mi_curso);
				print_r("<br>");

				$cont=0;

				$micursocomodin = $this->getCurso($mi_curso);

		  	foreach ($miarray as $m) {
					$cont+=1;


						$c = $this->getDatosoperacion($m);

                        $comodines = $this->getSiguienteorden($mi_curso);

                        if (count($comodines)==0) {
                            $comodin=0;
                        } else {
                            $comodin = $comodines[0];
                        }


						$midetcurso = New Detcurso();
						$midetcurso->setNombre($c->getNombre());
                        if ($comodin==0) {
                            $midetcurso->setOrden(1);
                        } else {
                            $midetcurso->setOrden($comodin->getOrden()+1);
                        }

						$miformacioncomodin = $this->getFormacion($c->getFormacionId());
                        $miformacion = new Formacion();
                        $miformacion->setNombre($miformacioncomodin->getNombre());
                        $miformacion->setVersion($miformacioncomodin->getVersion());
                        $miformacion->setOrden($miformacioncomodin->getOrden());
                        foreach ($miformacioncomodin->getDetformaciones() as $f) {
                            $midetformacion = new Detformacion();
                            $midetformacion->setFormacion($miformacion);
                            $midetformacion->setNombre($f->getNombre());
                            $midetformacion->setOrden($f->getOrden());
                            $midetformacion->setTexto($f->getTexto());
                            $midetformacion->setImagen($f->getImagen());
                            $midetformacion->setAudio($f->getAudio());
                            $midetformacion->setVideo($f->getVideo());
                            $midetformacion->setUrl($f->getUrl());
                            $em->persist($midetformacion);
                            $miformacion->addDetformacion($f);
                        }
                        $em->persist($miformacion);

						$midetcurso->setFormacion($miformacion);
						$mientrenamientocomodin = $this->getEntrenamiento($c->getEntrenamientoId());
                        $mientrenamiento = new Entrenamiento();
                        $mientrenamiento->setNombre($mientrenamientocomodin->getNombre());
                        $mientrenamiento->setVersion($mientrenamientocomodin->getVersion());
                        $mientrenamiento->setOrden($mientrenamientocomodin->getOrden());
                        foreach ($mientrenamientocomodin->getDetentrenamientos() as $e) {
                            print_r("-Izena: " . $e->getNombre());
                            $midetentrenamiento = new Detentrenamiento();
                            $midetentrenamiento->setDetformacion($e->getDetformacion());
                            $midetentrenamiento->setEntrenamiento($mientrenamiento);
                            $midetentrenamiento->setTipoentrenamiento($e->getTipoentrenamiento());
                            $midetentrenamiento->setNombre($e->getNombre());
                            $midetentrenamiento->setPregunta($e->getPregunta());
                            $midetentrenamiento->setPregunta2($e->getPregunta2());
                            $midetentrenamiento->setPregunta3($e->getPregunta3());
                            $midetentrenamiento->setPregunta4($e->getPregunta4());
                            $midetentrenamiento->setImg1($e->getImg1());
                            $midetentrenamiento->setImg2($e->getImg2());
                            $midetentrenamiento->setImg3($e->getImg3());
                            $midetentrenamiento->setImgok($e->getImgok());
                            $midetentrenamiento->setImgepi1($e->getImgepi1());
                            $midetentrenamiento->setImgepi2($e->getImgepi2());
                            $midetentrenamiento->setImgepi3($e->getImgepi3());
                            $midetentrenamiento->setImgepiok($e->getImgepiok());
                            $midetentrenamiento->setImgcomponente1($e->getImgcomponente1());
                            $midetentrenamiento->setImgcomponente2($e->getImgcomponente2());
                            $midetentrenamiento->setImgcomponente3($e->getImgcomponente3());
                            $midetentrenamiento->setImgcomponenteok($e->getImgcomponenteok());
                            $midetentrenamiento->setImgrotar1($e->getImgrotar1());
                            $midetentrenamiento->setImgrotar2($e->getImgrotar2());
                            $midetentrenamiento->setImgrotar3($e->getImgrotar3());
                            $midetentrenamiento->setImgrotarok($e->getImgrotarok());
                            $midetentrenamiento->setImgbase($e->getImgbase());
                            $midetentrenamiento->setCoordenadas($e->getCoordenadas());
                            $midetentrenamiento->setCoordenadas2($e->getCoordenadas2());
                            $midetentrenamiento->setCoordenadas3($e->getCoordenadas3());
                            $midetentrenamiento->setCoordenadasok($e->getCoordenadasok());
                            $midetentrenamiento->setOrden($e->getOrden());
                            $em->persist($midetentrenamiento);
                            $mientrenamiento->addDetentrenamiento($e);
                        }
                        $em->persist($mientrenamiento);

						$midetcurso->setEntrenamiento($mientrenamiento);
						$midetcurso->setCurso($micursocomodin);

						$em->persist($midetcurso);


				}

	           $em->flush();

			return $this->redirect($this->generateUrl('curso_show', array('id' => $mi_curso)));
	    }

	   protected function getCurso($id)
	    {
	        $em = $this->getDoctrine()->getManager();
	        $curso = $em->getRepository('GitekUdaBundle:Curso')->find($id);
	        if (!$curso) {
	            throw $this->createNotFoundException('Unable to find curso post.');
	        }
	        return $curso;
	    }

	   protected function getFormacion($id)
	    {
	        $em = $this->getDoctrine()->getManager();
	        $formacion = $em->getRepository('GitekUdaBundle:Formacion')->find($id);
	        if (!$formacion) {
	            throw $this->createNotFoundException('Unable to find curso post.');
	        }
	        return $formacion;
	    }

	   protected function getEntrenamiento($id)
	    {
	        $em = $this->getDoctrine()->getManager();
	        $entrenamiento = $em->getRepository('GitekUdaBundle:Entrenamiento')->find($id);
	        if (!$entrenamiento) {
	            throw $this->createNotFoundException('Unable to find curso post.');
	        }
	        return $entrenamiento;
	    }

	   protected function getDatosoperacion($miid)
	    {
	        $em = $this->getDoctrine()->getManager();
	        $form = $em->getRepository('GitekUdaBundle:Detcurso')->find($miid);
	        if (!$form) {
	            throw $this->createNotFoundException('Unable to find detcurso post.');
	        }
	        return $form;
	    }

        protected function getSiguienteorden($curso_id) {
            $em = $this->getDoctrine()->getManager();

            $consulta = $em->createQuery(
                    "SELECT d FROM GitekUdaBundle:Detcurso d
                    WHERE d.curso_id = :cursoid
                    ORDER BY d.orden DESC
                    ");
            $consulta->setParameter('cursoid',$curso_id);
            $consulta->setMaxResults(1);

            $det = $consulta->getResult();

            return $det;

        }
    /**
     * Lists all Entrenamiento entities.
     *
     * @Route("/", name="find")
     * @Template()
     */
    public function findAction()
    {
        $em = $this->getDoctrine()->getManager();

		$id = $this->get('request')->query->get('data');

		$em = $this->getDoctrine()->getManager();

		if (($id!=null) && ($id!="0")) {
			$consulta = $em->createQuery(
					"SELECT e FROM GitekUdaBundle:Detcurso e
					WHERE e.curso_id = :micursoid")
					->setParameter('micursoid',$id);
					$operaciones = $consulta->getResult();

		} else {
			$operaciones = $em->getRepository('GitekUdaBundle:Detcurso')->findAll();
		}

		// var_dump($operacion);
        return array('operaciones' => $operaciones);
    }

    /**
     * Deletes a Detcurso entity.
     *
     * @Route("/{id}/delete", name="detcurso_delete")
     * @Method("post")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getManager();
        $detcurso = $em->getRepository('GitekUdaBundle:Detcurso')->find($id);

        $curso_id = $detcurso->getCurso()->getId();

        $form->bind($request);


        // if ($form->isValid()) {

            if (!$detcurso) {
                throw $this->createNotFoundException('Unable to find Detcurso entity.');
            }

            $em->remove($detcurso);
            $em->flush();
        // }


        $this->get('session')->getFlashBag()->add('notice', 'Detalle de curso eliminado correctamente.');

        return $this->redirect($this->generateUrl('curso_show', array('id'=>$curso_id)));
    }


    protected function getDetcurso($detcurso_id)
    {
        $em = $this->getDoctrine()
                    ->getEntityManager();

        $detcurso = $em->getRepository('GitekUdaBundle:Detcurso')->find($detcurso_id);

        if (!$detcurso) {
            throw $this->createNotFoundException('Unable to find detcurso post.');
        }

        return $detcurso;
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    public function upAction($curso_id, $detcurso_id) {
        $em = $this->getDoctrine()->getManager();

        $detcurso = $em->getRepository('GitekUdaBundle:Detcurso')->find($detcurso_id);

        $ordena = $detcurso->getOrden();

        if ( ( $ordena == null) || ( $ordena == 0 )) {
            $ordena = 0;
        } else {
            $ordena = $ordena-1;
        }

        //actualizamos
        $detcursos = $em->getRepository('GitekUdaBundle:Detcurso')
                        ->getOperaciones($curso_id);

        foreach ($detcursos as $det) {
            if ( $det->getOrden() == $ordena ) {
                $det->setOrden($det->getOrden()+1);
                $em->persist($detcurso);
            }

        }
        $detcurso->setOrden($ordena);
        $em->persist($detcurso);
        $em->flush();

        return $this->redirect($this->generateUrl('curso_show', array('id'=>$curso_id)));
    }

    public function downAction($curso_id, $detcurso_id) {
        $em = $this->getDoctrine()->getManager();

        $detcurso = $em->getRepository('GitekUdaBundle:Detcurso')->find($detcurso_id);

        $ordena = $detcurso->getOrden();

        if ( ( $ordena == null) || ( $ordena == 0 )) {
            $ordena = 0;
        } else {
            $ordena = $ordena+1;
        }

        //actualizamos
        $detcursos = $em->getRepository('GitekUdaBundle:Detcurso')
                        ->getOperaciones($curso_id);

        foreach ($detcursos as $det) {

            if ( $det->getOrden() == $ordena ) {
                $det->setOrden($det->getOrden()-1);
                $em->persist($detcurso);
            }
        }
        $detcurso->setOrden($ordena);
        $em->persist($detcurso);
        $em->flush();

        return $this->redirect($this->generateUrl('curso_show', array('id'=>$curso_id)));
    }


}
