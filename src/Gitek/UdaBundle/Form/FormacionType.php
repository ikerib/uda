<?php

namespace Gitek\UdaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class FormacionType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('version')
        ;
    }


    public function getName()
    {
        return 'gitek_udabundle_formaciontype';
    }
}
