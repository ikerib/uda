<?php

namespace Gitek\UdaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\UdaBundle\Entity\Curso;
use Gitek\UdaBundle\Entity\Detcurso;
use Gitek\UdaBundle\Entity\Formacion;
use Gitek\UdaBundle\Entity\Entrenamiento;

class DetcursoFixtures extends AbstractFixture implements OrderedFixtureInterface
{
	public function load($manager)
	    {

	        $Detcurso1 = new Detcurso();
			$Detcurso1->setNombre('1-Primera operación');
			$Detcurso1->setOrden(0);
			$Detcurso1->setCurso($manager->merge($this->getReference('curso1')));
			$Detcurso1->setFormacion($manager->merge($this->getReference('formacion1')));
			$Detcurso1->setEntrenamiento($manager->merge($this->getReference('entrenamiento1')));
	        $manager->persist($Detcurso1);
	
	        $manager->flush();

	    }

	    public function getOrder()
	    {
	        return 7; // el orden en el que se deben cargar los accesorios
	    }
}