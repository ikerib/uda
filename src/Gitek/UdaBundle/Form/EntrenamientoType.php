<?php

namespace Gitek\UdaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class EntrenamientoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('version')
			->add('tipoentrenamiento')
        ;
    }

    public function getName()
    {
        return 'gitek_udabundle_entrenamientotype';
    }
}
