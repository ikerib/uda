<?php

namespace Gitek\UdaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\UdaBundle\Entity\Entrenamiento;
use Gitek\UdaBundle\Entity\Tipoentrenamiento;

class EntrenamientoFixtures extends AbstractFixture implements OrderedFixtureInterface
{
	public function load($manager)
	    {
	    	
	        $entrenamiento1 = new Entrenamiento();
	        $entrenamiento1->setNombre('1-Colocar Subconjunto Potting');
	        $manager->persist($entrenamiento1);

	
	        $manager->flush();

	        $this->addReference('entrenamiento1', $entrenamiento1);

	    }

	    public function getOrder()
	    {
	        return 4; // el orden en el que se deben cargar los accesorios
	    }
}