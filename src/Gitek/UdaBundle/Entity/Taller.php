<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gitek\UdaBundle\Entity\Taller
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Taller
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

	/**
     * @ORM\OneToMany(targetEntity="Historial", mappedBy="taller")
     */
    private $talleres;   


    public function __construct()
    {
        $this->historiales = new \Doctrine\Common\Collections\ArrayCollection();
        $this->operariostaller = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set txindoki_id
     *
     * @param integer $txindokiId
     */
    public function setTxindokiId($txindokiId)
    {
        $this->txindoki_id = $txindokiId;
    }

    /**
     * Get txindoki_id
     *
     * @return integer 
     */
    public function getTxindokiId()
    {
        return $this->txindoki_id;
    }

    /**
     * Add historiales
     *
     * @param Gitek\UdaBundle\Entity\Historial $historiales
     */
    public function addHistorial(\Gitek\UdaBundle\Entity\Historial $historiales)
    {
        $this->historiales[] = $historiales;
    }

    /**
     * Get historiales
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getHistoriales()
    {
        return $this->historiales;
    }

    /**
     * Add operariostaller
     *
     * @param Gitek\UdaBundle\Entity\Operario $operariostaller
     */
    public function addOperario(\Gitek\UdaBundle\Entity\Operario $operariostaller)
    {
        $this->operariostaller[] = $operariostaller;
    }

    /**
     * Get operariostaller
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getOperariostaller()
    {
        return $this->operariostaller;
    }

    /**
     * Get talleres
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getTalleres()
    {
        return $this->talleres;
    }

    /**
     * Add talleres
     *
     * @param \Gitek\UdaBundle\Entity\Historial $talleres
     * @return Taller
     */
    public function addTallere(\Gitek\UdaBundle\Entity\Historial $talleres)
    {
        $this->talleres[] = $talleres;
    
        return $this;
    }

    /**
     * Remove talleres
     *
     * @param \Gitek\UdaBundle\Entity\Historial $talleres
     */
    public function removeTallere(\Gitek\UdaBundle\Entity\Historial $talleres)
    {
        $this->talleres->removeElement($talleres);
    }
}