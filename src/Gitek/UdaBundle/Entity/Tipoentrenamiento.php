<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gitek\UdaBundle\Entity\Tipoentrenamiento
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Tipoentrenamiento
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $tipo
     *
     * @ORM\Column(name="tipo", type="string", length=255)
     */
    private $tipo;

	/**
     * @ORM\OneToMany(targetEntity="detentrenamiento", mappedBy="tipoentrenamiento", cascade={"remove"})
     */
    protected $entrenamientos;

    public function __toString()
    {
        return $this->getTipo();
    }




    public function __construct()
    {
        $this->entrenamientos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Add entrenamientos
     *
     * @param Gitek\UdaBundle\Entity\Detentrenamiento $entrenamientos
     */
    public function adddetentrenamiento(\Gitek\UdaBundle\Entity\Detentrenamiento $entrenamientos)
    {
        $this->entrenamientos[] = $entrenamientos;
    }

    /**
     * Get entrenamientos
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getEntrenamientos()
    {
        return $this->entrenamientos;
    }

    /**
     * Add entrenamientos
     *
     * @param \Gitek\UdaBundle\Entity\detentrenamiento $entrenamientos
     * @return Tipoentrenamiento
     */
    public function addEntrenamiento(\Gitek\UdaBundle\Entity\detentrenamiento $entrenamientos)
    {
        $this->entrenamientos[] = $entrenamientos;

        return $this;
    }

    /**
     * Remove entrenamientos
     *
     * @param \Gitek\UdaBundle\Entity\detentrenamiento $entrenamientos
     */
    public function removeEntrenamiento(\Gitek\UdaBundle\Entity\detentrenamiento $entrenamientos)
    {
        $this->entrenamientos->removeElement($entrenamientos);
    }
}