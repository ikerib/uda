<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gitek\UdaBundle\Entity\Operario
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\UdaBundle\Entity\OperarioRepository")
 */
class Operario
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string $apellidos
     *
     * @ORM\Column(name="apellidos", type="string", length=255)
     */
    private $apellidos;

    /**
     * @var integer $txindoki_id
     *
     * @ORM\Column(name="txindoki_id", type="integer", nullable=true)
     */
    private $txindoki_id;

    /**
     * @var integer $taller_id
     *
     * @ORM\Column(name="taller_id", type="integer", nullable=true)
     */
    private $taller_id;

    /**
     * @var boolean $escomodin
     *
     * @ORM\Column(name="escomodin", type="boolean", nullable=true)
     */
    private $escomodin;

        /**
     * @ORM\OneToMany(targetEntity="Historial", mappedBy="operario")
     */
    private $historiales;

    /**
     * @ORM\ManyToOne(targetEntity="Taller",inversedBy="talleres")
     * @ORM\JoinColumn(name="taller_id", referencedColumnName="id")
     */
    private $taller;

    
    public function __construct()
    {
        $this->historiales = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set txindoki_id
     *
     * @param integer $txindokiId
     */
    public function setTxindokiId($txindokiId)
    {
        $this->txindoki_id = $txindokiId;
    }

    /**
     * Get txindoki_id
     *
     * @return integer 
     */
    public function getTxindokiId()
    {
        return $this->txindoki_id;
    }

    /**
     * Set taller_id
     *
     * @param integer $tallerId
     */
    public function setTallerId($tallerId)
    {
        $this->taller_id = $tallerId;
    }

    /**
     * Get taller_id
     *
     * @return integer 
     */
    public function getTallerId()
    {
        return $this->taller_id;
    }

    /**
     * Set escomodin
     *
     * @param boolean $escomodin
     */
    public function setEscomodin($escomodin)
    {
        $this->escomodin = $escomodin;
    }

    /**
     * Get escomodin
     *
     * @return boolean 
     */
    public function getEscomodin()
    {
        return $this->escomodin;
    }

    /**
     * Add historiales
     *
     * @param Gitek\UdaBundle\Entity\Historial $historiales
     */
    public function addHistorial(\Gitek\UdaBundle\Entity\Historial $historiales)
    {
        $this->historiales[] = $historiales;
    }

    /**
     * Get historiales
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getHistoriales()
    {
        return $this->historiales;
    }

    /**
     * Set taller
     *
     * @param Gitek\UdaBundle\Entity\Taller $taller
     */
    public function setTaller(\Gitek\UdaBundle\Entity\Taller $taller)
    {
        $this->taller = $taller;
    }

    /**
     * Get taller
     *
     * @return Gitek\UdaBundle\Entity\Taller 
     */
    public function getTaller()
    {
        return $this->taller;
    }

    /**
     * Add historiales
     *
     * @param \Gitek\UdaBundle\Entity\Historial $historiales
     * @return Operario
     */
    public function addHistoriale(\Gitek\UdaBundle\Entity\Historial $historiales)
    {
        $this->historiales[] = $historiales;
    
        return $this;
    }

    /**
     * Remove historiales
     *
     * @param \Gitek\UdaBundle\Entity\Historial $historiales
     */
    public function removeHistoriale(\Gitek\UdaBundle\Entity\Historial $historiales)
    {
        $this->historiales->removeElement($historiales);
    }
}