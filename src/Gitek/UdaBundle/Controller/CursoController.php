<?php

namespace Gitek\UdaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Gitek\UdaBundle\Entity\Curso;
use Gitek\UdaBundle\Entity\Detcurso;
use Gitek\UdaBundle\Entity\Formacion;
use Gitek\UdaBundle\Entity\Entrenamiento;
use Gitek\UdaBundle\Form\CursoType;
use Gitek\UdaBundle\Form\DetcursoType;

/**
 * Curso controller.
 *
 * @Route("/curso")
 */
class CursoController extends Controller
{
  /**
   * Lists all Formacion entities.
   *
   * @Route("/", name="curso")
   * @Template()
   */
  public function indexAction()
  {
    $em = $this->getDoctrine()->getManager();

    $laguntzak = $em->getRepository('GitekUdaBundle:Laguntza')->findAll(array("mostrar"=>"1"));

    $entities = $em->getRepository('GitekUdaBundle:Curso')->findAll();

    $user = $this->get('security.context')->getToken()->getUser();



    return $this->render('GitekUdaBundle:Curso:index.html.twig', array(
      'entities' => $entities,
      'laguntzak' => $laguntzak,
      'user'=>$user
      ));
  }

  /**
   * Lists all Formacion entities.
   *
   * @Route("/", name="curso")
   * @Template()
   */
  public function hasieraAction()
  {
    $em = $this->getDoctrine()->getManager();

    $laguntzak = $em->getRepository('GitekUdaBundle:Laguntza')->findAll(array("mostrar"=>"1"));
    return $this->render('GitekUdaBundle:Curso:hasiera.html.twig', array(
      'laguntzak'=>$laguntzak,
      ));
  }

    /**
     * Finds and displays a Formacion entity.
     *
     * @Route("/{id}/show", name="curso_show")
     * @Template()
     */
    public function showAction($id)
    {
      $em = $this->getDoctrine()->getManager();

      $curso = $em->getRepository('GitekUdaBundle:Curso')->find($id);
      $asterisko = null;
      $erroreak = $this->bilatuerroreak($curso);

      if ( $erroreak != null )
        $asterisko = "*";

      $laguntzak = $em->getRepository('GitekUdaBundle:Laguntza')->findAll();

      if (!$curso) {
        throw $this->createNotFoundException('Unable to find Curso entity.');
      }

      $deleteForm = $this->createDeleteForm($id);

		// Detalle Formación
      $detcurso = new Detcurso();
      $formdetcurso   = $this->createForm(new DetcursoType(), $detcurso);
      $session = $this->getRequest()->getSession();
      $session->set('curso_nombre', $curso->getNombre());
      return $this->render('GitekUdaBundle:Curso:show.html.twig', array(
        'curso'      => $curso,
        'laguntzak' => $laguntzak,
        'delete_form' => $deleteForm->createView(),
        'detcurso'      => $detcurso,
        'formdetcurso' => $formdetcurso,
        'erroreak'=>$erroreak,
        'asterisko' => $asterisko
        ));
    }

    /**
     * Displays a form to create a new Formacion entity.
     *
     * @Route("/new", name="curso_new")
     * @Template()
     */
    public function newAction()
    {
      $curso = new Curso();
      $formcurso   = $this->createForm(new CursoType(), $curso);

      $em = $this->getDoctrine()->getManager();
      $laguntzak = $em->getRepository('GitekUdaBundle:Laguntza')->findAll(array("mostrar"=>"1"));

      return $this->render('GitekUdaBundle:Curso:new.html.twig', array(
        'curso' => $curso,
        'laguntzak' => $laguntzak,
        'formcurso'   => $formcurso->createView()
        ));
    }

    /**
     * Creates a new Formacion entity.
     *
     * @Route("/create", name="curso_create")
     * @Method("post")
     * @Template("GitekUdaBundle:Curso:new.html.twig")
     */
    public function createAction(Request $request)
    {
      $curso  = new Curso();
      $form    = $this->createForm(new CursoType(), $curso);
      $form->bind($request);

      if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $em->persist($curso);
        $em->flush();

        return $this->redirect($this->generateUrl('curso_show', array('id' => $curso->getId())));

      }

      return $this->render('GitekUdaBundle:Curso:new.html.twig', array(
        'entity' => $curso,
        'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Formacion entity.
     *
     * @Route("/{id}/edit", name="curso_edit")
     * @Template()
     */
    public function editAction($id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('GitekUdaBundle:Curso')->find($id);

      $laguntzak = $em->getRepository('GitekUdaBundle:Laguntza')->findAll(array("mostrar"=>"1"));

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Curso entity.');
      }

      $editForm = $this->createForm(new CursoType(), $entity);
      $deleteForm = $this->createDeleteForm($id);

      return $this->render('GitekUdaBundle:Curso:edit.html.twig', array(
        'entity'      => $entity,
        'laguntzak'     => $laguntzak,
        'edit_form'   => $editForm->createView(),
        'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Formacion entity.
     *
     * @Route("/{id}/update", name="curso_update")
     * @Method("post")
     * @Template("GitekUdaBundle:Curso:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('GitekUdaBundle:Curso')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Curso entity.');
      }

      $editForm   = $this->createForm(new CursoType(), $entity);
      $deleteForm = $this->createDeleteForm($id);

      $request = $this->getRequest();

      $editForm->bind($request);

      if ($editForm->isValid()) {
        $em->persist($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('curso_edit', array('id' => $id)));
      }

      return array(
        'entity'      => $entity,
        'edit_form'   => $editForm->createView(),
        'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Formacion entity.
     *
     * @Route("/{id}/delete", name="curso_delete")
     * @Method("post")
     */
    public function deleteAction(Request $request, $id)
    {
      $form = $this->createDeleteForm($id);
      $request = $this->getRequest();

      $form->bind($request);

        // if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $entity = $em->getRepository('GitekUdaBundle:Curso')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Curso entity.');
      }

      $em->remove($entity);
      $em->flush();
        // }

      return $this->redirect($this->generateUrl('administrazioa'));
    }

    private function createDeleteForm($id)
    {
      return $this->createFormBuilder(array('id' => $id))
      ->add('id', 'hidden')
      ->getForm()
      ;
    }


    public function protegerAction($id)
    {
      $em = $this->getDoctrine()->getManager();

      $entity = $em->getRepository('GitekUdaBundle:Curso')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Curso entity.');
      }

      if ( $entity->getProtegido() == null ) {
        $entity->setProtegido(true);
      } else {
        $entity->setProtegido(null);
      }
      $em->persist($entity);
      $em->flush();

      return $this->redirect($this->generateUrl('curso'));
    }

    public function bilatuerroreak($curso) {

      if ( count( $curso->getDetcursos() ) == 0 ) {
        return "No existen Operaciones";
      }


      foreach ($curso->getDetcursos() as $detcurso) {

        if ( $detcurso->getFormacion() == null ) {
          return "No hay formaciones.";
        }

        if ( count( $detcurso->getFormacion()->getDetformaciones() ) == 0) {
          return "No existen formaciones.";
        }

        // FORMACIONES
        foreach ($detcurso->getFormacion()->getDetformaciones() as $detformacion) {
          $dago = 0;
          if ( ( $detformacion->getImagen() != "" ) && ( $detformacion->getImagen() != null ) ) {
            $dago = 1;
          }
          if ( ( $detformacion->getAudio() != "" ) && ( $detformacion->getAudio() != null ) ) {
            $dago = 1;
          }
          if ( ( $detformacion->getVideo() != "" ) && ( $detformacion->getVideo() != null ) ) {
            $dago = 1;
          }
          if ( ( $detformacion->getUrl() != "" ) && ( $detformacion->getUrl() != null ) ) {
            $dago = 1;
          }
          if ( $dago == 0 ) {
            return "Falta completar la formación.";
          }
        }

        // SIMULACIONES
        foreach ($detcurso->getEntrenamiento()->getDetentrenamientos() as $detentrenamiento) {

          if ( $detentrenamiento->getTipoentrenamiento()->getTipo() == "X7" ) {

            //Preguntas
            if ( $detentrenamiento->getPregunta()==null ) {
              return "Especifica pregunta1 en simulación : " . $detentrenamiento->getNombre();
            }
            if ( $detentrenamiento->getPregunta2()==null ) {
              return "Especifica pregunta2 en simulación : " . $detentrenamiento->getNombre();
            }
            if ( $detentrenamiento->getPregunta3()==null ) {
              return "Especifica pregunta3 en simulación : " . $detentrenamiento->getNombre();
            }

            // Componente correcto
            if ( ($detentrenamiento->getImg1() == "") || ( $detentrenamiento->getImg1() == null) ) {
              return "Seleccione Imagen 1 del apartado 'Componente Correcto' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImg2() == "") || ( $detentrenamiento->getImg2() == null) ) {
              return "Seleccione Imagen 2 del apartado 'Componente Correcto' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImg3() == "") || ( $detentrenamiento->getImg3() == null) ) {
              return "Seleccione Imagen 3 del apartado 'Componente Correcto' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgok() == "") || ( $detentrenamiento->getImgok() == null) ) {
              return "Seleccione Imagen OK del apartado 'Componente Correcto' en simulación : " . $detentrenamiento->getNombre();
            }

            // EPI
            if ( ($detentrenamiento->getImgepi1() == "") || ( $detentrenamiento->getImgepi1() == null) ) {
              return "Seleccione Imagen 1 del apartado 'Epi Conforme' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgepi2() == "") || ( $detentrenamiento->getImgepi2() == null) ) {
              return "Seleccione Imagen 2 del apartado 'Epi Conforme' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgepi3() == "") || ( $detentrenamiento->getImgepi3() == null) ) {
              return "Seleccione Imagen 3 del apartado 'Epi Conforme' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgepiok() == "") || ( $detentrenamiento->getImgepiok() == null) ) {
              return "Seleccione Imagen OK del apartado 'Epi Conforme' en simulación : " . $detentrenamiento->getNombre();
            }

            // Componente no defectuoso
            if ( ($detentrenamiento->getImgcomponente1() == "") || ( $detentrenamiento->getImgcomponente1() == null) ) {
              return "Seleccione Imagen 1 del apartado 'Componente NO defectuoso' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgcomponente2() == "") || ( $detentrenamiento->getImgcomponente2() == null) ) {
              return "Seleccione Imagen 2 del apartado 'Componente NO defectuoso' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgcomponente3() == "") || ( $detentrenamiento->getImgcomponente3() == null) ) {
              return "Seleccione Imagen 3 del apartado 'Componente NO defectuoso' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgcomponenteok() == "") || ( $detentrenamiento->getImgcomponenteok() == null) ) {
              return "Seleccione Imagen OK del apartado 'Componente NO defectuoso' en simulación : " . $detentrenamiento->getNombre();
            }

            // Colocar componente
            if ( ($detentrenamiento->getImgrotar1() == "") || ( $detentrenamiento->getImgrotar1() == null) ) {
              return "Seleccione Imagen 1 del apartado 'Colocar componente' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgrotar2() == "") || ( $detentrenamiento->getImgrotar2() == null) ) {
              return "Seleccione Imagen 2 del apartado 'Colocar componente' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgrotar3() == "") || ( $detentrenamiento->getImgrotar3() == null) ) {
              return "Seleccione Imagen 3 del apartado 'Colocar componente' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgrotarok() == "") || ( $detentrenamiento->getImgrotarok() == null) ) {
              return "Seleccione Imagen OK del apartado 'Colocar componente' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgbase() == "") || ( $detentrenamiento->getImgbase() == null) ) {
              return "Seleccione la imagen de la petaca en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getCoordenadasok() == "") || ( $detentrenamiento->getCoordenadasok() == null) ) {
              return "Al menos selecciona un area correcta en simulación : " . $detentrenamiento->getNombre();
            }
          } elseif ( $detentrenamiento->getTipoentrenamiento()->getTipo() == "Colocar componente" ) {
            if ( $detentrenamiento->getPregunta3()==null ) {
              return "Especifica pregunta3 en simulación : " . $detentrenamiento->getNombre();
            }
            // Colocar componente
            if ( ($detentrenamiento->getImgrotar1() == "") || ( $detentrenamiento->getImgrotar1() == null) ) {
              return "Seleccione Imagen 1 del apartado 'Colocar componente' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgrotar2() == "") || ( $detentrenamiento->getImgrotar2() == null) ) {
              return "Seleccione Imagen 2 del apartado 'Colocar componente' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgrotar3() == "") || ( $detentrenamiento->getImgrotar3() == null) ) {
              return "Seleccione Imagen 3 del apartado 'Colocar componente' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgrotarok() == "") || ( $detentrenamiento->getImgrotarok() == null) ) {
              return "Seleccione Imagen OK del apartado 'Colocar componente' en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgbase() == "") || ( $detentrenamiento->getImgbase() == null) ) {
              return "Seleccione la imagen de la petaca en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getCoordenadasok() == "") || ( $detentrenamiento->getCoordenadasok() == null) ) {
              return "Al menos selecciona un area correcta en simulación : " . $detentrenamiento->getNombre();
            }
          } elseif ( $detentrenamiento->getTipoentrenamiento()->getTipo() == "4-Imagen" ) {
            if ( $detentrenamiento->getPregunta2()==null ) {
              return "Especifica pregunta en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImg1() == "") || ( $detentrenamiento->getImg1() == null) ) {
              return "Seleccione Imagen 1 en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImg2() == "") || ( $detentrenamiento->getImg2() == null) ) {
              return "Seleccione Imagen 2 en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImg3() == "") || ( $detentrenamiento->getImg3() == null) ) {
              return "Seleccione Imagen 3 en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgok() == "") || ( $detentrenamiento->getImgok() == null) ) {
              return "Seleccione Imagen OK en simulación : " . $detentrenamiento->getNombre();
            }
          } elseif ( $detentrenamiento->getTipoentrenamiento()->getTipo() == "1-Imagen" ) {
            if ( $detentrenamiento->getPregunta3()==null ) {
              return "Especifica pregunta en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImg1() == "") || ( $detentrenamiento->getImg1() == null) ) {
              return "Seleccione Imagen 1 en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImg2() == "") || ( $detentrenamiento->getImg2() == null) ) {
              return "Seleccione Imagen 2 en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImg3() == "") || ( $detentrenamiento->getImg3() == null) ) {
              return "Seleccione Imagen 3 en simulación : " . $detentrenamiento->getNombre();
            }
            if ( ($detentrenamiento->getImgok() == "") || ( $detentrenamiento->getImgok() == null) ) {
              return "Seleccione Imagen OK en simulación : " . $detentrenamiento->getNombre();
            }
          }
        }

      }

      return false;

    }

  }
