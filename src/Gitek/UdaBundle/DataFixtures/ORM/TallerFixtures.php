<?php

namespace Gitek\UdaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\UdaBundle\Entity\Taller;

class TallerFixtures extends AbstractFixture implements OrderedFixtureInterface
{
	public function load($manager)
	    {
	    	
	        $taller1 = new Taller();
	        $taller1->setNombre('ZZZZ');
	        $manager->persist($taller1);

	        $this->addReference('taller1', $taller1);
	    }

	    public function getOrder()
	    {
	        return 8; // el orden en el que se deben cargar los accesorios
	    }
}