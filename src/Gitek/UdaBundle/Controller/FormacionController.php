<?php

namespace Gitek\UdaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Gitek\UdaBundle\Entity\Formacion;
use Gitek\UdaBundle\Entity\Detformacion;
use Gitek\UdaBundle\Entity\Detcurso;
use Gitek\UdaBundle\Form\FormacionType;
use Gitek\UdaBundle\Form\DetformacionType;
/**
 * Formacion controller.
 *
 * @Route("/formacion")
 */
class FormacionController extends Controller
{
    /**
     * Lists all Formacion entities.
     *
     * @Route("/", name="formacion")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $formaciones = $em->getRepository('GitekUdaBundle:Formacion')->getLatestFormaciones();

        return $this->render('GitekUdaBundle:Formacion:index.html.twig', array(
            'formaciones' => $formaciones
        ));
    }

    /**
     * Lists all Entrenamiento entities.
     *
     * @Route("/", name="find")
     * @Template()
     */
    public function findAction()
    {
        $em = $this->getDoctrine()->getManager();

				$id = $this->get('request')->query->get('data');

				$em = $this->getDoctrine()->getManager();

				if (($id!=null) && ($id!="0")) {
					$consulta = $em->createQuery(
							"SELECT e FROM GitekUdaBundle:Detformacion e
							WHERE e.formacion_id = :miformacionid")
							->setParameter('miformacionid',$id);
					$formaciones = $consulta->getResult();

				} else {
					$formaciones = $em->getRepository('GitekUdaBundle:Formacion')->findAll();
				}

				// var_dump($cursos);
        return array('formaciones' => $formaciones
					);
    }

   /**
     * Lists all Entrenamiento entities.
     *
     * @Route("/", name="findformaciones")
     * @Template()
     */
    public function findformacionesAction()
    {
        $em = $this->getDoctrine()->getManager();

				$id = $this->get('request')->query->get('data');

				$em = $this->getDoctrine()->getManager();



				if (($id!=null) && ($id!="0")) {
					$consulta = $em->createQuery(
							"SELECT e FROM GitekUdaBundle:Formacion e
							 INNER JOIN e.detcursos d
							WHERE d.curso_id = :micursoid")
							->setParameter('micursoid',$id);
					$formaciones = $consulta->getResult();

					$consulta = $em->createQuery(
							"SELECT f FROM GitekUdaBundle:Detformacion f
							INNER JOIN f.formacion e
 						 	INNER JOIN e.detcursos d
							WHERE d.curso_id = :micursoid")
							->setParameter('micursoid',$id);
					$detformaciones = $consulta->getResult();

				} else {
					$formaciones = $em->getRepository('GitekUdaBundle:Formacion')->findAll();
					$detformaciones = $em->getRepository('GitekUdaBundle:Detformacion')->findAll();
				}

				// var_dump($cursos);
        return array('formaciones' => $formaciones, 'detformaciones' => $detformaciones
					);
    }

   /**
     *
     * @Route("/", name="finddetformaciones")
     * @Template()
     */
    public function finddetformacionesAction()
    {
        $em = $this->getDoctrine()->getManager();

				$id = $this->get('request')->query->get('data');

				$em = $this->getDoctrine()->getManager();



				if (($id!=null) && ($id!="0")) {
					$consulta = $em->createQuery(
							"SELECT f FROM GitekUdaBundle:Detformacion f
							INNER JOIN f.formacion e
 						 	INNER JOIN e.detcursos d
							WHERE d.curso_id = :micursoid")
							->setParameter('micursoid',$id);
					$formaciones = $consulta->getResult();

				} else {

					$formaciones = $em->getRepository('GitekUdaBundle:Detformacion')->findAll();
				}

				// var_dump($cursos);
        return array('formaciones' => $formaciones
					);
    }

    /**
     * Lists all Formacion entities.
     *
     * @Route("/", name="copy1")
     */
    public function copy1Action()
    {
        $em = $this->getDoctrine()->getManager();

				//cogemos el curso_id
				$curso_id 		= $this->get('request')->query->get('curso_id');
				$formacion_id = $this->get('request')->query->get('formacion_id');
				$detcurso_id 	= $this->get('request')->query->get('detcurso_id');
				$mianchor 		= $this->get('request')->query->get('mianchor');
				$mitab 				= $this->get('request')->query->get('mitab');

				$cursos  = $em->getRepository('GitekUdaBundle:Curso')->findAll();
				$formaciones = $em->getRepository('GitekUdaBundle:Formacion')->getLatestFormaciones();
				$detformaciones = $em->getRepository('GitekUdaBundle:Detformacion')->findAll();

        return $this->render('GitekUdaBundle:Formacion:copy1.html.twig', array(
          'formaciones'   => $formaciones,
          'cursos'        => $cursos,
          'detformaciones'=> $detformaciones,
          'curso_id'      =>$curso_id,
          'formacion_id'  =>$formacion_id,
          'detcurso_id'   => $detcurso_id,
          'mianchor'      => $mianchor,
          'mitab'         => $mitab,
        ));
    }

     /**
     * Lists all Formacion entities.
     *
     * @Route("/", name="copy2")
     * @Template()
     */
    public function copy2Action()
    {
			$request = $this->getRequest();

			//Obtenemos los seleccionados (Tenemos su ID)
			$miarray = $request->request->get('seleccionados');

			$em = $this->getDoctrine()->getManager();

			//Aqui tenemos el ID del curso
			$mi_curso 		= $request->request->get('curso_id');
			//Aqui tenemos el ID del formacion
			$formacion_id = $request->request->get('formacion_id');
			$mianchor 		= $request->request->get('mianchor');
			$mitab 				= $request->request->get('mitab');
			$detcurso_id 	= $request->request->get('detcurso_id');

			//Recorremos los seleccionados y vamos creando
			$cont=0;

			if ($formacion_id ==null)
			{
				//creamos nueva formacion, basandonos en el seleccionado (Copiamos el nombre)
				$miformacion  = new Formacion();
				$f = $em->getRepository('GitekUdaBundle:Formacion')->find($m);
				$miformacion->setNombre($f->getNombre());
				$em->persist($miformacion);

			} else {
				//editamos la formación
				$miformacion  = new Formacion();
				$miformacion = $em->getRepository('GitekUdaBundle:Formacion')->find($formacion_id);
				print_r("La formacion es: " . $miformacion->getNombre());
				print_r("<br>");
			}

		  foreach ($miarray as $m) {
				$cont+=1;

				//copiamos

					$c = $this->getDetalleformacion($m);

					$midet = new Detformacion();
					$midet->setFormacion($miformacion);
					$midet->setOrden($c->getOrden());
                    $midet->setNombre($c->getNombre());
                    $midet->setTexto($c->getTexto());
					$midet->setImagen($c->getImagen());
                    $midet->setAudio($c->getAudio());
					$midet->setVideo($c->getVideo());
                    $midet->setUrl($c->getUrl());
					$em->persist($midet);
					$miformacion->addDetformacion($midet);
					$em->persist($miformacion);


				//obtenemos datos del curso
				$micursocomodin = $this->getCurso($mi_curso);

				if ($detcurso_id!=null) {

				} else{
					//Creamos nuevo DetCurso para el curso
					$midetcurso = New Detcurso();
					$midetcurso->setNombre("Copia $cont");
					$midetcurso->setFormacion($miformacion);
					$midetcurso->setCurso($micursocomodin);
					$em->persist($midetcurso);

				}
			}

      $em->flush();

			// return $this->redirect($this->generateUrl('curso_show', array('id' => $mi_curso)));

			$url = $this->generateUrl('curso_show', array('id' => $mi_curso));
			return $this->redirect(
			    sprintf('%s#%s--%s', $url, $mianchor,$mitab)
			);

    }

   protected function getCurso($id)
    {
        $em = $this->getDoctrine()->getManager();
        $curso = $em->getRepository('GitekUdaBundle:Curso')->find($id);
        if (!$curso) {
            throw $this->createNotFoundException('Unable to find curso post.');
        }
        return $curso;
    }

   protected function getDetcurso($id)
    {
        $em = $this->getDoctrine()->getManager();
        $curso = $em->getRepository('GitekUdaBundle:Detcurso')->find($id);
        if (!$curso) {
            throw $this->createNotFoundException('Unable to find detcurso post.');
        }
        return $curso;
    }

    protected function getDetf($m)
    {
			$repositorio = $this->getDoctrine()
			    ->getRepository('GitekUdaBundle:Detformacion');

			$consulta = $repositorio->createQueryBuilder('p')
			    ->where('p.formacion_id = :miid')
			    ->setParameter('miid', $m)
			    ->getQuery();

			$productos = $consulta->getResult();
			return $productos;
    }


   protected function getDetalleformacion($miid)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $em->getRepository('GitekUdaBundle:Detformacion')->find($miid);
        if (!$form) {
            throw $this->createNotFoundException('Unable to find detcurso post.');
        }
        return $form;
    }

    /**
     * Finds and displays a Formacion entity.
     *
     * @Route("/{id}/show", name="formacion_show")
     * @Template()
     */
    public function showAction($id, $curso_id=null)
    {
        $em = $this->getDoctrine()->getManager();

        $formacion = $em->getRepository('GitekUdaBundle:Formacion')->find($id);

				$curso_id = $this->get('request')->query->get('curso_id');
				$curso = $this->getCurso($curso_id);

				$mianchor = $this->get('request')->query->get('mianchor');
				$mitab = $this->get('request')->query->get('mitab');

				if (!$formacion) {
            throw $this->createNotFoundException('Unable to find Formacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

				// Detalle Formación
        $detformacion = new Detformacion();
        $formdetformacion   = $this->createForm(new DetformacionType(), $detformacion);

        return array(
            'formacion'      		=> $formacion,
            'delete_form' 			=> $deleteForm->createView(),
						'detformacion' 			=> $detformacion,
            'formdetformacion'  => $formdetformacion->createView(),
   					'curso_id'					=> $curso_id,
						'curso'							=> $curso,
						'mianchor'					=> $mianchor,
						'mitab'							=> $mitab,
				);
    }

    /**
     * Displays a form to create a new Formacion entity.
     *
     * @Route("/new", name="formacion_new")
     * @Template()
     */
    public function newAction()
    {
        $formacion = new Formacion();
        $formulario   = $this->createForm(new FormacionType(), $formacion);
				$cursoid = $this->get('request')->query->get('cursoid');

				// var_dump($cursoid);
        return array(
            'formacion' => $formacion,
						'cursoid'	=> $cursoid,
            'formulario'   => $formulario->createView()
        );
    }

    /**
     * Creates a new Formacion entity.
     *
     * @Route("/create", name="formacion_create")
     * @Method("post")
     * @Template("GitekUdaBundle:Formacion:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new Formacion();
        $request = $this->getRequest();
        $form    = $this->createForm(new FormacionType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);

						$cursoid  = $this->get('request')->request->get('cursoid');
						// Si se esta creando desde un curso, primero asociamos la formación al curso, para asi poder acceder al mismo desde 							      curso_show/{id}
						if ($cursoid!=null) {

                            $comodines = $this->getSiguienteorden($cursoid);
                            $comodin = $comodines[0];

                            $detcurso  = new Detcurso();
							$detcurso->setNombre($entity->getNombre());
                            $midetcurso->setOrden($comodin->getOrden()+1);
							$curso = $this->getCurso($cursoid);
							$detcurso->setCurso($curso);
							$detcurso->setFormacion($entity);
							$em->persist($detcurso);
						}

            $em->flush();

            return $this->redirect(
						$this->generateUrl(
									'formacion_show',
									array('id' => $entity->getId(),
										'cursoid' => $cursoid
									)
								)
						);

        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Formacion entity.
     *
     * @Route("/{id}/edit", name="formacion_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Formacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Formacion entity.');
        }

        $editForm = $this->createForm(new FormacionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Formacion entity.
     *
     * @Route("/{id}/update", name="formacion_update")
     * @Method("post")
     * @Template("GitekUdaBundle:Formacion:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Formacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Formacion entity.');
        }

        $editForm   = $this->createForm(new FormacionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('formacion_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Formacion entity.
     *
     * @Route("/{id}/delete", name="formacion_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GitekUdaBundle:Formacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Formacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('formacion'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

		 /**
	     * Edits an existing Formacion entity.
	     *
	     * @Route("/{id}/play", name="formacion_play")
	     * @Template("GitekUdaBundle:Formacion:play.html.twig")
	     */
		public function playAction($id=null){
			$em = $this->getDoctrine()->getManager();
			$repo = $em->getRepository('GitekUdaBundle:Formacion');
			$paginator = $this->get('ideup.simple_paginator');

            $formacion = $em->getRepository('GitekUdaBundle:Formacion')->find($id);

			$paginator->setItemsPerPage(1);
			$detformaciones = $paginator->paginate($repo->getDetformaciones($id))->getResult();

            if (!$formacion) {
                throw $this->createNotFoundException('No se ha encontrado la formación especificada.');
            }
    		if (!$detformaciones) {
                throw $this->createNotFoundException('No se ha encontrado detalles de la formacion.');
            }
			return $this->render('GitekUdaBundle:Formacion:play.html.twig', array(
                'formacion' => $formacion,
                'detformaciones' => $detformaciones,
                'paginator'       => $paginator,
            ));

		}

		public function ikasiAction() {

			$em = $this->getDoctrine()->getManager();
			$repo = $em->getRepository('GitekUdaBundle:Detcurso');
			$paginator = $this->get('ideup.simple_paginator');

			//lo mejor conseguir todas las formaciones mediante sql

		}

		public function volverAction($curso_id=null,$mianchor=null,$mitab=null)
    {
			$url = $this->generateUrl('curso_show', array('id' => $curso_id));
			return $this->redirect(
				sprintf('%s#%s--%s', $url, $mianchor,$mitab)
			);
		}

    public function berriaAction($formacion_id=null,$curso_id=null,$mianchor=null,$mitab=null)
    {
		$em = $this->getDoctrine()->getManager();

		$formacion = $this->getForm($formacion_id);
		$curso = $this->getCurso($curso_id);

        $detformacion = new Detformacion();
		$detformacion->setNombre($formacion->getNombre());
		$detformacion->setFormacion($formacion);

		$em->persist($detformacion);
    	$em->flush();

        return $this->redirect(
			$this->generateUrl('detformacion_edit',
				array(
                    'id'       => $detformacion->getId(),
                    'curso_id' => $curso_id,
                    'mianchor' => $mianchor,
                    'mitab'    => $mitab,
					)
			));
    }

    protected function getForm($m)
    {
        $em = $this->getDoctrine()->getManager();
        $formacion = $em->getRepository('GitekUdaBundle:Formacion')->find($m);
        if (!$formacion) {
            throw $this->createNotFoundException('Unable to find formacion post.');
        }
        return $formacion;
    }

    protected function getSiguienteorden($curso_id) {
        $em = $this->getDoctrine()->getManager();

        $consulta = $em->createQuery(
                "SELECT d FROM GitekUdaBundle:Detcurso d
                WHERE d.curso_id = :cursoid
                ORDER BY d.orden DESC
                ");
        $consulta->setParameter('cursoid',$curso_id);
        $consulta->setMaxResults(1);

        $det = $consulta->getResult();

        return $det;

    }

}
