<?php

namespace Gitek\UdaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Gitek\UdaBundle\Entity\Laguntza;
use Gitek\UdaBundle\Form\LaguntzaType;

/**
 * Laguntza controller.
 *
 */
class LaguntzaController extends Controller
{
    /**
     * Lists all Laguntza entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GitekUdaBundle:Laguntza')->findAll();

        return $this->render('GitekUdaBundle:Laguntza:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Laguntza entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Laguntza')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Laguntza entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GitekUdaBundle:Laguntza:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Laguntza entity.
     *
     */
    public function newAction()
    {
        $entity = new Laguntza();
        $form   = $this->createForm(new LaguntzaType(), $entity);

        return $this->render('GitekUdaBundle:Laguntza:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Laguntza entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Laguntza();
        $form = $this->createForm(new LaguntzaType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('laguntza_show', array('id' => $entity->getId())));
        }

        return $this->render('GitekUdaBundle:Laguntza:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Laguntza entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Laguntza')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Laguntza entity.');
        }

        $editForm = $this->createForm(new LaguntzaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GitekUdaBundle:Laguntza:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Laguntza entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Laguntza')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Laguntza entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new LaguntzaType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('laguntza_edit', array('id' => $id)));
        }

        return $this->render('GitekUdaBundle:Laguntza:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Laguntza entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GitekUdaBundle:Laguntza')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Laguntza entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('laguntza'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
