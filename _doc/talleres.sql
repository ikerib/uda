# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.19-log)
# Database: UDA
# Generation Time: 2012-02-07 15:54:02 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table Taller
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Taller`;

CREATE TABLE `Taller` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `Taller` WRITE;
/*!40000 ALTER TABLE `Taller` DISABLE KEYS */;

INSERT INTO `Taller` (`id`, `nombre`)
VALUES
	(85,'I+D+I PROCESO ELEGIBLE        '),
	(111,'ESTR.DIVIS.INDUST.            '),
	(130,'TOLOSA                        '),
	(131,'C.P.E. 2'),
	(140,'BERGARA                       '),
	(141,'C.P.E.                        '),
	(150,'LEGAZPIA                      '),
	(160,'ORDIZIA                       '),
	(170,'AZKOITIA                      '),
	(180,'ORIO                          '),
	(190,'IRUN                          '),
	(200,'RENTERIA                      '),
	(210,'EIBAR                         '),
	(211,'CELULA DE REPUESTOS           '),
	(220,'ARRASATE                      '),
	(230,'LASARTE                       '),
	(240,'ILLARRA                       '),
	(242,'BERIO                         '),
	(250,'GKN                           '),
	(251,'RTS                           '),
	(293,'ORKLY                         '),
	(312,'ZZZZ'),
	(313,'ZZZZ'),
	(314,'ZZZZ'),
	(315,'ZZZZ');

/*!40000 ALTER TABLE `Taller` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
