<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Gitek\UdaBundle\Entity\Formacion
 *
 * @ORM\Table(name="Formacion")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Gitek\UdaBundle\Entity\FormacionRepository")
 */
class Formacion
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;


    /**
     * @var string $version
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=true)
     * @Assert\Length(
     *      min = "2",
     *      max = "50",
     *      minMessage = "Especifique como mínimo {{ limit }} carácteres."
     * )
     */
    private $version;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;

    /**
     * @var integer $orden
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;

    /**
     * @ORM\OneToMany(targetEntity="Detformacion", mappedBy="formacion", cascade={"remove"})
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    private $detformaciones;



	/**
     * @ORM\OneToMany(targetEntity="Detcurso", mappedBy="formacion")
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    private $detcursos;

		// /**
		//      * @ORM\OneToMany(targetEntity="Detentrenamiento", mappedBy="formacion")
		//      */
		//     private $detentrenamientos;


    public function __construct()
    {
        $this->detformaciones = new \Doctrine\Common\Collections\ArrayCollection();
		$this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
    }

    public function __toString()
    {
        return $this->getNombre();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set version
     *
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * Get version
     *
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Add detformaciones
     *
     * @param Gitek\UdaBundle\Entity\Detformacion $detformaciones
     */
    public function addDetformacion(\Gitek\UdaBundle\Entity\Detformacion $detformaciones)
    {
        $this->detformaciones[] = $detformaciones;
    }

    /**
     * Get detformaciones
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getDetformaciones()
    {
        return $this->detformaciones;
    }

    /**
     * Add detcursos
     *
     * @param Gitek\UdaBundle\Entity\Detcurso $detcursos
     */
    public function addDetcurso(\Gitek\UdaBundle\Entity\Detcurso $detcursos)
    {
        $this->detcursos[] = $detcursos;
    }

    /**
     * Get detcursos
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getDetcursos()
    {
        return $this->detcursos;
    }

    // /**
    //  * Add detentrenamientos
    //  *
    //  * @param Gitek\UdaBundle\Entity\Detentrenamiento $detentrenamientos
    //  */
    // public function addDetentrenamiento(\Gitek\UdaBundle\Entity\Detentrenamiento $detentrenamientos)
    // {
    //     $this->detentrenamientos[] = $detentrenamientos;
    // }
    //
    // /**
    //  * Get detentrenamientos
    //  *
    //  * @return Doctrine\Common\Collections\Collection
    //  */
    // public function getDetentrenamientos()
    // {
    //     return $this->detentrenamientos;
    // }

    /**
     * Add detformaciones
     *
     * @param \Gitek\UdaBundle\Entity\Detformacion $detformaciones
     * @return Formacion
     */
    public function addDetformacione(\Gitek\UdaBundle\Entity\Detformacion $detformaciones)
    {
        $this->detformaciones[] = $detformaciones;

        return $this;
    }

    /**
     * Remove detformaciones
     *
     * @param \Gitek\UdaBundle\Entity\Detformacion $detformaciones
     */
    public function removeDetformacione(\Gitek\UdaBundle\Entity\Detformacion $detformaciones)
    {
        $this->detformaciones->removeElement($detformaciones);
    }

    /**
     * Remove detcursos
     *
     * @param \Gitek\UdaBundle\Entity\Detcurso $detcursos
     */
    public function removeDetcurso(\Gitek\UdaBundle\Entity\Detcurso $detcursos)
    {
        $this->detcursos->removeElement($detcursos);
    }
}