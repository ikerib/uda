<?php

namespace Gitek\UdaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class HistorialType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('operario_id')
            ->add('taller_id')
            ->add('completado')
            ->add('aprobado')
            ->add('created')
            ->add('updated')
            ->add('operario')
            ->add('taller')
        ;
    }

    public function getName()
    {
        return 'gitek_udabundle_historialtype';
    }
}
