<?php


namespace Gitek\UdaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Gitek\UdaBundle\Entity\Detformacion;
use Gitek\UdaBundle\Form\DetformacionType;
use Gitek\UdaBundle\Entity\Formacion;
use Gitek\UdaBundle\Entity\Curso;


/**
 * Detformacion controller.
 */
class DetformacionController extends Controller
{

  	public function showAction($id)
   	{
       $em = $this->getDoctrine()->getManager();

       $detformacion = $em->getRepository('GitekUdaBundle:Detformacion')->find($id);

       if (!$detformacion) {
           throw $this->createNotFoundException('Unable to find Detcurso entity.');
       }

       $deleteForm = $this->createDeleteForm($id);

      	return $this->render('GitekUdaBundle:Detformacion:show.html.twig', array(
            'detformacion' => $detformacion,
            'delete_form' => $deleteForm->createView(),
        ));
   }

    public function newAction($formacion_id, $curso_id=null)
    {
        $formacion = $this->getFormacion($formacion_id);
        $detformacion = new Detformacion();
        $detformacion->setFormacion($formacion);
        $form   = $this->createForm(new DetformacionType(), $detformacion);

        return $this->render('GitekUdaBundle:Detformacion:form.html.twig', array(
            'detformacion' => $detformacion,
            'form'         => $form->createView(),
            'curso_id'     => $curso_id
        ));
    }

    public function createAction(Request $request, $formacion_id, $curso_id=null)
    {
        $formacion = $this->getFormacion($formacion_id);

        $detformacion  = new Detformacion();
        $detformacion->setFormacion($formacion);
        $request = $this->getRequest();
        $form    = $this->createForm(new DetformacionType(), $detformacion);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
			$em->persist($detformacion);
			$em->flush();
			$curso_id = $this->get('request')->query->get('curso_id');
			return $this->redirect($this->generateUrl('curso_show', array('id'=>$curso_id)));

        }

        return $this->render('GitekUdaBundle:Detformacion:create.html.twig', array(
            'detformacion' => $detformacion,
            'form'         => $form->createView()
        ));
    }

    public function volverAction($id,$curso_id=null,$mianchor=null,$mitab=null)
    {
			$url = $this->generateUrl('curso_show', array('id' => $curso_id));
			return $this->redirect(
				sprintf('%s#%s--%s', $url, $mianchor,$mitab)
			);
		}

    public function editAction(Request $request, $id,$curso_id=null,$mianchor=null,$mitab=null)
    {

		  $em = $this->getDoctrine()->getManager();

      $detformacion = $em->getRepository('GitekUdaBundle:Detformacion')->find($id);

      $laguntzak = $em->getRepository('GitekUdaBundle:Laguntza')->findAll(array("mostrar"=>"1"));

      if (!$detformacion) {
        throw $this->createNotFoundException('Unable to find detformacion entity.');
      }

      $formulario = $this->createForm(new DetformacionType(), $detformacion);

		  $deleteForm = $this->createDeleteForm($id);

		  $peticion = $this->getRequest();

      if ($peticion->getMethod() == 'POST') {
        $formulario->bind($peticion);

        // if ($formulario->isValid()) {
          $detformacion->setUpdatedAt(new \DateTime());
          $em->persist($detformacion);
          $em->flush();

          $this->get('session')->getFlashBag()->add('notice', 'Datos guardados con éxito.');

				  $url = $this->generateUrl('curso_show', array('id' => $curso_id));
				  // return $this->redirect(
				  //   sprintf('%s#%s--%s', $url, $mianchor,$mitab)
				  // );
        // }
      }

      return $this->render('GitekUdaBundle:Detformacion:edit.html.twig', array(
          'form'        => $formulario->createView(),
          'entity'      => $detformacion,
          'laguntzak'   => $laguntzak,
          'delete_form' => $deleteForm->createView(),
          'id'          => $id,
          'curso_id'    => $curso_id,
          'mianchor'    => $mianchor,
          'mitab'       => $mitab,
      ));
    }

    public function deleteuploadAction($id,$que)
    {
      $em = $this->getDoctrine()->getManager();
      $detformacion = $em->getRepository('GitekUdaBundle:Detformacion')->find($id);
      if ( $que == "foto") {

        unlink($detformacion->getFile()->getRealpath());
        $detformacion->setFile(null);
        $detformacion->setImagen(null);
        $detformacion->setUpdatedAt(new \DateTime());
        $em->persist($detformacion);
        $em->flush();

      } elseif ( $que == "audio" ) {

        unlink($detformacion->getFile2()->getRealpath());
        $detformacion->setFile2(null);
        $detformacion->setAudio(null);
        $detformacion->setUpdatedAt(new \DateTime());
        $em->persist($detformacion);
        $em->flush();

      } elseif ( $que == "video" ) {

        unlink($detformacion->getFile3()->getRealpath());
        $detformacion->setFile3(null);
        $detformacion->setVideo(null);
        $detformacion->setUpdatedAt(new \DateTime());
        $em->persist($detformacion);
        $em->flush();

      }
      return new Response('OK', 200);

    }


    protected function getFormacion($formacion_id)
    {
        $em = $this->getDoctrine()->getManager();

        $formacion = $em->getRepository('GitekUdaBundle:Formacion')->find($formacion_id);

        if (!$formacion) {
            throw $this->createNotFoundException('Unable to find formacion post.');
        }

        return $formacion;
    }

    public function deleteAction(Request $request, $id,$curso_id=null,$mianchor=null,$mitab=null)
    {
      $form = $this->createDeleteForm($id);
      $request = $this->getRequest();

      $form->bind($request);

      // if ($form->isValid()) {
        // print_r("DE K BAS?");
        $em = $this->getDoctrine()->getManager();
        $detformacion = $em->getRepository('GitekUdaBundle:Detformacion')->find($id);

			  if (!$detformacion) {
          // throw $this->createNotFoundException('Unable to find detformacion entity.');
          $url = $this->generateUrl('curso_show', array('id' => $curso_id));
          return $this->redirect(
            sprintf('%s#%s--%s', $url, $mianchor,$mitab)
          );
        }

        $em->remove($detformacion);
        $em->flush();

			$url = $this->generateUrl('curso_show', array('id' => $curso_id));
			return $this->redirect(
			    sprintf('%s#%s--%s', $url, $mianchor,$mitab)
			);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

 	/**
    * Finds and displays a Formacion entity.
    *
    * @Route("/{id}/play", name="detformacion_play")
    * @Template()
    */
		public function playAction($id=null){
			$em = $this->getDoctrine()->getManager();

      $detformacion = $em->getRepository('GitekUdaBundle:Detformacion')->find($id);

      if (!$detformacion) {
          throw $this->createNotFoundException('No se ha encontrado la formacíón especificada.');
      }

      return array(
      		'detformacion' => $detformacion,
			);
		}
}