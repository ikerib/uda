<?php

namespace Gitek\UdaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\UdaBundle\Entity\Detentrenamiento;
use Gitek\UdaBundle\Entity\Entrenamiento;
use Gitek\UdaBundle\Entity\Formacion;


class DetentrenamientoFixtures extends AbstractFixture implements OrderedFixtureInterface
{
	public function load($manager)
	{
		
	    $detentrenamiento1 = new Detentrenamiento();
	    $detentrenamiento1->setpregunta('Selecciona el componente correcto5.');	
	    $detentrenamiento1->setpregunta2('Selecciona el componente correcto51.');	
	    $detentrenamiento1->setpregunta3('Selecciona el componente correcto52.');	
	    $detentrenamiento1->setpregunta4('Selecciona el componente correcto53.');	
	    $detentrenamiento1->setNombre('Entrenamiento Operación1');
        $detentrenamiento1->setImg1('noborrar/x7/componente-1.png');
        $detentrenamiento1->setImg2('noborrar/x7/componente-2.png');
        $detentrenamiento1->setImg3('noborrar/x7/componente-3.png');
        $detentrenamiento1->setImgok('noborrar/x7/componente-ok.png');
        $detentrenamiento1->setImgepi1('noborrar/x7/epi-1.png');
        $detentrenamiento1->setImgepi2('noborrar/x7/epi-2.png');					
        $detentrenamiento1->setImgepi3('noborrar/x7/epi-3.png');					
        $detentrenamiento1->setImgepiok('noborrar/x7/epi-ok.png');					
        $detentrenamiento1->setImgcomponente1('noborrar/x7/defectuoso-1.png');
        $detentrenamiento1->setImgcomponente2('noborrar/x7/defectuoso-1.png');
        $detentrenamiento1->setImgcomponente3('noborrar/x7/defectuoso-1.png');
        $detentrenamiento1->setImgcomponenteok('noborrar/x7/defectuoso-ok.png');
        $detentrenamiento1->setImgrotar1('noborrar/x7/colocar-1.png');
        $detentrenamiento1->setImgrotar2('noborrar/x7/colocar-2.png');
        $detentrenamiento1->setImgrotar3('noborrar/x7/colocar-3.png');
        $detentrenamiento1->setImgrotarok('noborrar/x7/colocar-ok.png');
        $detentrenamiento1->setImgbase('noborrar/x7/petaca.jpg');
        $detentrenamiento1->setCoordenadas('top:99px;width:160px;left:562px;height:120px;');
        $detentrenamiento1->setCoordenadas2('top:359px;width:160px;left:22px;height:120px;;');
        $detentrenamiento1->setCoordenadas3('top:359px;width:160px;left:562px;height:120px;');
        $detentrenamiento1->setCoordenadasok('top:99px;width:160px;left:22px;height:120px;;');
        $detentrenamiento1->setEntrenamiento($manager->merge($this->getReference('entrenamiento1')));
        $detentrenamiento1->setDetformacion($manager->merge($this->getReference('detformacion1')));
        $detentrenamiento1->setTipoentrenamiento($manager->merge($this->getReference('tipoentrenamiento1')));
	    $manager->persist($detentrenamiento1);


	    $manager->flush();

	    }

	    public function getOrder()
	    {
	        return 5; // el orden en el que se deben cargar los accesorios
	    }
}