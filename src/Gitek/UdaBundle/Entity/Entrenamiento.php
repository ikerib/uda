<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Gitek\UdaBundle\Entity\Entrenamiento
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Gitek\UdaBundle\Entity\EntrenamientoRepository")
 */
class Entrenamiento
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string $version
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @var integer $orden
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;

    /**
     * @var time $tiempo_ini
     *
     * @ORM\Column(name="tiempo_ini", type="time", nullable=true)
     */
    private $tiempo_ini;

    /**
     * @var time $tiempo_fin
     *
     * @ORM\Column(name="tiempo_fin", type="time", nullable=true)
     */
    private $tiempo_fin;

	 /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;

	/**
     * @ORM\OneToMany(targetEntity="Detentrenamiento", mappedBy="entrenamiento", cascade={"remove"})
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    private $detentrenamientos;

	/**
     * @ORM\OneToMany(targetEntity="Detcurso", mappedBy="entrenamiento")
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    private $detcursos;

 
    public function __toString()
    {
        return $this->getNombre();
    }

    public function __construct()
    {
        $this->detentrenamientos = new \Doctrine\Common\Collections\ArrayCollection();
				$this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
    }
    
   

   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set version
     *
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * Get version
     *
     * @return string 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set tiempo_ini
     *
     * @param time $tiempoIni
     */
    public function setTiempoIni($tiempoIni)
    {
        $this->tiempo_ini = $tiempoIni;
    }

    /**
     * Get tiempo_ini
     *
     * @return time 
     */
    public function getTiempoIni()
    {
        return $this->tiempo_ini;
    }

    /**
     * Set tiempo_fin
     *
     * @param time $tiempoFin
     */
    public function setTiempoFin($tiempoFin)
    {
        $this->tiempo_fin = $tiempoFin;
    }

    /**
     * Get tiempo_fin
     *
     * @return time 
     */
    public function getTiempoFin()
    {
        return $this->tiempo_fin;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add detentrenamientos
     *
     * @param Gitek\UdaBundle\Entity\Detentrenamiento $detentrenamientos
     */
    public function addDetentrenamiento(\Gitek\UdaBundle\Entity\Detentrenamiento $detentrenamientos)
    {
        $this->detentrenamientos[] = $detentrenamientos;
    }

    /**
     * Get detentrenamientos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDetentrenamientos()
    {
        return $this->detentrenamientos;
    }

    /**
     * Add detcursos
     *
     * @param Gitek\UdaBundle\Entity\Detcurso $detcursos
     */
    public function addDetcurso(\Gitek\UdaBundle\Entity\Detcurso $detcursos)
    {
        $this->detcursos[] = $detcursos;
    }

    /**
     * Get detcursos
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getDetcursos()
    {
        return $this->detcursos;
    }

    /**
     * Remove detentrenamientos
     *
     * @param \Gitek\UdaBundle\Entity\Detentrenamiento $detentrenamientos
     */
    public function removeDetentrenamiento(\Gitek\UdaBundle\Entity\Detentrenamiento $detentrenamientos)
    {
        $this->detentrenamientos->removeElement($detentrenamientos);
    }

    /**
     * Remove detcursos
     *
     * @param \Gitek\UdaBundle\Entity\Detcurso $detcursos
     */
    public function removeDetcurso(\Gitek\UdaBundle\Entity\Detcurso $detcursos)
    {
        $this->detcursos->removeElement($detcursos);
    }
}