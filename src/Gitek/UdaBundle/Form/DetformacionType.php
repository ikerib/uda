<?php

namespace Gitek\UdaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DetformacionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('nombre','text',array('label'=>'Nombre descriptivo: '))
			->add('texto','textarea',array('label'=>'Texto: '))
            ->add('url', 'text', array('label'=>'Video youtube: ', 'required'=>false ))
            ->add('file', 'file', array(
                                    'label'=>'Imagen:',
                                    'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                                    'required' => false,
            ))
            ->add('file2', 'file', array(
                                    'label'=>'Audio:',
                                    'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                                    'required' => false,
            ))
            ->add('file3', 'file', array(
                                    'label'=>'Video:',
                                    'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                                    'required' => false,
            ))
            ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\UdaBundle\Entity\Detformacion'
        ));
    }

    public function getName()
    {
        return 'gitek_udabundle_detformaciontype';
    }
}
