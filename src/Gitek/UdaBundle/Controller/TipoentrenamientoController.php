<?php

namespace Gitek\UdaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Gitek\UdaBundle\Entity\Tipoentrenamiento;
use Gitek\UdaBundle\Form\TipoentrenamientoType;

/**
 * Tipoentrenamiento controller.
 *
 * @Route("/tipoentrenamiento")
 */
class TipoentrenamientoController extends Controller
{
    /**
     * Lists all Tipoentrenamiento entities.
     *
     * @Route("/", name="tipoentrenamiento")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('GitekUdaBundle:Tipoentrenamiento')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Tipoentrenamiento entity.
     *
     * @Route("/{id}/show", name="tipoentrenamiento_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Tipoentrenamiento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipoentrenamiento entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        );
    }

    /**
     * Displays a form to create a new Tipoentrenamiento entity.
     *
     * @Route("/new", name="tipoentrenamiento_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Tipoentrenamiento();
        $form   = $this->createForm(new TipoentrenamientoType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new Tipoentrenamiento entity.
     *
     * @Route("/create", name="tipoentrenamiento_create")
     * @Method("post")
     * @Template("GitekUdaBundle:Tipoentrenamiento:new.html.twig")
     */
    public function createAction()
    {
        $entity  = new Tipoentrenamiento();
        $request = $this->getRequest();
        $form    = $this->createForm(new TipoentrenamientoType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipoentrenamiento_show', array('id' => $entity->getId())));

        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Tipoentrenamiento entity.
     *
     * @Route("/{id}/edit", name="tipoentrenamiento_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Tipoentrenamiento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipoentrenamiento entity.');
        }

        $editForm = $this->createForm(new TipoentrenamientoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Tipoentrenamiento entity.
     *
     * @Route("/{id}/update", name="tipoentrenamiento_update")
     * @Method("post")
     * @Template("GitekUdaBundle:Tipoentrenamiento:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Tipoentrenamiento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tipoentrenamiento entity.');
        }

        $editForm   = $this->createForm(new TipoentrenamientoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('tipoentrenamiento_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Tipoentrenamiento entity.
     *
     * @Route("/{id}/delete", name="tipoentrenamiento_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GitekUdaBundle:Tipoentrenamiento')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tipoentrenamiento entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('tipoentrenamiento'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
