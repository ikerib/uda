<?php

namespace Gitek\UdaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DetentrenamientoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('nombre')
			->add('pregunta')
            ->add('pregunta2')
            ->add('pregunta3')
            // ->add('pregunta4')

            ->add('detformacion')
            // ->add('detformacion',
            //     'entity',
            //     array(
            //         'class' => 'Gitek\\UdaBundle\\Entity\\Detformacion',
            //         'empty_value' => 'Selecciona una formacion',
            //         'query_builder' => function(\Doctrine\ORM\EntityRepository $repositorio) use($options)
            //         {
            //             return $repositorio->createQueryBuilder('d')
            //                     ->innerJoin('d.formacion','f')
            //                     ->innerJoin('f.detcursos','de')
            //                     ->innerJoin('de.entrenamiento','en')
            //                     ->innerJoin('en.detentrenamientos','deten')
            //                     ->where("deten.id = :id ")
            //                     ->setParameter('id', $options['miid']);
            //         },
            //         'required'  => true,
            //     )
            // )

			->add('fimg1' , 'file', array(
                'label'=>'Imagen 1: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
			->add('fimg2' , 'file', array(
                'label'=>'Imagen 2: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
            ->add('fimg3' , 'file', array(
                'label'=>'Imagen 3: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
            ->add('fimgok' , 'file', array(
                'label'=>'Imagen CORRECTA: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))

			->add('fimgepi1' , 'file', array(
                'label'=>'Imagen epi1: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
			->add('fimgepi2' , 'file', array(
                'label'=>'Imagen epi2: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
            ->add('fimgepi3' , 'file', array(
                'label'=>'Imagen epi3: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
            ->add('fimgepiok' , 'file', array(
                'label'=>'Imagen epi CORRECTA: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))

			->add('fimgcomponente1' , 'file', array(
                'label'=>'Imagen componente1: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
			->add('fimgcomponente2' , 'file', array(
                'label'=>'Imagen componente2: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
            ->add('fimgcomponente3' , 'file', array(
                'label'=>'Imagen componente3: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
            ->add('fimgcomponenteok' , 'file', array(
                'label'=>'Imagen componente CORRECTA: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))

			->add('fimgrotar1' , 'file', array(
                'label'=>'Imagen rotar 1: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
			->add('fimgrotar2' , 'file', array(
                'label'=>'Imagen rotar 2: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
            ->add('fimgrotar3' , 'file', array(
                'label'=>'Imagen rotar 3: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
            ->add('fimgrotarok' , 'file', array(
                'label'=>'Imagen rotar CORRECTA: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
			->add('fimgbase' , 'file', array(
                'label'=>'Imagen base: ',
                'data_class' => 'Symfony\Component\HttpFoundation\File\File',
                'required'=>false
            ))
            ->add('coordenadas')
            ->add('coordenadas2')
			->add('coordenadas3')
            ->add('coordenadasok')
            // ->add('orden')
            // ->add('entrenamiento')
        ;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'miid' => null,
        );
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gitek\UdaBundle\Entity\Detentrenamiento'
        ));
    }


    public function getName()
    {
        return 'gitek_udabundle_detentrenamientotype';
    }
}
