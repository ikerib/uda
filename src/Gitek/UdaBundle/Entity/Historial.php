<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Gitek\UdaBundle\Entity\Historial
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Gitek\UdaBundle\Entity\HistorialRepository")
 */
class Historial
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $tipo
     *
     * @ORM\Column(name="tipo", type="string", length=255)
     */
    private $tipo;

    /**
     * @var integer $operario_id
     *
     * @ORM\Column(name="operario_id", type="integer")
     */
    private $operario_id;

    /**
     * @var integer $taller_id
     *
     * @ORM\Column(name="taller_id", type="integer")
     */
    private $taller_id;

    /**
     * @var integer $curso_id
     *
     * @ORM\Column(name="curso_id", type="integer")
     */
    private $curso_id;

    /**
     * @var boolean $completado
     *
     * @ORM\Column(name="completado", type="boolean", nullable=true)
     */
    private $completado=false;

    /**
     * @var boolean $aprobado
     *
     * @ORM\Column(name="aprobado", type="boolean", nullable=true)
     */
    private $aprobado=false;

    /**
     * @var datetime $hora_ini
     *
     * @ORM\Column(name="hora_ini", type="datetime", nullable=true)
     */
    private $hora_ini;

    /**
     * @var datetime $hora_fin
     *
     * @ORM\Column(name="hora_fin", type="datetime", nullable=true)
     */
    private $hora_fin;

    /**
     * @var integer $tiempomax
     *
     * @ORM\Column(name="tiempomax", type="integer", nullable=true)
     */
    private $tiempomax;

    /**
     * @var integer $tiempoexamen
     *
     * @ORM\Column(name="tiempoexamen", type="integer", nullable=true)
     */
    private $tiempoexamen;

	/**
	* @ORM\Column(type="datetime", name="created_at")
    *
    * @var DateTime $createdAt
    */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     *
     * @var DateTime $updatedAt
     */
    protected $updatedAt;

		/**
     * @ORM\OneToMany(targetEntity="Dethistorial", mappedBy="historial", cascade={"remove"})
     */
    private $dethistoriales;

    /**
     * @ORM\ManyToOne(targetEntity="Operario",inversedBy="historiales")
     * @ORM\JoinColumn(name="operario_id", referencedColumnName="id")
     */
    private $operario;

    /**
      * @ORM\ManyToOne(targetEntity="Taller",inversedBy="talleres")
      * @ORM\JoinColumn(name="taller_id", referencedColumnName="id")
      */
    private $taller;

    /**
     * @ORM\ManyToOne(targetEntity="Curso",inversedBy="historiales")
     * @ORM\JoinColumn(name="curso_id", referencedColumnName="id")
     */
    private $curso;

    public function __construct()
    {
        $this->dethistoriales = new \Doctrine\Common\Collections\ArrayCollection();
				$this->createdAt = new \DateTime();
	      $this->updatedAt = new \DateTime();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set operario_id
     *
     * @param integer $operarioId
     */
    public function setOperarioId($operarioId)
    {
        $this->operario_id = $operarioId;
    }

    /**
     * Get operario_id
     *
     * @return integer
     */
    public function getOperarioId()
    {
        return $this->operario_id;
    }

    /**
     * Set taller_id
     *
     * @param integer $tallerId
     */
    public function setTallerId($tallerId)
    {
        $this->taller_id = $tallerId;
    }

    /**
     * Get taller_id
     *
     * @return integer
     */
    public function getTallerId()
    {
        return $this->taller_id;
    }

    /**
     * Set curso_id
     *
     * @param integer $cursoId
     */
    public function setCursoId($cursoId)
    {
        $this->curso_id = $cursoId;
    }

    /**
     * Get curso_id
     *
     * @return integer
     */
    public function getCursoId()
    {
        return $this->curso_id;
    }

    /**
     * Set completado
     *
     * @param boolean $completado
     */
    public function setCompletado($completado)
    {
        $this->completado = $completado;
    }

    /**
     * Get completado
     *
     * @return boolean
     */
    public function getCompletado()
    {
        return $this->completado;
    }

    /**
     * Set aprobado
     *
     * @param boolean $aprobado
     */
    public function setAprobado($aprobado)
    {
        $this->aprobado = $aprobado;
    }

    /**
     * Get aprobado
     *
     * @return boolean
     */
    public function getAprobado()
    {
        return $this->aprobado;
    }

    /**
     * Set createdAt
     *
     * @param datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add dethistoriales
     *
     * @param Gitek\UdaBundle\Entity\Dethistorial $dethistoriales
     */
    public function addDethistorial(\Gitek\UdaBundle\Entity\Dethistorial $dethistoriales)
    {
        $this->dethistoriales[] = $dethistoriales;
    }

    /**
     * Get dethistoriales
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getDethistoriales()
    {
        return $this->dethistoriales;
    }

    /**
     * Set operario
     *
     * @param Gitek\UdaBundle\Entity\Operario $operario
     */
    public function setOperario(\Gitek\UdaBundle\Entity\Operario $operario)
    {
        $this->operario = $operario;
    }

    /**
     * Get operario
     *
     * @return Gitek\UdaBundle\Entity\Operario
     */
    public function getOperario()
    {
        return $this->operario;
    }

    /**
     * Set taller
     *
     * @param Gitek\UdaBundle\Entity\Taller $taller
     */
    public function setTaller(\Gitek\UdaBundle\Entity\Taller $taller)
    {
        $this->taller = $taller;
    }

    /**
     * Get taller
     *
     * @return Gitek\UdaBundle\Entity\Taller
     */
    public function getTaller()
    {
        return $this->taller;
    }

    /**
     * Set curso
     *
     * @param Gitek\UdaBundle\Entity\Curso $curso
     */
    public function setCurso(\Gitek\UdaBundle\Entity\Curso $curso)
    {
        $this->curso = $curso;
    }

    /**
     * Get curso
     *
     * @return Gitek\UdaBundle\Entity\Curso
     */
    public function getCurso()
    {
        return $this->curso;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set hora_ini
     *
     * @param time $horaIni
     */
    public function setHoraIni($horaIni)
    {
        $this->hora_ini = $horaIni;
    }

    /**
     * Get hora_ini
     *
     * @return time
     */
    public function getHoraIni()
    {
        return $this->hora_ini;
    }

    /**
     * Set hora_fin
     *
     * @param time $horaFin
     */
    public function setHoraFin($horaFin)
    {
        $this->hora_fin = $horaFin;
    }

    /**
     * Get hora_fin
     *
     * @return time
     */
    public function getHoraFin()
    {
        return $this->hora_fin;
    }

    /**
     * Set tiempoexamen
     *
     * @param integer $tiempoexamen
     */
    public function setTiempoexamen($tiempoexamen)
    {
        $this->tiempoexamen = $tiempoexamen;
    }

    /**
     * Get tiempoexamen
     *
     * @return integer
     */
    public function getTiempoexamen()
    {
        return $this->tiempoexamen;
    }

    /**
     * Set tiempomax
     *
     * @param integer $tiempomax
     */
    public function setTiempomax($tiempomax)
    {
        $this->tiempomax = $tiempomax;
    }

    /**
     * Get tiempomax
     *
     * @return integer
     */
    public function getTiempomax()
    {
        return $this->tiempomax;
    }

    /**
     * Add dethistoriales
     *
     * @param \Gitek\UdaBundle\Entity\Dethistorial $dethistoriales
     * @return Historial
     */
    public function addDethistoriale(\Gitek\UdaBundle\Entity\Dethistorial $dethistoriales)
    {
        $this->dethistoriales[] = $dethistoriales;

        return $this;
    }

    /**
     * Remove dethistoriales
     *
     * @param \Gitek\UdaBundle\Entity\Dethistorial $dethistoriales
     */
    public function removeDethistoriale(\Gitek\UdaBundle\Entity\Dethistorial $dethistoriales)
    {
        $this->dethistoriales->removeElement($dethistoriales);
    }
}