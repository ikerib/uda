<?php

namespace Gitek\UdaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\UdaBundle\Entity\Operario;

class OperarioFixtures extends AbstractFixture implements OrderedFixtureInterface
{
	public function load($manager)
	    {
	    	
	        $operario1 = new Operario();
	        $operario1->setNombre('zzz1');
	        $operario1->setApellidos('aaaa');
			$operario1->setTaller($manager->merge($this->getReference('taller1')));
	        $manager->persist($operario1);

					
	        $manager->flush();

	        $this->addReference('operario1', $operario1);

	    }

	    public function getOrder()
	    {
	        return 9; // el orden en el que se deben cargar los accesorios
	    }
}