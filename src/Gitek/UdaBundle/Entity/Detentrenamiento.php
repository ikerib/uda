<?php

namespace Gitek\UdaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Gitek\UdaBundle\Entity\Detentrenamiento
 *
 * @ORM\Table()
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\Entity(repositoryClass="Gitek\UdaBundle\Entity\DetentrenamientoRepository")
 */
class Detentrenamiento
{
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
    	$this->updatedAt = new \DateTime();
    }

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    protected $nombre;

    /**
     * @var string $pregunta
     *
     * @ORM\Column(name="pregunta", type="string", length=255)
     */
    protected $pregunta;

    /**
     * @var string $pregunta2
     *
     * @ORM\Column(name="pregunta2", type="string", length=255, nullable=true)
     */
    protected $pregunta2;

    /**
     * @var string $pregunta3
     *
     * @ORM\Column(name="pregunta3", type="string", length=255, nullable=true)
     */
    protected $pregunta3;

    /**
     * @var string $pregunta4
     *
     * @ORM\Column(name="pregunta4", type="string", length=255, nullable=true)
     */
    protected $pregunta4;

    /**
     * @var integer $entrenamiento_id
     *
     * @ORM\Column(name="entrenamiento_id", type="integer")
     */
    protected $entrenamiento_id;

	/**
	 * @var integer $detformacion_id
	 *
	 * @ORM\Column(name="detformacion_id", type="integer", nullable=true)
     */
    protected $detformacion_id;

    /**
     * @ORM\ManyToOne(targetEntity="Detformacion",inversedBy="detformacion")
     * @ORM\JoinColumn(name="detformacion_id", referencedColumnName="id")
     */
    protected $detformacion;


    /**
     * @var string $img1
     *
     * @ORM\Column(name="img1", type="string", length=255, nullable=true)
     */
    protected $img1;

    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="img1", nullable=true)
    *
    * @var File $fimg1
    */
    protected $fimg1;
    public function setFimg1($fimg1) {
        $this -> fimg1 = $fimg1;
        if ($fimg1 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimg1() {
        return $this-> fimg1;
    }

    /**
     * @var string $img2
     *
     * @ORM\Column(name="img2", type="string", length=255, nullable=true)
     */
    protected $img2;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="img2", nullable=true)
    *
    * @var File $fimg2
    */
    protected $fimg2;
    public function setFimg2($fimg2) {
        $this -> fimg2 = $fimg2;
        if ($fimg2 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimg2() {
        return $this-> fimg2;
    }

    /**
     * @var string $img3
     *
     * @ORM\Column(name="img3", type="string", length=255, nullable=true)
     */
    protected $img3;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="img3", nullable=true)
    *
    * @var File $fimg3
    */
    protected $fimg3;
    public function setFimg3($fimg3) {
        $this -> fimg3 = $fimg3;
        if ($fimg3 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimg3() {
        return $this-> fimg3;
    }

    /**
     * @var string $imgok
     *
     * @ORM\Column(name="imgok", type="string", length=255, nullable=true)
     */
    protected $imgok;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgok", nullable=true)
    *
    * @var File $fimgok
    */
    protected $fimgok;
    public function setFimgok($fimgok) {
        $this -> fimgok = $fimgok;
        if ($fimgok instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgok() {
        return $this-> fimgok;
    }

	/**
     * @var string $imgepi1
     *
     * @ORM\Column(name="imgepi1", type="string", length=255, nullable=true)
     */
    protected $imgepi1;

    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgepi1", nullable=true)
    *
    * @var File $fimgepi1
    */
    protected $fimgepi1;
    public function setFimgepi1($fimgepi1) {
        $this -> fimgepi1 = $fimgepi1;
        if ($fimgepi1 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgepi1() {
        return $this-> fimgepi1;
    }

	/**
     * @var string $imgepi2
     *
     * @ORM\Column(name="imgepi2", type="string", length=255, nullable=true)
     */
    protected $imgepi2;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgepi2", nullable=true)
    *
    * @var File $fimgepi2
    */
    protected $fimgepi2;
    public function setFimgepi2($fimgepi2) {
        $this -> fimgepi2 = $fimgepi2;
        if ($fimgepi2 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgepi2() {
        return $this-> fimgepi2;
    }

	/**
     * @var string $imgepi3
     *
     * @ORM\Column(name="imgepi3", type="string", length=255, nullable=true)
     */

    protected $imgepi3;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgepi3", nullable=true)
    *
    * @var File $fimgepi3
    */
    protected $fimgepi3;
    public function setFimgepi3($fimgepi3) {
        $this -> fimgepi3 = $fimgepi3;
        if ($fimgepi3 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgepi3() {
        return $this-> fimgepi3;
    }

	/**
     * @var string $imgepiok
     *
     * @ORM\Column(name="imgepiok", type="string", length=255, nullable=true)
     */
    protected $imgepiok;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgepiok", nullable=true)
    *
    * @var File $fimgepiok
    */
    protected $fimgepiok;
    public function setFimgepiok($fimgepiok) {
        $this -> fimgepiok = $fimgepiok;
        if ($fimgepiok instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgepiok() {
        return $this-> fimgepiok;
    }

	/**
     * @var string $imgcomponente1
     *
     * @ORM\Column(name="imgcomponente1", type="string", length=255, nullable=true)
     */
    protected $imgcomponente1;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgcomponente1", nullable=true)
    *
    * @var File $fimgcomponente1
    */
    protected $fimgcomponente1;
    public function setFimgcomponente1($fimgcomponente1) {
        $this -> fimgcomponente1 = $fimgcomponente1;
        if ($fimgcomponente1 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgcomponente1() {
        return $this-> fimgcomponente1;
    }

	/**
     * @var string $imgcomponente2
     *
     * @ORM\Column(name="imgcomponente2", type="string", length=255, nullable=true)
     */
    protected $imgcomponente2;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgcomponente2", nullable=true)
    *
    * @var File $fimgcomponente2
    */
    protected $fimgcomponente2;
    public function setFimgcomponente2($fimgcomponente2) {
        $this -> fimgcomponente2 = $fimgcomponente2;
        if ($fimgcomponente2 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgcomponente2() {
        return $this-> fimgcomponente2;
    }

	/**
     * @var string $imgcomponente3
     *
     * @ORM\Column(name="imgcomponente3", type="string", length=255, nullable=true)
     */
    protected $imgcomponente3;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgcomponente3", nullable=true)
    *
    * @var File $fimgcomponente3
    */
    protected $fimgcomponente3;
    public function setFimgcomponente3($fimgcomponente3) {
        $this -> fimgcomponente3 = $fimgcomponente3;
        if ($fimgcomponente3 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgcomponente3() {
        return $this-> fimgcomponente3;
    }

	/**
     * @var string $imgcomponenteok
     *
     * @ORM\Column(name="imgcomponenteok", type="string", length=255, nullable=true)
     */
    protected $imgcomponenteok;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgcomponenteok", nullable=true)
    *
    * @var File $fimgcomponenteok
    */
    protected $fimgcomponenteok;
    public function setFimgcomponenteok($fimgcomponenteok) {
        $this -> fimgcomponenteok = $fimgcomponenteok;
        if ($fimgcomponenteok instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgcomponenteok() {
        return $this-> fimgcomponenteok;
    }

	/**
     * @var string $imgrotar1
     *
     * @ORM\Column(name="imgrotar1", type="string", length=255, nullable=true)
     */
    protected $imgrotar1;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgrotar1", nullable=true)
    *
    * @var File $fimgrotar1
    */
    protected $fimgrotar1;
    public function setFimgrotar1($fimgrotar1) {
        $this -> fimgrotar1 = $fimgrotar1;
        if ($fimgrotar1 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgrotar1() {
        return $this-> fimgrotar1;
    }

	/**
     * @var string $imgrotar2
     *
     * @ORM\Column(name="imgrotar2", type="string", length=255, nullable=true)
     */
    protected $imgrotar2;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgrotar2", nullable=true)
    *
    * @var File $fimgrotar2
    */
    protected $fimgrotar2;
    public function setFimgrotar2($fimgrotar2) {
        $this -> fimgrotar2 = $fimgrotar2;
        if ($fimgrotar2 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgrotar2() {
        return $this-> fimgrotar2;
    }

	/**
     * @var string $imgrotar3
     *
     * @ORM\Column(name="imgrotar3", type="string", length=255, nullable=true)
     */
    protected $imgrotar3;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgrotar3", nullable=true)
    *
    * @var File $fimgrotar3
    */
    protected $fimgrotar3;
    public function setFimgrotar3($fimgrotar3) {
        $this -> fimgrotar3 = $fimgrotar3;
        if ($fimgrotar3 instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgrotar3() {
        return $this-> fimgrotar3;
    }

    /**
     * @var string $imgrotarok
     *
     * @ORM\Column(name="imgrotarok", type="string", length=255, nullable=true)
     */
    protected $imgrotarok;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgrotarok", nullable=true)
    *
    * @var File $fimgrotarok
    */
    protected $fimgrotarok;
    public function setFimgrotarok($fimgrotarok) {
        $this -> fimgrotarok = $fimgrotarok;
        if ($fimgrotarok instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgrotarok() {
        return $this-> fimgrotarok;
    }

    /**
     * @var string $imgbase
     *
     * @ORM\Column(name="imgbase", type="string", length=255, nullable=true)
     */
    protected $imgbase;
    /**
    * @Assert\File(
    *     maxSize="1M",
    *     mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
    * )
    * @Vich\UploadableField(mapping="uploads", fileNameProperty="imgbase", nullable=true)
    *
    * @var File $fimgbase
    */
    protected $fimgbase;
    public function setFimgbase($fimgbase) {
        $this -> fimgbase = $fimgbase;
        if ($fimgbase instanceof UploadedFile) {
            // $this->setNombre("KAKA2");
            $this->setUpdatedAt(new \DateTime());
        }
    }
    public function getFimgbase() {
        return $this-> fimgbase;
    }


    /**
     * @var string $coordenadas
     *
     * @ORM\Column(name="coordenadas", type="string", length=255, nullable=true)
     */
    protected $coordenadas;

    /**
     * @var string $coordenadas2
     *
     * @ORM\Column(name="coordenadas2", type="string", length=255, nullable=true)
     */
    protected $coordenadas2;

    /**
     * @var string $coordenadas3
     *
     * @ORM\Column(name="coordenadas3", type="string", length=255, nullable=true)
     */
    protected $coordenadas3;

    /**
     * @var string $coordenadasok
     *
     * @ORM\Column(name="coordenadasok", type="string", length=255, nullable=true)
     */
    protected $coordenadasok;

    /**
     * @var integer $orden
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    protected $orden;

	/**
	* @ORM\Column(type="datetime", name="created_at")
    *
    * @var DateTime $createdAt
    */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     *
     * @var DateTime $updatedAt
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="Entrenamiento", inversedBy="detentrenamientos" )
     * @ORM\JoinColumn(name="entrenamiento_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $entrenamiento;

    /**
     * @var integer $tipoentrenamiendo_id
     *
     * @ORM\Column(name="tipoentrenamiendo_id", type="integer")
     */
    protected $tipoentrenamiendo_id;

    /**
     * @ORM\ManyToOne(targetEntity="Tipoentrenamiento", inversedBy="entrenamientos" )
     * @ORM\JoinColumn(name="tipoentrenamiendo_id", referencedColumnName="id")
     */
    protected $tipoentrenamiento;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function __toString()
    {
        return $this->getNombre();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Detentrenamiento
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set pregunta
     *
     * @param string $pregunta
     * @return Detentrenamiento
     */
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return string
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set pregunta2
     *
     * @param string $pregunta2
     * @return Detentrenamiento
     */
    public function setPregunta2($pregunta2)
    {
        $this->pregunta2 = $pregunta2;

        return $this;
    }

    /**
     * Get pregunta2
     *
     * @return string
     */
    public function getPregunta2()
    {
        return $this->pregunta2;
    }

    /**
     * Set pregunta3
     *
     * @param string $pregunta3
     * @return Detentrenamiento
     */
    public function setPregunta3($pregunta3)
    {
        $this->pregunta3 = $pregunta3;

        return $this;
    }

    /**
     * Get pregunta3
     *
     * @return string
     */
    public function getPregunta3()
    {
        return $this->pregunta3;
    }

    /**
     * Set pregunta4
     *
     * @param string $pregunta4
     * @return Detentrenamiento
     */
    public function setPregunta4($pregunta4)
    {
        $this->pregunta4 = $pregunta4;

        return $this;
    }

    /**
     * Get pregunta4
     *
     * @return string
     */
    public function getPregunta4()
    {
        return $this->pregunta4;
    }

    /**
     * Set entrenamiento_id
     *
     * @param integer $entrenamientoId
     * @return Detentrenamiento
     */
    public function setEntrenamientoId($entrenamientoId)
    {
        $this->entrenamiento_id = $entrenamientoId;

        return $this;
    }

    /**
     * Get entrenamiento_id
     *
     * @return integer
     */
    public function getEntrenamientoId()
    {
        return $this->entrenamiento_id;
    }

    /**
     * Set detformacion_id
     *
     * @param integer $detformacionId
     * @return Detentrenamiento
     */
    public function setDetformacionId($detformacionId)
    {
        $this->detformacion_id = $detformacionId;

        return $this;
    }

    /**
     * Get detformacion_id
     *
     * @return integer
     */
    public function getDetformacionId()
    {
        return $this->detformacion_id;
    }

    /**
     * Set img1
     *
     * @param string $img1
     * @return Detentrenamiento
     */
    public function setImg1($img1)
    {
        $this->img1 = $img1;

        return $this;
    }

    /**
     * Get img1
     *
     * @return string
     */
    public function getImg1()
    {
        return $this->img1;
    }

    /**
     * Set img2
     *
     * @param string $img2
     * @return Detentrenamiento
     */
    public function setImg2($img2)
    {
        $this->img2 = $img2;

        return $this;
    }

    /**
     * Get img2
     *
     * @return string
     */
    public function getImg2()
    {
        return $this->img2;
    }

    /**
     * Set img3
     *
     * @param string $img3
     * @return Detentrenamiento
     */
    public function setImg3($img3)
    {
        $this->img3 = $img3;

        return $this;
    }

    /**
     * Get img3
     *
     * @return string
     */
    public function getImg3()
    {
        return $this->img3;
    }

    /**
     * Set imgok
     *
     * @param string $imgok
     * @return Detentrenamiento
     */
    public function setImgok($imgok)
    {
        $this->imgok = $imgok;

        return $this;
    }

    /**
     * Get imgok
     *
     * @return string
     */
    public function getImgok()
    {
        return $this->imgok;
    }

    /**
     * Get imgepi1
     *
     * @return string
     */
    public function getImgepi1()
    {
        return $this->imgepi1;
    }

    /**
     * Get imgepi2
     *
     * @return string
     */
    public function getImgepi2()
    {
        return $this->imgepi2;
    }

    /**
     * Get imgepi3
     *
     * @return string
     */
    public function getImgepi3()
    {
        return $this->imgepi3;
    }

    /**
     * Get imgepiok
     *
     * @return string
     */
    public function getImgepiok()
    {
        return $this->imgepiok;
    }

    /**
     * Get imgcomponente1
     *
     * @return string
     */
    public function getImgcomponente1()
    {
        return $this->imgcomponente1;
    }

    /**
     * Get imgcomponente2
     *
     * @return string
     */
    public function getImgcomponente2()
    {
        return $this->imgcomponente2;
    }

    /**
     * Get imgcomponente3
     *
     * @return string
     */
    public function getImgcomponente3()
    {
        return $this->imgcomponente3;
    }

    /**
     * Get imgcomponenteok
     *
     * @return string
     */
    public function getImgcomponenteok()
    {
        return $this->imgcomponenteok;
    }

    /**
     * Set imgrotar1
     *
     * @param string $imgrotar1
     * @return Detentrenamiento
     */
    public function setImgrotar1($imgrotar1)
    {
        $this->imgrotar1 = $imgrotar1;

        return $this;
    }

    /**
     * Get imgrotar1
     *
     * @return string
     */
    public function getImgrotar1()
    {
        return $this->imgrotar1;
    }

    /**
     * Set imgrotar2
     *
     * @param string $imgrotar2
     * @return Detentrenamiento
     */
    public function setImgrotar2($imgrotar2)
    {
        $this->imgrotar2 = $imgrotar2;

        return $this;
    }

    /**
     * Get imgrotar2
     *
     * @return string
     */
    public function getImgrotar2()
    {
        return $this->imgrotar2;
    }

    /**
     * Set imgrotar3
     *
     * @param string $imgrotar3
     * @return Detentrenamiento
     */
    public function setImgrotar3($imgrotar3)
    {
        $this->imgrotar3 = $imgrotar3;

        return $this;
    }

    /**
     * Get imgrotar3
     *
     * @return string
     */
    public function getImgrotar3()
    {
        return $this->imgrotar3;
    }

    /**
     * Set imgrotarok
     *
     * @param string $imgrotarok
     * @return Detentrenamiento
     */
    public function setImgrotarok($imgrotarok)
    {
        $this->imgrotarok = $imgrotarok;

        return $this;
    }

    /**
     * Get imgrotarok
     *
     * @return string
     */
    public function getImgrotarok()
    {
        return $this->imgrotarok;
    }

    /**
     * Set imgbase
     *
     * @param string $imgbase
     * @return Detentrenamiento
     */
    public function setImgbase($imgbase)
    {
        $this->imgbase = $imgbase;

        return $this;
    }

    /**
     * Get imgbase
     *
     * @return string
     */
    public function getImgbase()
    {
        return $this->imgbase;
    }

    /**
     * Set coordenadas
     *
     * @param string $coordenadas
     * @return Detentrenamiento
     */
    public function setCoordenadas($coordenadas)
    {
        $this->coordenadas = $coordenadas;

        return $this;
    }

    /**
     * Get coordenadas
     *
     * @return string
     */
    public function getCoordenadas()
    {
        return $this->coordenadas;
    }

    /**
     * Set coordenadas2
     *
     * @param string $coordenadas2
     * @return Detentrenamiento
     */
    public function setCoordenadas2($coordenadas2)
    {
        $this->coordenadas2 = $coordenadas2;

        return $this;
    }

    /**
     * Get coordenadas2
     *
     * @return string
     */
    public function getCoordenadas2()
    {
        return $this->coordenadas2;
    }

    /**
     * Set coordenadas3
     *
     * @param string $coordenadas3
     * @return Detentrenamiento
     */
    public function setCoordenadas3($coordenadas3)
    {
        $this->coordenadas3 = $coordenadas3;

        return $this;
    }

    /**
     * Get coordenadas3
     *
     * @return string
     */
    public function getCoordenadas3()
    {
        return $this->coordenadas3;
    }

    /**
     * Set coordenadasok
     *
     * @param string $coordenadasok
     * @return Detentrenamiento
     */
    public function setCoordenadasok($coordenadasok)
    {
        $this->coordenadasok = $coordenadasok;

        return $this;
    }

    /**
     * Get coordenadasok
     *
     * @return string
     */
    public function getCoordenadasok()
    {
        return $this->coordenadasok;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Detentrenamiento
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Detentrenamiento
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Detentrenamiento
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set tipoentrenamiendo_id
     *
     * @param integer $tipoentrenamiendoId
     * @return Detentrenamiento
     */
    public function setTipoentrenamiendoId($tipoentrenamiendoId)
    {
        $this->tipoentrenamiendo_id = $tipoentrenamiendoId;

        return $this;
    }

    /**
     * Get tipoentrenamiendo_id
     *
     * @return integer
     */
    public function getTipoentrenamiendoId()
    {
        return $this->tipoentrenamiendo_id;
    }

    /**
     * Set detformacion
     *
     * @param \Gitek\UdaBundle\Entity\Detformacion $detformacion
     * @return Detentrenamiento
     */
    public function setDetformacion(\Gitek\UdaBundle\Entity\Detformacion $detformacion = null)
    {
        $this->detformacion = $detformacion;

        return $this;
    }

    /**
     * Get detformacion
     *
     * @return \Gitek\UdaBundle\Entity\Detformacion
     */
    public function getDetformacion()
    {
        return $this->detformacion;
    }

    /**
     * Set entrenamiento
     *
     * @param \Gitek\UdaBundle\Entity\Entrenamiento $entrenamiento
     * @return Detentrenamiento
     */
    public function setEntrenamiento(\Gitek\UdaBundle\Entity\Entrenamiento $entrenamiento = null)
    {
        $this->entrenamiento = $entrenamiento;

        return $this;
    }

    /**
     * Get entrenamiento
     *
     * @return \Gitek\UdaBundle\Entity\Entrenamiento
     */
    public function getEntrenamiento()
    {
        return $this->entrenamiento;
    }

    /**
     * Set tipoentrenamiento
     *
     * @param \Gitek\UdaBundle\Entity\Tipoentrenamiento $tipoentrenamiento
     * @return Detentrenamiento
     */
    public function setTipoentrenamiento(\Gitek\UdaBundle\Entity\Tipoentrenamiento $tipoentrenamiento = null)
    {
        $this->tipoentrenamiento = $tipoentrenamiento;

        return $this;
    }

    /**
     * Get tipoentrenamiento
     *
     * @return \Gitek\UdaBundle\Entity\Tipoentrenamiento
     */
    public function getTipoentrenamiento()
    {
        return $this->tipoentrenamiento;
    }

    /**
     * Set imgepi1
     *
     * @param string $imgepi1
     * @return Detentrenamiento
     */
    public function setImgepi1($imgepi1)
    {
        $this->imgepi1 = $imgepi1;
    
        return $this;
    }

    /**
     * Set imgepi2
     *
     * @param string $imgepi2
     * @return Detentrenamiento
     */
    public function setImgepi2($imgepi2)
    {
        $this->imgepi2 = $imgepi2;
    
        return $this;
    }

    /**
     * Set imgepi3
     *
     * @param string $imgepi3
     * @return Detentrenamiento
     */
    public function setImgepi3($imgepi3)
    {
        $this->imgepi3 = $imgepi3;
    
        return $this;
    }

    /**
     * Set imgepiok
     *
     * @param string $imgepiok
     * @return Detentrenamiento
     */
    public function setImgepiok($imgepiok)
    {
        $this->imgepiok = $imgepiok;
    
        return $this;
    }

    /**
     * Set imgcomponente1
     *
     * @param string $imgcomponente1
     * @return Detentrenamiento
     */
    public function setImgcomponente1($imgcomponente1)
    {
        $this->imgcomponente1 = $imgcomponente1;
    
        return $this;
    }

    /**
     * Set imgcomponente2
     *
     * @param string $imgcomponente2
     * @return Detentrenamiento
     */
    public function setImgcomponente2($imgcomponente2)
    {
        $this->imgcomponente2 = $imgcomponente2;
    
        return $this;
    }

    /**
     * Set imgcomponente3
     *
     * @param string $imgcomponente3
     * @return Detentrenamiento
     */
    public function setImgcomponente3($imgcomponente3)
    {
        $this->imgcomponente3 = $imgcomponente3;
    
        return $this;
    }

    /**
     * Set imgcomponenteok
     *
     * @param string $imgcomponenteok
     * @return Detentrenamiento
     */
    public function setImgcomponenteok($imgcomponenteok)
    {
        $this->imgcomponenteok = $imgcomponenteok;
    
        return $this;
    }
}