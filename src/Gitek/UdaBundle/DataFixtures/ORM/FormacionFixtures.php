<?php

namespace Gitek\UdaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\UdaBundle\Entity\Formacion;

class FormacionFixtures extends AbstractFixture implements OrderedFixtureInterface
{
	public function load($manager)
	    {
	    	
	        $formacion1 = new Formacion();
	        $formacion1->setNombre('1-Colocar Subconjunto Potting');
	        $formacion1->setVersion('-1');
			$formacion1->setOrden(1);
	        $manager->persist($formacion1);
	
	        $manager->flush();

	        $this->addReference('formacion1', $formacion1);
	    }

	    public function getOrder()
	    {
	        return 1; // el orden en el que se deben cargar los accesorios
	    }
}