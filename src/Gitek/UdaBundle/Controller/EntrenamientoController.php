<?php

namespace Gitek\UdaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Gitek\UdaBundle\Entity\Detcurso;
use Gitek\UdaBundle\Entity\Entrenamiento;
use Gitek\UdaBundle\Entity\Detentrenamiento;
use Gitek\UdaBundle\Entity\Tipotrenamiento;

use Gitek\UdaBundle\Form\EntrenamientoType;
use Gitek\UdaBundle\Form\DetentrenamientoType;
use Gitek\UdaBundle\Form\TipoentrenamientoType;


/**
 * Entrenamiento controller.
 *
 * @Route("/entrenamiento")
 */
class EntrenamientoController extends Controller
{
    /**
     * Lists all Entrenamiento entities.
     *
     * @Route("/", name="entrenamiento")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entrenamientos = $em->getRepository('GitekUdaBundle:Entrenamiento')->findAll();

        return $this->render('GitekUdaBundle:Entrenamiento:index.html.twig', array(
	        'entrenamientos' => $entrenamientos
		));
    }

    /**
     * Lists all Entrenamiento entities.
     *
     * @Route("/", name="find")
     * @Template()
     */
    public function findAction() {
        $em = $this->getDoctrine()->getManager();

		$id = $this->get('request')->query->get('data');

		$em = $this->getDoctrine()->getManager();

		if (($id!=null) && ($id!="0")) {
			$consulta = $em->createQuery(
						"SELECT e FROM GitekUdaBundle:Detentrenamiento e
						WHERE e.entrenamiento_id = :mientrenamientoid")
						->setParameter('mientrenamientoid',$id);
							// "SELECT e FROM GitekUdaBundle:Entrenamiento e
							//  INNER JOIN e.detcursos d
							// WHERE d.curso_id = :micursoid")
							// ->setParameter('micursoid',$id);
					$entrenamientos = $consulta->getResult();

		} else {
			$entrenamientos = $em->getRepository('GitekUdaBundle:Entrenamiento')->findAll();
		}

				// var_dump($cursos);
        return array('entrenamientos' => $entrenamientos);
    }

		 /**
	     * Lists all Entrenamiento entities.
	     *
	     * @Route("/", name="findentrenamientos")
	     * @Template()
	     */
	    public function findentrenamientosAction() {
	        $em = $this->getDoctrine()->getManager();

			$id = $this->get('request')->query->get('data');

			$em = $this->getDoctrine()->getManager();

			if (($id!=null) && ($id!="0")) {
				$consulta = $em->createQuery(
								"SELECT e FROM GitekUdaBundle:Entrenamiento e
								 INNER JOIN e.detcursos d
								WHERE d.curso_id = :micursoid")
								->setParameter('micursoid',$id);
				$entrenamientos = $consulta->getResult();

				$consulta = $em->createQuery(
								"SELECT f FROM GitekUdaBundle:Detentrenamiento f
								INNER JOIN f.entrenamiento e
	 						 	INNER JOIN e.detcursos d
								WHERE d.curso_id = :micursoid")
								->setParameter('micursoid',$id);
				$detentrenamientos = $consulta->getResult();

			} else {
				$entrenamientos = $em->getRepository('GitekUdaBundle:Entrenamiento')->findAll();
				$detentrenamientos = $em->getRepository('GitekUdaBundle:Detentrenamiento')->findAll();
            }

		    return array(
			    'entrenamientos' => $entrenamientos,
			    'detentrenamientos' => $detentrenamientos
			);
	    }

	   /**
	     *
	     * @Route("/", name="finddetentrenamientos")
	     * @Template()
	     */
	    public function finddetentrenamientosAction()
	    {
			$em = $this->getDoctrine()->getManager();
			$id = $this->get('request')->query->get('data');

			if (($id!=null) && ($id!="0")) {
				$consulta = $em->createQuery(
									"SELECT f FROM GitekUdaBundle:Detentrenamiento f
									INNER JOIN f.entrenamiento e
									INNER JOIN e.detcursos d
									WHERE d.curso_id = :micursoid")
			                    ->setParameter('micursoid',$id);
				$entrenamientos = $consulta->getResult();

			} else {
				$entrenamientos = $em->getRepository('GitekUdaBundle:Detentrenamientos')->findAll();
			}

			return array('entrenamientos' => $entrenamientos);
	    }


    /**
     * Lists all Entrenamiento entities.
     *
     * @Route("/", name="copy1")
     */
    public function copy1Action() {
        $em = $this->getDoctrine()->getManager();

		$curso_id         = $this->get('request')->query->get('curso_id');
		$entrenamiento_id = $this->get('request')->query->get('entrenamiento_id');
		$detcurso_id      = $this->get('request')->query->get('detcurso_id');
		$mianchor         = $this->get('request')->query->get('mianchor');
		$mitab            = $this->get('request')->query->get('mitab');

		$cursos            = $em->getRepository('GitekUdaBundle:Curso')->findAll();
		$entrenamientos    = $em->getRepository('GitekUdaBundle:Entrenamiento')->getLatestEntrenamientos();
		$detentrenamientos = $em->getRepository('GitekUdaBundle:Detentrenamiento')->findAll();

    return $this->render('GitekUdaBundle:Entrenamiento:copy1.html.twig', array(
      'entrenamientos'    => $entrenamientos,
      'cursos'            => $cursos,
      'detentrenamientos' => $detentrenamientos,
      'curso_id'          => $curso_id,
      'entrenamiento_id'  => $entrenamiento_id,
      'detcurso_id'       => $detcurso_id,
      'mianchor'          => $mianchor,
      'mitab'             => $mitab,
    ));

    }

    /**
    * Lists all Formacion entities.
    *
    * @Route("/", name="copy2")
    * @Template()
    */
	  public function copy2Action()
	    {
				$request = $this->getRequest();

				//Obtenemos los seleccionados (Tenemos su ID)
				$miarray = $request->request->get('seleccionados');

				$em = $this->getDoctrine()->getManager();

				//Aqui tenemos el ID del curso
				$mi_curso = $request->request->get('curso_id');
				//Aqui tenemos el ID del Entrenamiento
				$entrenamiento_id = $request->request->get('entrenamiento_id');
				$mianchor 		= $request->request->get('mianchor');
				$mitab 				= $request->request->get('mitab');
				$detcurso_id = $request->request->get('detcurso_id');


				$cont=0;

				if ($entrenamiento_id ==null)
				{
					//creamos nueva Entrenamiento, basandonos en el seleccionado (Copiamos el nombre)
					$mientrenamiento  = new Entrenamiento();
					$f = $em->getRepository('GitekUdaBundle:Entrenamiento')->find($m);
					$mientrenamiento->setNombre($f->getNombre());
					$em->persist($mientrenamiento);

				} else {
					//editamos la formación
					$mientrenamiento  = new Entrenamiento();
					$mientrenamiento = $em->getRepository('GitekUdaBundle:Entrenamiento')->find($entrenamiento_id);
					if ($mientrenamiento!=null){
					}

				}

			  foreach ($miarray as $m) {
					$cont+=1;

					//copiamos
						$c = $this->getDetalleentrenamiento($m);
						$midete = new Detentrenamiento();

                        // if ($c->getDetformacion()) {
                        //     $midete->setDetformacion($c->getDetformacion());
                        // }

						$midete->setEntrenamiento($mientrenamiento);
						$midete->setTipoentrenamiento($c->getTipoentrenamiento());
						$midete->setNombre($c->getNombre());
						$midete->setPregunta($c->getPregunta());
                        $midete->setPregunta2($c->getPregunta2());
                        $midete->setPregunta3($c->getPregunta3());
						$midete->setImg1($c->getImg1());
						$midete->setImg2($c->getImg2());
						$midete->setImg3($c->getImg3());
						$midete->setImgok($c->getImgok());

						$midete->setImgepi1($c->getImgepi1());
						$midete->setImgepi2($c->getImgepi2());
						$midete->setImgepi3($c->getImgepi3());
						$midete->setImgepiok($c->getImgepiok());

						$midete->setImgcomponente1($c->getImgcomponente1());
						$midete->setImgcomponente2($c->getImgcomponente2());
						$midete->setImgcomponente3($c->getImgcomponente3());
						$midete->setImgcomponenteok($c->getImgcomponenteok());

						$midete->setImgrotar1($c->getImgrotar1());
						$midete->setImgrotar2($c->getImgrotar2());
						$midete->setImgrotar3($c->getImgrotar3());
						$midete->setImgrotarok($c->getImgrotarok());
						$midete->setImgbase($c->getImgbase());

						$midete->setCoordenadas($c->getCoordenadas());
						$midete->setCoordenadas2($c->getCoordenadas2());
						$midete->setCoordenadasok($c->getCoordenadasok());

						$em->persist($midete);

					//obtenemos datos del curso
					$micursocomodin = $this->getCurso($mi_curso);

					if ($detcurso_id!=null) {

					} else{
						//Creamos nuevo DetCurso para el curso

						$comodines = $this->getSiguienteorden($mi_curso);
                        $comodin = $comodines[0];

						$midetcurso = New Detcurso();
						$midetcurso->setNombre("Copia $cont");
						$midetcurso->setOrden($comodin->getOrden()+1);
						$midetcurso->setFormacion($miformacion);
						$midetcurso->setCurso($micursocomodin);
						$em->persist($midetcurso);

					}
				}

	      $em->flush();

				// return $this->redirect($this->generateUrl('curso_show', array('id' => $mi_curso)));

				$url = $this->generateUrl('curso_show', array('id' => $mi_curso));
				return $this->redirect(
				    sprintf('%s#%s--%s', $url, $mianchor,$mitab)
				);

	    }

	protected function getDetalleentrenamiento($miid){
	        $em = $this->getDoctrine()->getManager();
	        $form = $em->getRepository('GitekUdaBundle:Detentrenamiento')->find($miid);
	        if (!$form) {
	            throw $this->createNotFoundException('Unable to find detcurso post.');
	        }
	        return $form;
	    }


	protected function getCurso($miid){
	        $em = $this->getDoctrine()->getManager();
	        $form = $em->getRepository('GitekUdaBundle:Curso')->find($miid);
	        if (!$form) {
	            throw $this->createNotFoundException('Unable to find detcurso post.');
	        }
	        return $form;
	}

    protected function getEnt($m){
        $em = $this->getDoctrine()->getManager();
        $entre = $em->getRepository('GitekUdaBundle:Entrenamiento')->find($m);
        if (!$entre) {
            throw $this->createNotFoundException('Unable to find detcurso post.');
        }
        return $entre;
    }


    protected function getDete($m){
		$repositorio = $this->getDoctrine()
		    ->getRepository('GitekUdaBundle:Detentrenamiento');

		$consulta = $repositorio->createQueryBuilder('p')
		    ->where('p.entrenamiento_id = :miid')
		    ->setParameter('miid', $m)
		    ->getQuery();

		$productos = $consulta->getResult();
		return $productos;
    }

    /**
     * Finds and displays a Entrenamiento entity.
     *
     * @Route("/{id}/show", name="entrenamiento_show")
     * @Template()
     */
    public function showAction($id, $curso_id=null)    {
        $em = $this->getDoctrine()->getManager();

        $entrenamiento = $em->getRepository('GitekUdaBundle:Entrenamiento')->find($id);

		$curso_id = $this->get('request')->query->get('curso_id');
		$curso = $this->getCurso($curso_id);

		$mianchor = $this->get('request')->query->get('mianchor');
		$mitab = $this->get('request')->query->get('mitab');

        if (!$entrenamiento) {
            throw $this->createNotFoundException('Unable to find Entrenamiento entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

				// Detalle Entrenamiento
        $detentrenamiento = new Detentrenamiento();
        $formdetentrenamiento   = $this->createForm(new DetentrenamientoType(), $detentrenamiento);

        return array(
            'entrenamiento'      		=> $entrenamiento,
            'delete_form' 					=> $deleteForm->createView(),
						'detentrenamiento' 			=> $detentrenamiento,
            'formdetentrenamiento'  => $formdetentrenamiento->createView(),
						'curso_id'							=> $curso_id,
						'curso'									=> $curso,
						'mianchor'							=> $mianchor,
						'mitab'									=> $mitab,
       );
    }


    /**
     * Displays a form to create a new Entrenamiento entity.
     *
     * @Route("/new", name="entrenamiento_new")
     * @Template()
     */
    public function newAction() {
		$request = $this->getRequest();
		// $tipoentrenamiento_id = $request->request->get('tipoentrenamiento_id');

		// $tipoentrenamiento = $this->getTipoentrenamiento($tipoentrenamiento_id);

		$entrenamiento = new Entrenamiento();
		// $entrenamiento->setTipoentrenamiento($tipoentrenamiento);
		$form   = $this->createForm(new EntrenamientoType(), $entrenamiento);

	    return array(
	    	'entrenamiento' => $entrenamiento,
	     	'form'   => $form->createView()
		);
    }

    protected function getTipoentrenamiento($tipoentrenamiento_id){
        $em = $this->getDoctrine()
                    ->getEntityManager();

        $tipoentrenamiento = $em->getRepository('GitekUdaBundle:Tipoentrenamiento')->find($tipoentrenamiento_id);

        if (!$tipoentrenamiento) {
            throw $this->createNotFoundException('Unable to find formacion post.');
        }

        return $tipoentrenamiento;
    }

    /**
     * Creates a new Entrenamiento entity.
     *
     * @Route("/create", name="entrenamiento_create")
     * @Method("post")
     * @Template("GitekUdaBundle:Entrenamiento:new.html.twig")
     */
    public function createAction(){
        $entity  = new Entrenamiento();
        $request = $this->getRequest();
        $form    = $this->createForm(new EntrenamientoType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('entrenamiento_show', array('id' => $entity->getId())));

        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    public function createx7Action($entrenamiento_id=null,$curso_id=null,$mianchor=null,$mitab=null)	{
    	// print_r("Entrenamiento id: " .$entrenamiento_id);
    	// print_r("<br />");
    	// print_r("Curso id : " . $curso_id);
    	// print_r("<br />");


		$em = $this->getDoctrine()->getManager();
        $entrenamiento = $this->getEnt($entrenamiento_id);
		$mitipo = $em->getRepository('GitekUdaBundle:Tipoentrenamiento')->findOneBy(array('tipo' => 'X7'));
		$curso = $this->getCurso($curso_id);

        $detentrenamiento = new Detentrenamiento();
		$detentrenamiento->setNombre($entrenamiento->getNombre());
		$detentrenamiento->setPregunta('Seleccione componente correcto');
        $detentrenamiento->setPregunta2('Seleccione Epi correcto');
        $detentrenamiento->setPregunta3('Comprueba el componente');
		$detentrenamiento->setEntrenamiento($entrenamiento);
		$detentrenamiento->setTipoentrenamiento($mitipo);

		$em->persist($detentrenamiento);
    	$em->flush();

        return $this->redirect(
			$this->generateUrl('detentrenamiento_edit',
						array(
                            'id'       => $detentrenamiento->getId(),
                            'curso_id' => $curso_id,
                            'mianchor' => $mianchor,
                            'mitab'    => $mitab,
							)
						));
    }

    public function create1imgAction($entrenamiento_id=null,$curso_id=null,$mianchor=null,$mitab=null)
    {
		$em = $this->getDoctrine()->getManager();

		$entrenamiento = $this->getEnt($entrenamiento_id);
		$mitipo = $em->getRepository('GitekUdaBundle:Tipoentrenamiento')->findOneBy(array('tipo' => '1-Imagen'));
		$curso = $this->getCurso($curso_id);

        $detentrenamiento = new Detentrenamiento();
		$detentrenamiento->setNombre('Operación 1 Imagen');
		$detentrenamiento->setPregunta('Seleccione componente correcto');
		$detentrenamiento->setEntrenamiento($entrenamiento);
		$detentrenamiento->setTipoentrenamiento($mitipo);

		$em->persist($detentrenamiento);
   		$em->flush();

        return $this->redirect(
				$this->generateUrl('detentrenamiento_edit',
						array('id' 			=> $detentrenamiento->getId(),
									'curso_id'=> $curso_id,
									'mianchor'=> $mianchor,
									'mitab'		=> $mitab,
									)
							));
    }

    public function create4imgAction($entrenamiento_id=null,$curso_id=null,$mianchor=null,$mitab=null)
    {
				$em = $this->getDoctrine()->getManager();

				$entrenamiento = $this->getEnt($entrenamiento_id);
				$mitipo = $em->getRepository('GitekUdaBundle:Tipoentrenamiento')->findOneBy(array('tipo' => '4-Imagen'));
				$curso = $this->getCurso($curso_id);

        $detentrenamiento = new Detentrenamiento();
				$detentrenamiento->setNombre('Operación 4 Imagen');
				$detentrenamiento->setPregunta('Seleccione componente correcto');
				$detentrenamiento->setEntrenamiento($entrenamiento);
				$detentrenamiento->setTipoentrenamiento($mitipo);

				$em->persist($detentrenamiento);
    		$em->flush();

        return $this->redirect(
					$this->generateUrl('detentrenamiento_edit',
						array('id' 			=> $detentrenamiento->getId(),
									'curso_id'=> $curso_id,
									'mianchor'=> $mianchor,
									'mitab'		=> $mitab,
									)
									));
    }

    public function createbaseimgAction($entrenamiento_id=null,$curso_id=null,$mianchor=null,$mitab=null)
    {
				$em = $this->getDoctrine()->getManager();

				$entrenamiento = $this->getEnt($entrenamiento_id);
				$mitipo = $em->getRepository('GitekUdaBundle:Tipoentrenamiento')->findOneBy(array('tipo' => 'Colocar componente'));
				$curso = $this->getCurso($curso_id);

        $detentrenamiento = new Detentrenamiento();
				$detentrenamiento->setNombre('Operación Colocar componente');
				$detentrenamiento->setPregunta('Seleccione componente correcto');
				$detentrenamiento->setEntrenamiento($entrenamiento);
				$detentrenamiento->setTipoentrenamiento($mitipo);

				$em->persist($detentrenamiento);
    		$em->flush();

        return $this->redirect(
					$this->generateUrl('detentrenamiento_edit',
						array('id' 			=> $detentrenamiento->getId(),
									'curso_id'=> $curso_id,
									'mianchor'=> $mianchor,
									'mitab'		=> $mitab,
									)
									));
    }

    /**
     * Displays a form to edit an existing Entrenamiento entity.
     *
     * @Route("/{id}/edit", name="entrenamiento_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Entrenamiento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Entrenamiento entity.');
        }

        $editForm = $this->createForm(new EntrenamientoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Entrenamiento entity.
     *
     * @Route("/{id}/update", name="entrenamiento_update")
     * @Method("post")
     * @Template("GitekUdaBundle:Entrenamiento:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Entrenamiento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Entrenamiento entity.');
        }

        $editForm   = $this->createForm(new EntrenamientoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('entrenamiento_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Entrenamiento entity.
     *
     * @Route("/{id}/delete", name="entrenamiento_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GitekUdaBundle:Entrenamiento')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Entrenamiento entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('entrenamiento'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

	public function seleccionAction() {
		return $this->render('GitekUdaBundle:Entrenamiento:selecciona.html.twig');
	}

    protected function getSiguienteorden($curso_id) {
        $em = $this->getDoctrine()->getManager();

        $consulta = $em->createQuery(
                "SELECT d FROM GitekUdaBundle:Detcurso d
                WHERE d.curso_id = :cursoid
                ORDER BY d.orden DESC
                ");
        $consulta->setParameter('cursoid',$curso_id);
        $consulta->setMaxResults(1);

        $det = $consulta->getResult();

        return $det;

    }

}
