<?php


namespace Gitek\UdaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Gitek\UdaBundle\Entity\Detentrenamiento;
use Gitek\UdaBundle\Entity\Tipoentrenamiento;
use Gitek\UdaBundle\Form\DetentrenamientoType;
use Gitek\UdaBundle\Form\TipoentrenamientoType;

/**
 * Detentrenamiento controller.
 */
class DetentrenamientoController extends Controller
{
    public function newAction($entrenamiento_id, $curso_id=null)
    {
        $entrenamiento = $this->getEntrenamiento($entrenamiento_id);

        $detentrenamiento = new Detentrenamiento();
        $detentrenamiento->setEntrenamiento($entrenamiento);
        $form   = $this->createForm(new DetentrenamientoType(), $detentrenamiento);

        return $this->render('GitekUdaBundle:Detentrenamiento:form.html.twig', array(
            'detentrenamiento' => $detentrenamiento,
            'form'   => $form->createView(),
						'curso_id'=> $curso_id
        ));
    }

    public function createAction(Request $request, $entrenamiento_id, $curso_id=null)
    {
        $entrenamiento = $this->getEntrenamiento($entrenamiento_id);

        $detentrenamiento  = new Detentrenamiento();
        $detentrenamiento->setEntrenamiento($entrenamiento);
        $request = $this->getRequest();
        $form    = $this->createForm(new DetentrenamientoType(), $detentrenamiento);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

			$em->persist($detentrenamiento);
			$em->flush();

			$curso_id = $this->get('request')->query->get('curso_id');
			//	var_dump($curso_id);
			return $this->redirect($this->generateUrl('curso_show', array('id'=>$curso_id)));
        }

        return $this->render('GitekUdaBundle:Detentrenamiento:create.html.twig', array(
            'detentrenamiento' => $detentrenamiento,
            'form'    => $form->createView()
        ));
    }


    public function editAction(Request $request, $id, $curso_id=null, $mianchor=null, $mitab=null)
    {


		$em = $this->getDoctrine()->getManager();

        $detentrenamiento = $em->getRepository('GitekUdaBundle:Detentrenamiento')->find($id);
        $laguntzak = $em->getRepository('GitekUdaBundle:Laguntza')->findAll(array("mostrar"=>"1"));

        if (!$detentrenamiento) {
            throw $this->createNotFoundException('Unable to find Detentrnamiento entity.');
        }

        // $form = $this->createForm(new DetentrenamientoType(), $detentrenamiento, array('miid' => $id));
        $form = $this->createForm(new DetentrenamientoType(), $detentrenamiento);


		$deleteForm = $this->createDeleteForm($id);

		$peticion = $this->getRequest();
		$curso = $this->getCurso($curso_id);

        $formacionesasociadas = $em->getRepository('GitekUdaBundle:Detformacion')
                        ->getFormacionesasociadas($curso->getId());

        if ($peticion->getMethod() == 'POST' ) {

            $form->bind($peticion);

            // if ($form->isValid()) {
				$detentrenamiento->setUpdatedAt(new \DateTime());
                $em->persist($detentrenamiento);
                $em->flush();

                // $peticion->getSession()->setFlash('notice', 'Datos guardados con éxito.');
                $this->get('session')->getFlashBag()->add('notice', 'Datos guardados con éxito.');

				$niretab = $peticion->request->get('niretab');

    			$url = $this->generateUrl(
                    'detentrenamiento_edit', array(
                        'id'       => $id,
                        'curso_id' => $curso_id,
                        'mianchor' => $mianchor,
                        'mitab'    => $mitab,
                        'formacionesasociadas' => $formacionesasociadas,
                    ));
				return $this->redirect(
                    sprintf('%s#%s', $url, $niretab)
				);
            // }
        }
        return $this->render('GitekUdaBundle:Detentrenamiento:edit.html.twig', array(
            'form'        => $form->createView(),
            'entity'      => $detentrenamiento,
            'laguntzak'     => $laguntzak,
            'delete_form' => $deleteForm->createView(),
            'id'          => $id,
            'curso_id'    => $curso_id,
            'mianchor'    => $mianchor,
            'mitab'       => $mitab,
            'curso'       => $curso,
            'formacionesasociadas' => $formacionesasociadas,
        ));
    }

    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Detentrenamiento')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Detentrnamiento entity.');
        }

        $editForm   = $this->createForm(new DetentrnamientoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('detentrenamiento_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    public function deleteAction(Request $request, $id, $curso_id=null, $mianchor=null, $mitab=null)
    {
      $form = $this->createDeleteForm($id);
      $request = $this->getRequest();

      $form->bind($request);


      // if ($form->isValid()) {

          $em = $this->getDoctrine()->getManager();
          $detentrenamiento = $em->getRepository('GitekUdaBundle:Detentrenamiento')->find($id);

          if (!$detentrenamiento) {
              // throw $this->createNotFoundException('Unable to find detentrenamiento entity.');
            $url = $this->generateUrl('curso_show', array('id' => $curso_id));

            return $this->redirect(
                  sprintf('%s#%s--%s', $url, $mianchor,$mitab)
              );
          }

          $em->remove($detentrenamiento);
          $em->flush();
      // }

      $url = $this->generateUrl('curso_show', array('id' => $curso_id));

      return $this->redirect(
                  sprintf('%s#%s--%s', $url, $mianchor,$mitab)
              );

    }

    protected function getEntrenamiento($entrenamiento_id)
    {
        $em = $this->getDoctrine()
                    ->getEntityManager();

        $entrenamiento = $em->getRepository('GitekUdaBundle:Entrenamiento')->find($entrenamiento_id);

        if (!$entrenamiento) {
            throw $this->createNotFoundException('Unable to find entrenamiento post.');
        }

        return $entrenamiento;
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    public function volverAction($curso_id=null,$mianchor=null,$mitab=null)
    {
			$url = $this->generateUrl('curso_show', array('id' => $curso_id));
			return $this->redirect(
				sprintf('%s#%s--%s', $url, $mianchor,$mitab)
			);
		}

	 protected function getCurso($id)
    {
        $em = $this->getDoctrine()->getManager();
        $curso = $em->getRepository('GitekUdaBundle:Curso')->find($id);
        if (!$curso) {
            throw $this->createNotFoundException('Unable to find curso post.');
        }
        return $curso;
    }

}