<?php

namespace Gitek\UdaBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Gitek\UdaBundle\Entity\Curso;

class CursoFixtures extends AbstractFixture implements OrderedFixtureInterface
{
	public function load($manager)
	    {
	    	
	        $curso1 = new Curso();
	        $curso1->setNombre('Curso ejemplo');
	        $curso1->setTiempo(120);
	        $manager->persist($curso1);

	        $this->addReference('curso1', $curso1);
	    }

	    public function getOrder()
	    {
	        return 6; // el orden en el que se deben cargar los accesorios
	    }
}