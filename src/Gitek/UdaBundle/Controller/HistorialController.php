<?php

namespace Gitek\UdaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Gitek\UdaBundle\Entity\Historial;
use Gitek\UdaBundle\Entity\Dethistorial;
use Gitek\UdaBundle\Form\HistorialType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Historial controller.
 *
 * @Route("/historial")
 */
class HistorialController extends Controller
{

    public function portadaAction() {
        return $this->render('GitekUdaBundle:Historial:portada.html.twig');
    }

    /**
     * Lists all Historial entities.
     *
     * @Route("/", name="historial")
     * @Template()
     */
    public function indexAction() {

		$request    = $this->getRequest();
        $aprobado   =null;
		$tipo		=null;
		$taller_id	=null;
		$operario_id=null;
		$curso_id	=null;
        $operacion_id=null;
		$hayfiltro  =0;

		$tipo		 	= $request->request->get('tipo');
        $aprobado       = $request->request->get('aprobado');
		$taller_id 		= $request->request->get('taller_id');
		$operario_id 	= $request->request->get('operario_id');
		$curso_id 		= $request->request->get('curso_id');
        $operacion_id   = $request->request->get('operacion_id');

        $em = $this->getDoctrine()->getManager();
		$repo = $em->getRepository('GitekUdaBundle:Historial');

		if ( ( $tipo!=null ) && ( $tipo!="0" ) ) {
			$criteria['tipo'] = $tipo;
			$hayfiltro  =1;
		}

        if ( ( $aprobado!=null ) && ( $aprobado!="0" ) ) {
            $criteria['aprobado'] = $aprobado;
            $hayfiltro  =1;
        }

		if ( ( $taller_id!=null ) && ( $taller_id!="0" ) ) {
			$criteria['taller_id'] = $taller_id;
			$hayfiltro  =1;
		}

		if (( $operario_id!=null ) && ( $operario_id!="0")) {
			$criteria['operario_id'] = $operario_id;
			$hayfiltro  =1;
		}

		if (( $curso_id!=null ) && ( $curso_id!="0" ) ) {
			$criteria['curso_id'] = $curso_id;
			$hayfiltro  =1;
		}

        if (( $operacion_id!=null ) && ( $operacion_id!="0" ) ) {
            $criteria['operacion_id'] = $operacion_id;
            $hayfiltro  =1;
        }

		if ( $hayfiltro==0 ) {
			$entities = $em->getRepository('GitekUdaBundle:Historial')->findAll();
		} else {
			$entities = $repo->findBy($criteria);
		}

		$talleres		= $em->getRepository('GitekUdaBundle:Taller')->findAll();
		// $operarios 	    = $em->getRepository('GitekUdaBundle:Operario')->findOperariosordenados();
        $operarios      = $repo->getOperariosordenados();
        $laguntzak = $em->getRepository('GitekUdaBundle:Laguntza')->findAll(array("mostrar"=>"1"));
		$cursos			= $em->getRepository('GitekUdaBundle:Curso')->findAll();
        $operaciones    = $em->getRepository('GitekUdaBundle:Detcurso')->findAll();


        return $this->render('GitekUdaBundle:Historial:index.html.twig', array(
			'entities' 		=> $entities,
			'tipo'			=> $tipo,
			'talleres'		=> $talleres,
			'operarios'		=> $operarios,
			'cursos'		=> $cursos,
            'operaciones'   => $operaciones,
			'taller_id'		=> $taller_id,
			'operario_id'	=> $operario_id,
			'curso_id'		=> $curso_id,
            'laguntzak'     => $laguntzak,
            'operacion_id'  => $operacion_id,
		));
    }

    /**
     * Finds and displays a Historial entity.
     *
     * @Route("/{id}/show", name="historial_show")
     * @Template()
     */
    public function showAction($id){
        $em = $this->getDoctrine()->getManager();

        $historial = $em->getRepository('GitekUdaBundle:Historial')->find($id);
				// $curso = $em->getRepository('GitekUdaBundle:Curso')->find($historial->getCursoId());

        $laguntzak = $em->getRepository('GitekUdaBundle:Laguntza')->findAll(array("mostrar"=>"1"));

        if (!$historial) {
            throw $this->createNotFoundException('Unable to find Historial entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GitekUdaBundle:Historial:show.html.twig', array(
            'historial'		=> $historial,
            'laguntzak'     => $laguntzak,
						// 'curso'				=> $curso,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to create a new Historial entity.
     *
     * @Route("/new", name="historial_new")
     * @Template()
     */
    public function newAction(){
        $entity = new Historial();
        $form   = $this->createForm(new HistorialType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * Creates a new Historial entity.
     *
     * @Route("/create", name="historial_create")
     * @Method("post")
     * @Template("GitekUdaBundle:Historial:new.html.twig")
     */
    public function createAction(){
        $entity  = new Historial();
        $request = $this->getRequest();
        $form    = $this->createForm(new HistorialType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('historial_show', array('id' => $entity->getId())));

        }

        return $this->render('GitekUdaBundle:Historial:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Historial entity.
     *
     * @Route("/{id}/edit", name="historial_edit")
     * @Template()
     */
    public function editAction($id){
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Historial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Historial entity.');
        }

        $editForm = $this->createForm(new HistorialType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GitekUdaBundle:Historial:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Historial entity.
     *
     * @Route("/{id}/update", name="historial_update")
     * @Method("post")
     * @Template("GitekUdaBundle:Historial:edit.html.twig")
     */
    public function updateAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GitekUdaBundle:Historial')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Historial entity.');
        }

        $editForm   = $this->createForm(new HistorialType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('historial_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Historial entity.
     *
     * @Route("/{id}/delete", name="historial_delete")
     * @Method("post")
     */
    public function deleteAction($id) {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('GitekUdaBundle:Historial')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Historial entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('historial'));
    }

    private function createDeleteForm($id) {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    public function kaixoAction() {
        $em = $this->getDoctrine()->getManager();

        $talleres   = $em->getRepository('GitekUdaBundle:Taller')->findAll();
        $operarios  = $em->getRepository('GitekUdaBundle:Operario')->findAll();
        $cursos     =$em->getRepository('GitekUdaBundle:Curso')->findAll();
        $laguntzak = $em->getRepository('GitekUdaBundle:Laguntza')->findAll(array("mostrar"=>"1"));

        return $this->render('GitekUdaBundle:Historial:selecciona.html.twig', array(
            'talleres'  => $talleres,
            'operarios' => $operarios,
            'cursos'    => $cursos,
            'laguntzak' => $laguntzak,
        ));

    }

    public function kaixoAction2() {
        $em = $this->getDoctrine()->getManager();

        $talleres   = $em->getRepository('GitekUdaBundle:Taller')->findAll();
		$operarios  = $em->getRepository('GitekUdaBundle:Operario')->findAll();
		$cursos     =$em->getRepository('GitekUdaBundle:Curso')->findAll();

		return $this->render('GitekUdaBundle:Historial:selecciona.html.twig', array(
            'talleres' 	=> $talleres,
            'operarios' => $operarios,
            'cursos'	=> $cursos,
        ));

    }

    public function hautatuAction() {
        $request = $this->getRequest();

        $taller_id = $request->request->get('taller_id');
        $operario_id = $request->request->get('operario_id');
        $curso_id = $request->request->get('curso_id');

        $curso = $this->getCurso($curso_id);

        $em = $this->getDoctrine()->getManager();
        $operaciones = $em->getRepository('GitekUdaBundle:Detcurso')->getOperaciones($curso_id);
        return $this->render('GitekUdaBundle:Historial:hautatu.html.twig',	array(
                                'taller_id'		=> $taller_id,
                                'operario_id'	=> $operario_id,
                                'curso_id'		=> $curso_id,
                                'curso'         => $curso,
                                'operaciones'	=> $operaciones
        ));
    }

	public function testakAction() {
        $request = $this->getRequest();

        $taller_id      = $request->request->get('taller_id');
        $operario_id    = $request->request->get('operario_id');
        $curso_id       = $request->request->get('curso_id');

        $curso = $this->getCurso($curso_id);

        $em = $this->getDoctrine()->getManager();

		$testak = 	$em->getRepository('GitekUdaBundle:Historial')->getTestak($taller_id,$operario_id,$curso_id);

        $operaciones = $em->getRepository('GitekUdaBundle:Detcurso')->getOperaciones($curso_id);

        $session = $this->getRequest()->getSession();
        $session->set('validacion', '1');

        return $this->render('GitekUdaBundle:Historial:testak.html.twig',	array(
                                'taller_id'		=> $taller_id,
                                'operario_id'	=> $operario_id,
                                'curso_id'		=> $curso_id,
                                'tiempo'        => $curso->getTiempo(),
                                'operaciones'	=> $testak
        ));
    }

    public function eragiketaAction() {
        $request 		= $this->getRequest();

        $taller_id 		= $request->request->get('taller_id');
        $operario_id 	= $request->request->get('operario_id');
        $curso_id	 	= $request->request->get('curso_id');

        if ( $request->request->get('taller_id') != null ) {
            // Guardamos los datos en session
            $session = $this->getRequest()->getSession();
            $session->set('taller_id', 		$taller_id);
            $session->set('operario_id', 	$operario_id);
            $session->set('curso_id', 		$curso_id);
        } else {
            $session 		= $this->getRequest()->getSession();
            $taller_id 		= $session->get('taller_id');
            $operario_id 	= $session->get('operario_id');
            $curso_id 		= $session->get('curso_id');
        }

		// comprobamos si tiene puede realizar algún examen

		$historiales = $this->getHistorialparaexamen($taller_id,$operario_id,$curso_id);
		$completado = 0;
		if ($historiales) {
			foreach ( $historiales as $historial ) {
				if (($historial->getCompletado()==1) && ($historial->getAprobado()==0)) {
					$completado = 1;
                    break;
				} else {
					$completado = 0;
				}
			}
		}

        $curso = $this->getCurso($curso_id);

        $session = $this->getRequest()->getSession();
		$session->set('curso_nombre', $curso->getNombre());

        return $this->render('GitekUdaBundle:Historial:eragiketa.html.twig',	array(
                                'taller_id'		=> $taller_id,
                                'operario_id'	=> $operario_id,
                                'curso_id'		=> $curso_id,
                                'curso'			=> $curso,
								'completado'	=> $completado,
        ));
    }

    public function selentrenamentuakAction() {
        $request = $this->getRequest();

        $taller_id = $request->request->get('taller_id');
        $operario_id = $request->request->get('operario_id');
        $curso_id = $request->request->get('curso_id');

        $em = $this->getDoctrine()->getManager();
        $operaciones = $em->getRepository('GitekUdaBundle:Detcurso')->getOperaciones($curso_id);

        $session = $this->getRequest()->getSession();
        $session->set('validacion', '0');

        return $this->render('GitekUdaBundle:Historial:selentrenamentua.html.twig',	array(
                                'taller_id'		=> $taller_id,
                                'operario_id'	=> $operario_id,
                                'curso_id'		=> $curso_id,
                                'operaciones'	=> $operaciones
        ));
    }

    public function prestatuAction() {
        //leemos datos
 		$request = $this->getRequest();
		$multiseleccionados[] = $request->request->get('operaciones');

		$taller_id = $request->request->get('taller_id');
		$operario_id = $request->request->get('operario_id');
		$curso_id = $request->request->get('curso_id');

		$curso 		= $this->getCurso($curso_id);
		$taller 	= $this->getTaller($taller_id);
		$operario = $this->getOperario($operario_id);

		$em = $this->getDoctrine()->getManager();

        foreach ( $multiseleccionados as $seleccionados ) {
        	$cont=-1;

            foreach ( $seleccionados as $seleccion ) {
                // Ahora en $seleccion tenemos el ID del Entrenamiento. Tenemos que rellenar los datos en Historial/DetHistorial
				$cont+=1;

                if ( $cont==0	) { // Solo creamos la primera vez, los demás serán los detaller
                    $mihistorial = New Historial();
                    $mihistorial->setOperario($operario);
                    $mihistorial->setTaller($taller);
                    $mihistorial->setCurso($curso);
                    $mihistorial->setHoraini(new \DateTime());
                    $mihistorial->setCreatedat(new \DateTime());
                    $mihistorial->setUpdatedat(new \DateTime());
					$mihistorial->setTipo("Simulación");
                    $em->persist($mihistorial);
				}

                $detcurso      = $this->getDetcurso($seleccion);
                $entrenamiento = $this->getEntrenamiento($detcurso->getEntrenamientoId());
                $detalles      = $this->getDetalleentrenamiento($entrenamiento->getId());

				$orden=0;
                $iker = 0;
				foreach ( $detalles as $detalle ) {
                    $iker+=1;
                    $orden+=1;
                    $vv = $detalle->getTipoentrenamiento()->getTipo();
                    $midethistorial = New Dethistorial();
					$midethistorial->setDetcurso($detcurso);
                    $midethistorial->setOrden($orden);
                    $midethistorial->setHistorial($mihistorial);
                    $midethistorial->setEntrenamiento($entrenamiento);
                    $midethistorial->setNombre($detalle->getNombre());
                    $midethistorial->setPregunta($detalle->getPregunta());
                    $midethistorial->setTipoentrenamiento($detalle->getTipoentrenamiento());

                    if ( $orden == 1 ) { // aqui cualquier cosa, y si es X7 componente correcto
                        $midethistorial->setImg1($detalle->getImg1());
                        $midethistorial->setImg2($detalle->getImg2());
                        $midethistorial->setImg3($detalle->getImg3());
                        $midethistorial->setImgok($detalle->getImgok());
                        $midethistorial->setDetformacion($detalle->getDetformacion());
                        $midethistorial->setEntrenamientoId($entrenamiento->getId());
                        $orden+=1;
                        $em->persist($midethistorial);
                    }

                    if ( $vv == 'X7' ) {
                        //al ser X7 llenamos los otros campos
                        if ( $orden==2 ) {
                            $midethistorial = New Dethistorial();
							$midethistorial->setDetcurso($detcurso);
                            $midethistorial->setOrden($orden);
                            $midethistorial->setHistorial($mihistorial);
                            $midethistorial->setEntrenamiento($entrenamiento);
                            $midethistorial->setNombre($detalle->getNombre());
                            $midethistorial->setPregunta($detalle->getPregunta2());
                            $midethistorial->setTipoentrenamiento($detalle->getTipoentrenamiento());
                            $midethistorial->setImgepi1($detalle->getImgepi1());
                            $midethistorial->setImgepi2($detalle->getImgepi2());
                            $midethistorial->setImgepi3($detalle->getImgepi3());
                            $midethistorial->setImgepiok($detalle->getImgepiok());
                            $midethistorial->setDetformacion($detalle->getDetformacion());
                            $midethistorial->setEntrenamientoId($entrenamiento->getId());
                            $em->persist($midethistorial);
                            $orden+=1;
                        }
                        if ( $orden==3 ) {
                            $midethistorial = New Dethistorial();
							$midethistorial->setDetcurso($detcurso);
                            $midethistorial->setOrden($orden);
                            $midethistorial->setHistorial($mihistorial);
                            $midethistorial->setEntrenamiento($entrenamiento);
                            $midethistorial->setNombre($detalle->getNombre());
                            $midethistorial->setPregunta($detalle->getPregunta3());
                            $midethistorial->setTipoentrenamiento($detalle->getTipoentrenamiento());
                            $midethistorial->setImgcomponente1($detalle->getImgcomponente1());
                            $midethistorial->setImgcomponente2($detalle->getImgcomponente2());
                            $midethistorial->setImgcomponente3($detalle->getImgcomponente3());
                            $midethistorial->setImgcomponenteok($detalle->getImgcomponenteok());
                            $midethistorial->setDetformacion($detalle->getDetformacion());
                            $midethistorial->setEntrenamientoId($entrenamiento->getId());
                            $em->persist($midethistorial);
                            $orden+=1;
                        }
                        if ( $orden==4 ) {
                            $midethistorial = New Dethistorial();
							$midethistorial->setDetcurso($detcurso);
                            $midethistorial->setOrden($orden);
                            $midethistorial->setHistorial($mihistorial);
                            $midethistorial->setEntrenamiento($entrenamiento);
                            $midethistorial->setNombre($detalle->getNombre());
                            $midethistorial->setPregunta($detalle->getPregunta4());
                            $midethistorial->setTipoentrenamiento($detalle->getTipoentrenamiento());
                            $midethistorial->setImgrotar1($detalle->getImgrotar1());
                            $midethistorial->setImgrotar2($detalle->getImgrotar2());
                            $midethistorial->setImgrotar3($detalle->getImgrotar3());
                            $midethistorial->setImgrotarok($detalle->getImgrotarok());
                            $midethistorial->setImgbase($detalle->getImgbase());
                            $midethistorial->setCoordenadas($detalle->getCoordenadas());
                            $midethistorial->setCoordenadas2($detalle->getCoordenadas2());
                            $midethistorial->setCoordenadas3($detalle->getCoordenadas3());
                            $midethistorial->setCoordenadasok($detalle->getCoordenadasok());
                            $midethistorial->setDetformacion($detalle->getDetformacion());
                            $midethistorial->setEntrenamientoId($entrenamiento->getId());
                            $em->persist($midethistorial);
                            $orden+=1;
                        }
                        $orden=0;
                    } elseif ($vv=="1-Imagen") {
                        // $midethistorial = New Dethistorial();
                        $midethistorial->setDetcurso($detcurso);
                        $midethistorial->setOrden($orden);
                        $midethistorial->setHistorial($mihistorial);
                        $midethistorial->setEntrenamiento($entrenamiento);
                        $midethistorial->setNombre($detalle->getNombre());
                        $midethistorial->setPregunta($detalle->getPregunta3());
                        $midethistorial->setTipoentrenamiento($detalle->getTipoentrenamiento());
                        $midethistorial->setImg1($detalle->getImg1());
                        $midethistorial->setImg2($detalle->getImg2());
                        $midethistorial->setImg3($detalle->getImg3());
                        $midethistorial->setImgok($detalle->getImgok());
                        $midethistorial->setDetformacion($detalle->getDetformacion());
                        $midethistorial->setEntrenamientoId($entrenamiento->getId());
                        $em->persist($midethistorial);
                        $orden+=1;
                    } elseif ($vv=="4-Imagen") {
                        // $midethistorial = New Dethistorial();
                        $midethistorial->setDetcurso($detcurso);
                        $midethistorial->setOrden($orden);
                        $midethistorial->setHistorial($mihistorial);
                        $midethistorial->setEntrenamiento($entrenamiento);
                        $midethistorial->setNombre($detalle->getNombre());
                        $midethistorial->setPregunta($detalle->getPregunta2());
                        $midethistorial->setTipoentrenamiento($detalle->getTipoentrenamiento());
                        $midethistorial->setImg1($detalle->getImg1());
                        $midethistorial->setImg2($detalle->getImg2());
                        $midethistorial->setImg3($detalle->getImg3());
                        $midethistorial->setImgok($detalle->getImgok());
                        $midethistorial->setDetformacion($detalle->getDetformacion());
                        $midethistorial->setEntrenamientoId($entrenamiento->getId());
                        $em->persist($midethistorial);
                        $orden+=1;
                    } elseif ($vv=="Colocar componente") {
                        // $midethistorial = New Dethistorial();
                        $midethistorial->setDetcurso($detcurso);
                        $midethistorial->setOrden($orden);
                        $midethistorial->setHistorial($mihistorial);
                        $midethistorial->setEntrenamiento($entrenamiento);
                        $midethistorial->setNombre($detalle->getNombre());
                        $midethistorial->setPregunta($detalle->getPregunta4());
                        $midethistorial->setTipoentrenamiento($detalle->getTipoentrenamiento());
                        $midethistorial->setImgrotar1($detalle->getImgrotar1());
                        $midethistorial->setImgrotar2($detalle->getImgrotar2());
                        $midethistorial->setImgrotar3($detalle->getImgrotar3());
                        $midethistorial->setImgrotarok($detalle->getImgrotarok());
                        $midethistorial->setImgbase($detalle->getImgbase());
                        $midethistorial->setCoordenadas($detalle->getCoordenadas());
                        $midethistorial->setCoordenadas2($detalle->getCoordenadas2());
                        $midethistorial->setCoordenadas3($detalle->getCoordenadas3());
                        $midethistorial->setCoordenadasok($detalle->getCoordenadasok());
                        $midethistorial->setDetformacion($detalle->getDetformacion());
                        $midethistorial->setEntrenamientoId($entrenamiento->getId());
                        $em->persist($midethistorial);
                        $orden+=1;
                    }
                } //foreach ( $detalles as $detalle ) {
            }
        }
        $em->flush();

        $repo = $em->getRepository('GitekUdaBundle:Historial');
        $paginator = $this->get('ideup.simple_paginator');

        $paginator->setItemsPerPage(1);
        $detentrenamientos = $paginator->paginate($repo->getMientrenamiento($mihistorial->getId()))->getResult();

        $session = $this->getRequest()->getSession();
        $session->set('cuenta', 0);


        return $this->render('GitekUdaBundle:Historial:entrenatu.html.twig', array(
						'mihistorial_id' => $mihistorial->getId(),
						'detentrenamientos' => $detentrenamientos,
						'paginator'       => $paginator,
						'seleccionados' => $seleccionados,
		));
    }

    function examinatuAction() {

        //leemos datos
        $request = $this->getRequest();
        $multiseleccionados[] = $request->request->get('operaciones');

        $taller_id = $request->request->get('taller_id');
        $operario_id = $request->request->get('operario_id');
        $curso_id = $request->request->get('curso_id');
        $tiempomax      = $request->request->get('txtdenbora');

        $curso      = $this->getCurso($curso_id);
        $taller     = $this->getTaller($taller_id);
        $operario = $this->getOperario($operario_id);


        $em = $this->getDoctrine()->getManager();

        foreach ( $multiseleccionados as $seleccionados ) {
            $cont=-1;
            foreach ( $seleccionados as $seleccion ) {
                // Ahora en $seleccion tenemos el ID del Entrenamiento. Tenemos que rellenar los datos en Historial/DetHistorial
                $cont+=1;

                if ( $cont==0   ) { // Solo creamos la primera vez, los demás serán los detaller
                    $mihistorial = New Historial();
                    $mihistorial->setOperario($operario);
                    $mihistorial->setTaller($taller);
                    $mihistorial->setCurso($curso);
                    $mihistorial->setHoraini(new \DateTime());
                    $mihistorial->setCreatedat(new \DateTime());
                    $mihistorial->setUpdatedat(new \DateTime());
                    $mihistorial->setTipo("Validación");
                    $mihistorial->setTiempomax($tiempomax);
                    $em->persist($mihistorial);
                }

                $detcurso   = $this->getDetcurso($seleccion);
                $entrenamiento  = $this->getEntrenamiento($detcurso->getEntrenamientoId());
                $detalles   = $this->getDetalleentrenamiento($entrenamiento->getId());

                $orden=0;
                foreach ( $detalles as $detalle ) {
                    $orden+=1;

                    $vv = $detalle->getTipoentrenamiento()->getTipo();

                    $midethistorial = New Dethistorial();
                                        $midethistorial->setDetcurso($detcurso);
                    $midethistorial->setOrden($orden);
                    $midethistorial->setHistorial($mihistorial);
                    $midethistorial->setEntrenamiento($entrenamiento);
                    $midethistorial->setNombre($detalle->getNombre());
                    $midethistorial->setPregunta($detalle->getPregunta());
                    $midethistorial->setPregunta2($detalle->getPregunta2());
                    $midethistorial->setPregunta3($detalle->getPregunta3());
                    $midethistorial->setPregunta4($detalle->getPregunta4());
                    $midethistorial->setTipoentrenamiento($detalle->getTipoentrenamiento());

                    if ( $orden == 1 ) { // aqui cualquier cosa, y si es X7 componente correcto
                        $midethistorial->setImg1($detalle->getImg1());
                        $midethistorial->setImg2($detalle->getImg2());
                        $midethistorial->setImg3($detalle->getImg3());
                        $midethistorial->setImgok($detalle->getImgok());
                        $midethistorial->setDetformacion($detalle->getDetformacion());
                        $midethistorial->setEntrenamientoId($entrenamiento->getId());
                        $orden+=1;
                        $em->persist($midethistorial);
                    }

                    if ( $vv == 'X7' ) {
                        //al ser X7 llenamos los otros campos

                        if ( $orden==2 ) {
                            $midethistorial = New Dethistorial();
                                                        $midethistorial->setDetcurso($detcurso);
                            $midethistorial->setOrden($orden);
                            $midethistorial->setHistorial($mihistorial);
                            $midethistorial->setEntrenamiento($entrenamiento);
                            $midethistorial->setNombre($detalle->getNombre());
                            $midethistorial->setTipoentrenamiento($detalle->getTipoentrenamiento());
                            $midethistorial->setImgepi1($detalle->getImgepi1());
                            $midethistorial->setImgepi2($detalle->getImgepi2());
                            $midethistorial->setImgepi3($detalle->getImgepi3());
                            $midethistorial->setImgepiok($detalle->getImgepiok());
                            $midethistorial->setDetformacion($detalle->getDetformacion());
                            $midethistorial->setEntrenamientoId($entrenamiento->getId());
                            $em->persist($midethistorial);
                            $orden+=1;
                        }
                        if ( $orden==3 ) {
                                $midethistorial = New Dethistorial();
                                                                $midethistorial->setDetcurso($detcurso);
                                $midethistorial->setOrden($orden);
                                $midethistorial->setHistorial($mihistorial);
                                $midethistorial->setEntrenamiento($entrenamiento);
                                $midethistorial->setNombre($detalle->getNombre());
                                $midethistorial->setTipoentrenamiento($detalle->getTipoentrenamiento());
                                $midethistorial->setImgcomponente1($detalle->getImgcomponente1());
                                $midethistorial->setImgcomponente2($detalle->getImgcomponente2());
                                $midethistorial->setImgcomponente3($detalle->getImgcomponente3());
                                $midethistorial->setImgcomponenteok($detalle->getImgcomponenteok());
                                $midethistorial->setDetformacion($detalle->getDetformacion());
                                $midethistorial->setEntrenamientoId($entrenamiento->getId());
                                $em->persist($midethistorial);
                                $orden+=1;
                        }
                        if ( $orden==4 ) {
                                $midethistorial = New Dethistorial();
                                                                $midethistorial->setDetcurso($detcurso);
                                $midethistorial->setOrden($orden);
                                $midethistorial->setHistorial($mihistorial);
                                $midethistorial->setEntrenamiento($entrenamiento);
                                $midethistorial->setNombre($detalle->getNombre());
                                $midethistorial->setTipoentrenamiento($detalle->getTipoentrenamiento());
                                $midethistorial->setImgrotar1($detalle->getImgrotar1());
                                $midethistorial->setImgrotar2($detalle->getImgrotar2());
                                $midethistorial->setImgrotar3($detalle->getImgrotar3());
                                $midethistorial->setImgrotarok($detalle->getImgrotarok());
                                $midethistorial->setImgbase($detalle->getImgbase());
                                $midethistorial->setCoordenadas($detalle->getCoordenadas());
                                $midethistorial->setCoordenadas2($detalle->getCoordenadas2());
                                $midethistorial->setCoordenadas3($detalle->getCoordenadas3());
                                $midethistorial->setCoordenadasok($detalle->getCoordenadasok());
                                $midethistorial->setDetformacion($detalle->getDetformacion());
                                $midethistorial->setEntrenamientoId($entrenamiento->getId());
                                $em->persist($midethistorial);
                                $orden+=1;
                        }

                        $orden=0;

                    }
                } //foreach ( $detalles as $detalle ) {
            }
        }
        $em->flush();

        $repo = $em->getRepository('GitekUdaBundle:Historial');
        $paginator = $this->get('ideup.simple_paginator');

        $paginator->setItemsPerPage(1);
        $detentrenamientos = $paginator->paginate($repo->getMientrenamiento($mihistorial->getId()))->getResult();

        // var_dump( $detentrenamientos );
        return $this->render('GitekUdaBundle:Historial:entrenatu.html.twig', array(
                        'mihistorial_id' => $mihistorial->getId(),
                        'detentrenamientos' => $detentrenamientos,
                        'paginator'       => $paginator,
                        'seleccionados' => $seleccionados,
            ));
    }

    function formazioaplayAction($multisel=null) {
        //leemos datos
        $request = $this->getRequest();

        $multiseleccionados[] = $request->request->get('operaciones');

        if ( $multiseleccionados[0] != null ) {

            $session = $this->getRequest()->getSession();
            $session->set('miseleccion', $multiseleccionados[0]);
        } else {

            $session = $this->getRequest()->getSession();
            $multiseleccionados[0] = $session->get('miseleccion');
        }

        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('GitekUdaBundle:Historial');
        $paginator = $this->get('ideup.simple_paginator');

        $paginator->setItemsPerPage(1);
        $detformaciones = $paginator->paginate($repo->getSeleccion($multiseleccionados[0]))->getResult();

        return $this->render('GitekUdaBundle:Historial:formazioaplay.html.twig', array(
                                'multiseleccionados' => $multiseleccionados[0],
                                'detformaciones' => $detformaciones,
                                'paginator'       => $paginator,
        ));
    }

    function playAction($mihistorial_id) {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('GitekUdaBundle:Historial');
        $paginator = $this->get('ideup.simple_paginator');

        $paginator->setItemsPerPage(1);
        $detentrenamientos = $paginator->paginate($repo->getMientrenamiento($mihistorial_id))->getResult();

        return $this->render('GitekUdaBundle:Historial:entrenatu.html.twig', array(
                                'mihistorial_id' => $mihistorial_id,
                                'detentrenamientos' => $detentrenamientos,
                                'paginator'       => $paginator,
                                // 'seleccionados' => $seleccionados,
        ));
    }

    function checkAction($mihistorial_id) {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $respuesta_usuario = $request->request->get('respuesta');
        $mipag = $request->query->get('page');
        $tienecuenta = $request->request->get('hirugarrenariketa');
        $mipag = $mipag-1;
        $session = $this->getRequest()->getSession();

        // print_r("Tienealgo tiene: " . $tienecuenta);

        // si recive algo es que estamo en x73.html.twig
        if ($tienecuenta == 1) {
            // print_r("Sartu naiz");
            // print_r("<br />");
            // print_r(" Tienecuenta " . $tienecuenta);
            if (is_null($session->get('cuenta'))){
                $session->set('cuenta', 0);
            }

            $cuenta = $session->get('cuenta');
            $session->set('cuenta', $cuenta + 1);
        }


        $midethistorial_id = $request->request->get('midethistorial_id');
        $dethistorial = $this->getDethistorial($midethistorial_id);

        if ( $respuesta_usuario == "0" ) { // no a acertado
            $dethistorial->setPulsaKO($dethistorial->getPulsaKO()+1);
            $dethistorial->setUpdatedat(new \DateTime());
            $em->persist($dethistorial);
            $em->flush();

        } elseif ( $respuesta_usuario == "1" ) {
            $dethistorial->setPulsaOK($dethistorial->getPulsaOK()+1);
            $dethistorial->setUpdatedat(new \DateTime());
            $em->persist($dethistorial);
            $em->flush();
        } elseif ( $respuesta_usuario == "3") {
            // a acertado pero la imagen que se mostraba no era la correcta, luego tiene que reintentar. no guardamos nada. Redirigimos.
            $repo = $em->getRepository('GitekUdaBundle:Historial');
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(1);
            $detentrenamientos = $paginator->paginate($repo->getMientrenamiento($mihistorial_id))->getResult();
            $url = $this->generateUrl('historial_play', array(
                                            'mihistorial_id'    => $mihistorial_id,
                                            'detentrenamientos' => $detentrenamientos,
                                            'paginator'         => $paginator
                                ));
            return $this->redirect(sprintf('%s?page=%s', $url,$mipag));
        }

        $repo = $em->getRepository('GitekUdaBundle:Historial');
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(1);
        $detentrenamientos = $paginator->paginate($repo->getMientrenamiento($mihistorial_id))->getResult();

        return $this->render('GitekUdaBundle:Historial:entrenatu.html.twig', array(
                                'mihistorial_id' => $mihistorial_id,
                                'detentrenamientos' => $detentrenamientos,
                                'paginator'       => $paginator,
                                // 'seleccionados' => $seleccionados,
        ));
    }

    function reintentarAction($mihistorial_id,$mipage) {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $midethistorial_id = $request->request->get('reintentar_midethistorial_id');

        $dethistorial = $this->getDethistorial($midethistorial_id);
		$dethistorial->setPulsako($dethistorial->getPulsako()+1);
        $dethistorial->setPulsareintentar($dethistorial->getPulsareintentar()+1);
        $dethistorial->setUpdatedat(new \DateTime());

        $em->persist($dethistorial);
        $em->flush();

        $session = $this->getRequest()->getSession();
        // if ($session->get('cuenta')!=""){
            $cuenta = $session->get('cuenta');
            $session->set('cuenta', $cuenta + 1);
        // }

        $repo = $em->getRepository('GitekUdaBundle:Historial');
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(1);
        $detentrenamientos = $paginator->paginate($repo->getMientrenamiento($mihistorial_id))->getResult();

        $url = $this->generateUrl('historial_play', array(
                                            'mihistorial_id'		=> $mihistorial_id,
                                            'detentrenamientos'	=> $detentrenamientos,
                                            'paginator'					=> $paginator
                                ));
        return $this->redirect(sprintf('%s?page=%s', $url,$mipage));
    }

    function d16Action($mihistorial_id,$mipage) {
            $request = $this->getRequest();
            $em = $this->getDoctrine()->getManager();

            $midethistorial_id = $request->request->get('d16_midethistorial_id');

            // $midethistorial_id = $request->request->get('midethistorial_iid');
            $dethistorial = $this->getDethistorial($midethistorial_id);

            $detformacion_id = $dethistorial->getDetformacionId();

						$dethistorial = $dethistorial->setPulsako($dethistorial->getPulsako()+1);
            $dethistorial->setPulsad16($dethistorial->getPulsad16()+1);
            $dethistorial->setUpdatedat(new \DateTime());

            $em->persist($dethistorial);
            $em->flush();

            $repo = $em->getRepository('GitekUdaBundle:Historial');
            $paginator = $this->get('ideup.simple_paginator');
            $paginator->setItemsPerPage(1);
            $detentrenamientos = $paginator->paginate($repo->getMientrenamiento($mihistorial_id))->getResult();

            $url = $this->generateUrl('historial_play', array(
                                        'mihistorial_id'		=> $mihistorial_id,
                                        'detentrenamientos'	=> $detentrenamientos,
                                        'paginator'					=> $paginator
                                    ));
            return $this->redirect(sprintf('%s?page=%s#d16', $url,$mipage));

    }

    function sumad16Action() {
        // $request = $this->getRequest();
        if ($this->get('request')->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $midethistorial_id = $this->get('request')->request->get('midethistorial_id');
            $dethistorial = $this->getDethistorial($midethistorial_id);
            $dethistorial->setPulsad16($dethistorial->getPulsad16()+1);
            $dethistorial->setUpdatedat(new \DateTime());
            $em->persist($dethistorial);
            $em->flush();

            return new Response("OK");

        }

    }

    function falloAction() {

        if ($this->get('request')->isXmlHttpRequest()) {

            $em = $this->getDoctrine()->getManager();
            $midethistorial_id = $this->get('request')->request->get('midethistorial_id');
            $dethistorial = $this->getDethistorial($midethistorial_id);
            $dethistorial->setPulsaKO($dethistorial->getPulsaKO()+1);
            $dethistorial->setUpdatedat(new \DateTime());
            $em->persist($dethistorial);
            $em->flush();

            return new Response("OK");

        }

    }

    function solucionajaxAction() {

        if ($this->get('request')->isXmlHttpRequest()) {

            $em = $this->getDoctrine()->getManager();
            $midethistorial_id = $this->get('request')->request->get('midethistorial_id');
            $dethistorial = $this->getDethistorial($midethistorial_id);
            $dethistorial->setPulsasolucion($dethistorial->getPulsasolucion()+1);
            $dethistorial->setUpdatedat(new \DateTime());
            $em->persist($dethistorial);
            $em->flush();

            return new Response("OK");

        }

    }

    function pulsakoajaxAction() {

        if ($this->get('request')->isXmlHttpRequest()) {

            $em = $this->getDoctrine()->getManager();
            $midethistorial_id = $this->get('request')->request->get('midethistorial_id');
            $dethistorial = $this->getDethistorial($midethistorial_id);
            $dethistorial->setPulsako($dethistorial->getPulsako()+1);
            $dethistorial->setUpdatedat(new \DateTime());
            $em->persist($dethistorial);
            $em->flush();

            return new Response("OK");

        }

    }

    function pulsaokajaxAction() {

        if ($this->get('request')->isXmlHttpRequest()) {

            $em = $this->getDoctrine()->getManager();
            $midethistorial_id = $this->get('request')->request->get('midethistorial_id');
            $dethistorial = $this->getDethistorial($midethistorial_id);
            $dethistorial->setPulsaok($dethistorial->getPulsaok()+1);
            $dethistorial->setUpdatedat(new \DateTime());
            $em->persist($dethistorial);
            $em->flush();

            return new Response("OK");

        }

    }

    function solucionAction($mihistorial_id,$mipage) {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        $midethistorial_id = $request->request->get('solucion_midethistorial_id');
        $mipag = $request->query->get('page');

        // $midethistorial_id = $request->request->get('midethistorial_iid');
        $dethistorial = $this->getDethistorial($midethistorial_id);
				$dethistorial = $dethistorial->setPulsako($dethistorial->getPulsako()+1);
        $dethistorial->setPulsasolucion($dethistorial->getPulsasolucion()+1);
        $dethistorial->setUpdatedat(new \DateTime());

      	$em->persist($dethistorial);
        $em->flush();

        $repo = $em->getRepository('GitekUdaBundle:Historial');
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(1);
        $detentrenamientos = $paginator->paginate($repo->getMientrenamiento($mihistorial_id))->getResult();

        $url = $this->generateUrl('historial_play', array(
                                                    'mihistorial_id'		=> $mihistorial_id,
                                                    'detentrenamientos'	=> $detentrenamientos,
                                                    'paginator'					=> $paginator
                                ));
        return $this->redirect(sprintf('%s?page=%s#solucion', $url,$mipage));
    }

	function finAction($mihistorial_id) {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getManager();

        // $midethistorial_id = $request->request->get('solucion_midethistorial_id');
		$midethistorial_id = $request->request->get('midethistorial_id');
        $mipag = $request->query->get('page');

        $dethistorial   = $this->getDethistorial($midethistorial_id);
        $historial 		= $this->getHistorial($mihistorial_id);
		$historial->setHorafin(new \DateTime());
		// Comprobamos si a respondido todas correctamente, en ese caso enhorabuena y se le activa la opción de realizar Examen (COMPLETATO==1)
		$completado =1;
		$detalles = $this->getDetalleshistorial($mihistorial_id);

		foreach ($detalles as $detalle) {

			if ( $detalle->getPulsako() > 0 ) {
				$completado=0;
				break;
			}
		}

		if ( $completado==1) {
			$historial->setCompletado(1);
            $historial->setUpdatedat(new \DateTime());
		} else {
			$historial->setCompletado(0);
            $historial->setUpdatedat(new \DateTime());
		}

        // calculamos el tiempo que le a llevado en segundos
        // en Historial.tiempomax tenemos el tiempo máximo para realizar el examen
        // en Historial.tiempoexamen guardaremos el tiempo en el que se a realizado la prueba para historico

        $tiempomax = $historial->getTiempomax();

        $tiempoini = $historial->getHoraini();

        $tiempofin = $historial->getHorafin();

        $tiemporeal= $tiempoini->diff($tiempofin);

        // echo $tiempoini->format("h:m:s");
        // echo "<br />";
        // echo $tiempofin->format("h:m:s");
        // echo "<br />";
        // echo $tiemporeal->format("h:m:s");
        // echo "<br />";
        // echo $tiemporeal->format('%s');


        $historial->setTiempoexamen($tiemporeal->format("%s"));



        if( $tiemporeal->format("%s") > $tiempomax ) { // Suspendido
            $historial->setAprobado("0");
        } else {
            // lo a echo dentro del tiempo
            // miramos si a tenido errores
            if ( $completado = 1 ) {
                $historial->setAprobado("1");

                //tenemos que marcar los entrenamientos correspondientes aprobados tambien
                //seran aquellos que el operario,taller, curso
                $misentrenamientos = $this->getHistorialparaexamen(
                $historial->getTallerId(),
                $historial->getOperarioId(),
                $historial->getCursoId());

                if($misentrenamientos) {
                    foreach ($misentrenamientos as $mentrenamiento) {
                        $mentrenamiento->setAprobado("1");
                        $mentrenamiento->setUpdatedat(new \DateTime);
                        $em->persist($mentrenamiento);
                    }
                } else {
                }

            } else {
                $historial->setAprobado("0");
            }
        }



        $dethistorial->setUpdatedat(new \DateTime());
        $em->persist($dethistorial);
        $em->persist($historial);
		$em->flush();

		if ( $completado==1 ) {

			$url = $this->generateUrl('historial_aprobado', array(
                                'mihistorial_id'		=> $mihistorial_id,
                                'midethistorial_id'	=> $midethistorial_id,
            ));

	        return $this->redirect($url);


		} else {

    		$url = $this->generateUrl('historial_suspendido', array(
                                'mihistorial_id'		=> $mihistorial_id,
                                'midethistorial_id'	=> $midethistorial_id,
            ));
	        return $this->redirect($url);

		}

    }

	public function aprobadoAction($mihistorial_id,$midethistorial_id) {
        $em = $this->getDoctrine()->getManager();

        $historial = $em->getRepository("GitekUdaBundle:Historial")->find($mihistorial_id);

		return $this->render('GitekUdaBundle:Historial:aprobado.html.twig', array(
          'mihistorial_id' 		=> $mihistorial_id,
          'midethistorial_id'	=> $midethistorial_id,
          'historial'           => $historial
        ));

    }

	public function suspendidoAction($mihistorial_id,$midethistorial_id) {
        $em = $this->getDoctrine()->getManager();

        $historial = $em->getRepository("GitekUdaBundle:Historial")->find($mihistorial_id);

		return $this->render('GitekUdaBundle:Historial:suspendido.html.twig', array(
          'mihistorial_id' 		=> $mihistorial_id,
          'midethistorial_id'	=> $midethistorial_id,
          'historial'           => $historial,
        ));
    }

    public function usuariostallerAction($tallerid) {
        $em = $this->getDoctrine()->getManager();


        $historial = $em->getRepository("GitekUdaBundle:Historial")->find($mihistorial_id);

        return $this->render('GitekUdaBundle:Historial:suspendido.html.twig', array(
          'mihistorial_id'      => $mihistorial_id,
          'midethistorial_id'   => $midethistorial_id,
          'historial'           => $historial,
        ));
    }

    public function operariostallerAction() { // EN KAIXO COMBOBOX CON AJAX
        $em = $this->getDoctrine()->getManager();

        $id = $this->get('request')->query->get('data');

        $em = $this->getDoctrine()->getManager();

        if (($id!=null) && ($id!="0")) {
            $consulta = $em->createQuery(
                    "SELECT o FROM GitekUdaBundle:Operario o
                    WHERE o.taller_id = :mitallerid
                    ORDER BY o.apellidos ASC
                    ")
                    ->setParameter('mitallerid',$id);

            $operarios = $consulta->getResult();

        } else {
            $consulta = $em->createQuery(
                    "SELECT o FROM GitekUdaBundle:Operario o
                    ORDER BY o.apellidos ASC
                    ");
                    $operarios = $consulta->getResult();
        }

        return $this->render('GitekUdaBundle:Historial:combo.html.twig', array(
            'operarios' => $operarios
        ));

    }

    public function operariostallerfiltroAction() { // EN HISTORIAL COMBOBOX CON AJAX
        $em = $this->getDoctrine()->getManager();

        $id = $this->get('request')->query->get('data');
        $operario_id = $this->get('request')->query->get('operario_id');

        $em = $this->getDoctrine()->getManager();

        if (($id!=null) && ($id!="0")) {
            $consulta = $em->createQuery(
                    "SELECT o FROM GitekUdaBundle:Operario o
                    WHERE o.taller_id = :mitallerid
                    ORDER BY o.apellidos ASC
                    ")
                    ->setParameter('mitallerid',$id);

            $operarios = $consulta->getResult();

        } else {
            $consulta = $em->createQuery(
                    "SELECT o FROM GitekUdaBundle:Operario o
                    ORDER BY o.apellidos ASC
                    ");
                    $operarios = $consulta->getResult();
        }

        return $this->render('GitekUdaBundle:Historial:combofiltro.html.twig', array(
            'operarios'     => $operarios,
            'operario_id'   => $operario_id,
        ));

    }

    public function operacionescursofiltroAction() { // EN HISTORIAL COMBOBOX CON AJAX
        $em = $this->getDoctrine()->getManager();

        $id = $this->get('request')->query->get('data');
        $operacion_id = $this->get('request')->query->get('operacion_id');

        $em = $this->getDoctrine()->getManager();

        if (($id!=null) && ($id!="0")) {
            $consulta = $em->createQuery(
                    "SELECT d FROM GitekUdaBundle:Detcurso d
                    WHERE d.curso_id = :cursoid
                    ORDER BY d.nombre ASC
                    ")
                    ->setParameter('cursoid',$id);

            $operaciones = $consulta->getResult();

        } else {
            $consulta = $em->createQuery(
                    "SELECT d FROM GitekUdaBundle:Detcurso d
                    ORDER BY d.nombre ASC
                    ");
            $operaciones = $consulta->getResult();
        }

        return $this->render('GitekUdaBundle:Historial:combooperacionesfiltro.html.twig', array(
            'operaciones'   => $operaciones,
            'operacion_id'   => $operacion_id,
        ));

    }

    protected function getDethistorial($id) {
        $em = $this->getDoctrine()->getManager();
        $dethistorial = $em->getRepository('GitekUdaBundle:Dethistorial')->find($id);
        if (!$dethistorial) {
            throw $this->createNotFoundException('ez da aurkitu.Pena.');
        }
        return $dethistorial;
    }

	protected function getHistorial($mihistorial_id) {
      $em = $this->getDoctrine()->getManager();
      $historial = $em->getRepository('GitekUdaBundle:Historial')->find($mihistorial_id);
      if (!$historial) {
          throw $this->createNotFoundException('ez da aurkitu.Jur!');
      }
      return $historial;
		}

	protected function getHistorialparaexamen ( $taller_id, $operario_id, $curso_id) {
      $em = $this->getDoctrine()->getManager();
      $historial = $em->getRepository('GitekUdaBundle:Historial')->getlatestHistorial($taller_id, $operario_id, $curso_id );

      // if (!$historial) {
      //           throw $this->createNotFoundException('ez da aurkitu.ups!');
      //       }
      return $historial;
		}

	protected function getDetalleshistorial($historial_id) {
        $em = $this->getDoctrine()->getManager();
        $dethistorial = $em->getRepository('GitekUdaBundle:Dethistorial')->findBy(array('historial_id'=>$historial_id));
        if (!$dethistorial) {
            throw $this->createNotFoundException('ez da aurkitu.Pena.');
        }
        return $dethistorial;
	}

    protected function getEntrenamientosexamen($operarioid,$tallerid,$curosid) {
        $em = $this->getDoctrine()->getManager();
        $misentrenamientos = $em->getRepository('GitekUdaBundle:Historial')->findBy(
            array(
                'operario_id'   => $operarioid,
                'taller_id'     => $tallerid,
                'curso_id'      => $cursoid,
                'tipo'          => 'Simulación'
                )
        );
        if (!$misentrenamientos) {
            throw $this->createNotFoundException('ez da aurkitu.misentrenamientos.');
        }
        return $misentrenamientos;
    }

    protected function getCurso($id) {
        $em = $this->getDoctrine()->getManager();
        $curso = $em->getRepository('GitekUdaBundle:Curso')->find($id);
        if (!$curso) {
            throw $this->createNotFoundException('Ez da aurkitu 1.');
        }
        return $curso;
    }

    protected function getDetcurso($id) {
        $em = $this->getDoctrine()->getManager();
        $detcurso = $em->getRepository('GitekUdaBundle:Detcurso')->find($id);
        if (!$detcurso) {
            throw $this->createNotFoundException('Ez da aurkitu 2.');
        }
        return $detcurso;
    }

    protected function getTaller($id) {
        $em = $this->getDoctrine()->getManager();


        $taller = $em->getRepository('GitekUdaBundle:Taller')->find($id);

        // $consulta = $em->createQuery(
        //             "SELECT t FROM GitekUdaBundle:Taller t
        //             WHERE t.taller_id = :mitallerid
        //             ")
        //             ->setParameter('mitallerid',$id);

        //     $taller = $consulta->getResult();
        if (!$taller) {
            throw $this->createNotFoundException('Ez da aurkitu 3.');
        }
	   return $taller;
    }

    protected function getOperario($id) {
        $em = $this->getDoctrine()->getManager();
        $operario = $em->getRepository('GitekUdaBundle:Operario')->find($id);
        if (!$operario) {
            throw $this->createNotFoundException('Unable to find operario post.');
        }
        return $operario;
    }

    protected function getEntrenamiento($id) {
        $em = $this->getDoctrine()->getManager();
        $entrenamiento = $em->getRepository('GitekUdaBundle:Entrenamiento')->find($id);
        if (!$entrenamiento) {
            throw $this->createNotFoundException('No ha sido posible encontrar el entrenamiento '.$id);
        }
        return $entrenamiento;
    }

    protected function getDetalleentrenamiento($m) {
        $repositorio = $this->getDoctrine()
                            ->getRepository('GitekUdaBundle:Detentrenamiento');

        $consulta = $repositorio->createQueryBuilder('p')
			    ->where('p.entrenamiento_id = :miid')
			    ->setParameter('miid', $m)
			    ->getQuery();

        $detalleentrenamientos = $consulta->getResult();
        return $detalleentrenamientos;
    }

}








